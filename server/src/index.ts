import http from "http";
import express from "express";
import cors from "cors";
import {LobbyRoom, Server} from "colyseus";
import {monitor} from "@colyseus/monitor";
import {SettlersRoom} from "./rooms/SettlersRoom";
import * as path from "path";
import expressBasicAuth from "express-basic-auth";
// import socialRoutes from "@colyseus/social/express"

const port = Number(process.env.PORT || 2567);
const app = express()

app.use(cors());
app.use(express.json())

const server = http.createServer(app);
const gameServer = new Server({
    server,
});

// lobby room just tracks all other open rooms (using enableRealtimeListing)
// This allows client to see room updates without refreshing
gameServer
    .define("lobby", LobbyRoom);

// register your room handlers
gameServer
    .define('settlers', SettlersRoom)
    .enableRealtimeListing();

/**
 * Register @colyseus/social routes
 *
 * - uncomment if you want to use default authentication (https://docs.colyseus.io/server/authentication/)
 * - also uncomment the import statement
 */
// app.use("/", socialRoutes);

const basicAuthMiddleware = expressBasicAuth({
    // list of users and passwords
    users: {
        "admin": process.env.ADMIN_PASSWORD ?? "admin",
    },
    // sends WWW-Authenticate header, which will prompt the user to fill
    // credentials in
    challenge: true
});

// register colyseus monitor AFTER registering your room handlers
app.use("/colyseus", basicAuthMiddleware, monitor());

// Routing for react client
let clientPath = path.join(__dirname, '../../client/build');
app.use("/", express.static(clientPath));
app.use((req, res) => res.sendFile(path.join(clientPath, "index.html")));

gameServer.listen(port);
console.log(`Listening on ws://localhost:${port}`)
