import {TileSchema} from "./schema/TileSchema";
import {SettlersSchema} from "./schema/SettlersSchema";
import {VertexLoc} from "./schema/VertexSchema";

export function resetKnightStatus(state: SettlersSchema) {
    if (!state.settings.addonCitiesAndKnights) {
        return;
    }
    let update = (tile: TileSchema, loc: VertexLoc) => {
        let knight = tile[loc].knight;
        if (knight !== undefined) {
            knight.actionTaken = false;
            knight.promoted = false;
        }
    };

    for (let tile of state.board.tiles) {
        update(tile, "top");
        update(tile, "bottom");
    }
}