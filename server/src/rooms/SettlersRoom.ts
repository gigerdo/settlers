import {Client, matchMaker, Room, ServerError, updateLobby} from "colyseus";
import {ArraySchema, MapSchema, SetSchema} from "@colyseus/schema";
import {Dispatcher} from "@colyseus/command";
import {SettlersSchema} from "./schema/SettlersSchema";
import {StartGameCommand} from "./commands/StartGameCommand";
import {JoinGameCommand} from "./commands/JoinGameCommand";
import {PlaceSettlementCommand} from "./commands/PlaceSettlementCommand";
import {PlaceRoadCommand} from "./commands/PlaceRoadCommand";
import {RollDiceCommand} from "./commands/RollDiceCommand";
import {FinishTurnCommand} from "./commands/FinishTurnCommand";
import {BuildRoadCommand} from "./commands/BuildRoadCommand";
import {BuildSettlementCommand} from "./commands/BuildSettlementCommand";
import {BuildCityCommand} from "./commands/BuildCityCommand";
import {BuyDevelopmentCardCommand} from "./commands/BuyDevelopmentCardCommand";
import {PlaceRobberCommand} from "./commands/PlaceRobberCommand";
import {PlayDevelopmentCardCommand} from "./commands/PlayDevelopmentCardCommand";
import {NewTradeOfferCommand} from "./commands/NewTradeOfferCommand";
import {CancelTradeOfferCommand} from "./commands/CancelTradeOfferCommand";
import {ExecuteTradeCommand} from "./commands/ExecuteTradeCommand";
import {AnswerTradeOfferCommand} from "./commands/AnswerTradeOfferCommand";
import {DiscardCardsCommand} from "./commands/DiscardCardsCommand";
import {persistence} from "./Persistence";
import {BoardSchema} from "./schema/BoardSchema";
import {BankSchema} from "./schema/BankSchema";
import {PlayerSchema} from "./schema/PlayerSchema";
import {RobberSchema} from "./schema/RobberSchema";
import {ResourcesSchema} from "./schema/ResourcesSchema";
import {EdgeSchema, HarborType} from "./schema/EdgeSchema";
import {TileSchema} from "./schema/TileSchema";
import {VertexSchema} from "./schema/VertexSchema";
import {DevelopmentCardSchema} from "./schema/DevelopmentCardSchema";
import {updateLongestRoad} from "./updateLongestRoad";
import {TradeSchema} from "./schema/TradeSchema";
import {ChatMessage} from "./ChatMessage";
import {GameSettingsSchema} from "./schema/GameSettingsSchema";
import {DiceCardsSchema} from "./schema/DiceCardsSchema";
import {DiceCardSchema} from "./schema/DiceCardSchema";
import {BuyCityUpgradeCommand} from "./commands/BuyCityUpgradeCommand";
import {CityUpgradeSchema} from "./schema/CityUpgradeSchema";
import {PlayProgressCardCommand} from "./commands/progresscards/PlayProgressCardCommand";
import {trace} from "@opentelemetry/api";
import {BuildWallCommand} from "./commands/BuildWallCommand";
import {BuildMetropolisCommand} from "./commands/BuildMetropolisCommand";
import {ChooseResourceCommand} from "./commands/ChooseResourceCommand";
import {BuyKnightCommand} from "./commands/BuyKnightCommand";
import {KnightSchema} from "./schema/KnightSchema";
import {PromoteKnightCommand} from "./commands/PromoteKnightCommand";
import {ActivateKnightCommand} from "./commands/ActivateKnightCommand";
import {MoveKnightCommand} from "./commands/MoveKnightCommand";
import {ChaseAwayRobberCommand} from "./commands/ChaseAwayRobberCommand";
import {DestroyCityCommand} from "./commands/DestroyCityCommand";
import {ChooseProgressCardCommand} from "./commands/ChooseProgressCardCommand";
import {RemoveKnightCommand} from "./commands/RemoveKnightCommand";
import {SkipCommand} from "./commands/SkipCommand";

export const tracer = trace.getTracer("settlers-game");

export class SettlersRoom extends Room<SettlersSchema> {
    dispatcher: Dispatcher<SettlersRoom> = new Dispatcher(this);

    async onCreate(options: any) {
        this.setState(new SettlersSchema());
        this.maxClients = 4;
        this.autoDispose = false;

        if (options.roomId) {
            let roomId = options.roomId;
            const existingRoom = matchMaker.getRoomById(roomId);
            if (existingRoom) {
                throw new ServerError(500, "Room already exists " + roomId);
            }
            this.roomId = roomId;
            await this.setPrivate(true);
            console.log("Restore room from db", roomId);
            let prev = await persistence.loadRoomState(roomId)
                .catch(err => {
                    throw new ServerError(500, "loadRoomState failed " + err);
                });
            if (prev === undefined) {
                throw new ServerError(500, "Unknown room " + roomId);
            }
            this.state.assign(prev);
            this.state.settings = new GameSettingsSchema();
            this.state.settings.assign(prev.settings);
            this.state.diceCards = new DiceCardsSchema();
            this.state.diceCards.currentDeck = new ArraySchema();
            prev.diceCards.currentDeck.forEach(card => {
                let diceCard = new DiceCardSchema(card.roll1, card.roll2);
                this.state.diceCards.currentDeck.push(diceCard);
            });
            this.state.players = new MapSchema<PlayerSchema>();
            Object.entries(prev.players)
                .forEach(([key, player]) => {
                    let playerSchema = new PlayerSchema(key);
                    playerSchema.assign(player);
                    playerSchema.resources = new ResourcesSchema(0, 0, 0, 0, 0, 0, 0, 0);
                    playerSchema.resources.assign(player.resources);
                    playerSchema.harbors = new SetSchema<HarborType>();
                    playerSchema.connected = false;
                    player.harbors.forEach((h: HarborType) => playerSchema.harbors.add(h));
                    playerSchema.developmentCards = new ArraySchema<DevelopmentCardSchema>();
                    player.developmentCards.forEach((d: DevelopmentCardSchema) => {
                        let developmentCardSchema = new DevelopmentCardSchema("monopoly");
                        developmentCardSchema.assign(d);
                        playerSchema.developmentCards.push(developmentCardSchema);
                    });
                    playerSchema.cityUpgrades = new CityUpgradeSchema();
                    playerSchema.cityUpgrades.assign(player.cityUpgrades);
                    this.state.players.set(key, playerSchema);
                });
            this.state.board = new BoardSchema();
            this.state.board.assign(prev.board);
            this.state.board.tiles = new ArraySchema<TileSchema>();
            prev.board.tiles.forEach(tile => {
                let tileSchema = new TileSchema("desert", -1, -1, -1);
                tileSchema.assign(tile);
                tileSchema.players = new SetSchema<string>();
                tile.players.forEach(p => tileSchema.players.add(p));

                tileSchema.top = new VertexSchema(-1, -1, "top");
                tileSchema.top.assign(tile.top);
                if (tile.top.knight !== undefined) {
                    tileSchema.top.knight = new KnightSchema();
                    tileSchema.top.knight.assign(tile.top.knight);
                }

                tileSchema.bottom = new VertexSchema(-1, -1, "bottom");
                tileSchema.bottom.assign(tile.bottom);
                if (tile.bottom.knight !== undefined) {
                    tileSchema.bottom.knight = new KnightSchema();
                    tileSchema.bottom.knight.assign(tile.bottom.knight);
                }

                tileSchema.north = new EdgeSchema(-1, -1, "north");
                tileSchema.north.assign(tile.north);
                tileSchema.east = new EdgeSchema(-1, -1, "east");
                tileSchema.east.assign(tile.east);
                tileSchema.south = new EdgeSchema(-1, -1, "south");
                tileSchema.south.assign(tile.south);
                this.state.board.tiles.push(tileSchema);
            });
            this.state.board.robber = new RobberSchema(-1, -1);
            this.state.board.robber.assign(prev.board.robber);
            this.state.bank = new BankSchema();
            this.state.bank.assign(prev.bank);
            this.state.openTrades = new MapSchema<TradeSchema>();
            if (prev.knightBeingDisplaced !== undefined) {
                this.state.knightBeingDisplaced = new KnightSchema();
                this.state.knightBeingDisplaced.assign(prev.knightBeingDisplaced);
            }

            updateLongestRoad(this.state);
        }

        this.onMessage("command", (client, message) => {
            try {
                let playerId = client.userData!.playerId;
                switch (message.name) {
                    case "start-game":
                        this.dispatcher.dispatch(new StartGameCommand(playerId, message.settings));
                        break;
                    case "place-settlement":
                        this.dispatcher.dispatch(new PlaceSettlementCommand(playerId, message.index, message.loc));
                        break;
                    case "place-road":
                        this.dispatcher.dispatch(new PlaceRoadCommand(playerId, message.index, message.loc));
                        break;
                    case "roll-dice":
                        this.dispatcher.dispatch(new RollDiceCommand(playerId, message.redDie, message.yellowDie));
                        break;
                    case "finish-turn":
                        this.dispatcher.dispatch(new FinishTurnCommand(playerId));
                        break;
                    case "build-road":
                        this.dispatcher.dispatch(new BuildRoadCommand(playerId, message.index, message.loc));
                        break;
                    case "build-settlement":
                        this.dispatcher.dispatch(new BuildSettlementCommand(playerId, message.index, message.loc));
                        break;
                    case "build-city":
                        this.dispatcher.dispatch(new BuildCityCommand(playerId, message.index, message.loc, message.useMedicineCard));
                        break;
                    case "build-wall":
                        this.dispatcher.dispatch(new BuildWallCommand(playerId, message.index, message.loc, message.useEngineerCard));
                        break;
                    case "buy-development-card":
                        this.dispatcher.dispatch(new BuyDevelopmentCardCommand(playerId));
                        break;
                    case "place-robber":
                        this.dispatcher.dispatch(new PlaceRobberCommand(playerId, message.x, message.y, message.stealFromPlayer));
                        break;
                    case "play-development-card":
                        this.dispatcher.dispatch(new PlayDevelopmentCardCommand(playerId, message.card,
                            message.firstResource, message.secondResource, message.monopoly));
                        break;
                    case "new-trade-offer":
                        this.dispatcher.dispatch(new NewTradeOfferCommand(playerId, message.trade, client));
                        break;
                    case "answer-trade-offer":
                        this.dispatcher.dispatch(new AnswerTradeOfferCommand(playerId, message.tradeOfferId, message.acceptOffer));
                        break;
                    case "execute-trade":
                        this.dispatcher.dispatch(new ExecuteTradeCommand(playerId, message.tradeOfferId, message.playerId));
                        break;
                    case "cancel-trade-offer":
                        this.dispatcher.dispatch(new CancelTradeOfferCommand(playerId, message.tradeOfferId));
                        break;
                    case "discard-cards":
                        this.dispatcher.dispatch(new DiscardCardsCommand(playerId, message.resources));
                        break;
                    case "choose-resource":
                        this.dispatcher.dispatch(new ChooseResourceCommand(playerId, message.resourceToReceive));
                        break;
                    case "buy-city-upgrade":
                        this.dispatcher.dispatch(new BuyCityUpgradeCommand(playerId, message.color, message.useCrane));
                        break;
                    case "build-metropolis":
                        this.dispatcher.dispatch(new BuildMetropolisCommand(playerId, message.index, message.loc));
                        break;
                    case "buy-knight":
                        this.dispatcher.dispatch(new BuyKnightCommand(playerId, message.index, message.loc));
                        break;
                    case "activate-knight":
                        this.dispatcher.dispatch(new ActivateKnightCommand(playerId, message.index, message.loc));
                        break;
                    case "promote-knight":
                        this.dispatcher.dispatch(new PromoteKnightCommand(playerId, message.index, message.loc));
                        break;
                    case "move-knight":
                        this.dispatcher.dispatch(new MoveKnightCommand(playerId, message.fromIndex, message.fromLoc, message.toIndex, message.toLoc));
                        break;
                    case "remove-knight":
                        this.dispatcher.dispatch(new RemoveKnightCommand(playerId, message.index, message.loc));
                        break;
                    case "chase-away-robber":
                        this.dispatcher.dispatch(new ChaseAwayRobberCommand(playerId, message.index, message.loc));
                        break;
                    case "destroy-city":
                        this.dispatcher.dispatch(new DestroyCityCommand(playerId, message.index, message.loc));
                        break;
                    case "choose-progress-card":
                        this.dispatcher.dispatch(new ChooseProgressCardCommand(playerId, message.color));
                        break;
                    case "play-progress-card":
                        this.dispatcher.dispatch(new PlayProgressCardCommand(playerId,
                            message.card, message.resource, message.player, message.index, message.loc));
                        break;
                    case "skip":
                        this.dispatcher.dispatch(new SkipCommand(playerId));
                        break;
                }
            } catch (e) {
                console.error("Uncaught exception", e);
            }

            persistence.storeRoomState(this.roomId, this.state);
        });

        this.onMessage("chat", (client, message) => {
            let playerId = client.userData!.playerId;
            let player = this.state.players.get(playerId);
            if (player) {
                let data: ChatMessage = {
                    player: player.name,
                    color: player.color,
                    message: message,
                };
                this.broadcast("chat", data);
            }
        });

        console.log("Created room", options);
    }

    async onAuth(client: Client, options: any) {
        // There is no actual authentication
        // The client just has to supply a random userId
        // This id will be used to identify the player the client represents
        if (options.userId) {
            if (this.state.phase === "setup") {
                // Game hasn't started, anyone can join
                return true;
            } else {
                // Game has started, only already joined players can re-join
                let player = Array.from(this.state.players?.values())
                    .find(p => p.id === options.userId);
                if (player) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    async onJoin(client: Client, options: any) {
        try {
            console.log("Player joined", options.userId);
            if (this.state.phase === "setup") {
                await this.dispatcher.dispatch(new JoinGameCommand(options.userId, options.userName));
                client.userData = {
                    playerId: options.userId,
                };
                await this.setMetadata({
                    players: this.state.players.size,
                });
                updateLobby(this);
                return;
            }

            let player = Array.from(this.state.players?.values())
                .find(p => p.id === options.userId);
            if (player) {
                console.log("Player has reconnected", player.id);
                player.connected = true;
                client.userData = {
                    playerId: player.id,
                };
                return;
            }
        } catch (e) {
            console.error("Uncaught exception", e);
        }
    }

    async onLeave(client: Client, consented: boolean) {
        try {
            console.log("Player disconnected", client.userData.playerId, client.id);
            let playerId = client.userData.playerId;
            this.state.players.get(playerId)!.connected = false;

            if (this.state.phase === "setup") {
                this.state.players.delete(playerId);
                await this.setMetadata({
                    players: this.state.players.size,
                });
                updateLobby(this);
            }
        } catch (e) {
            console.error("Uncaught exception", e);
        }
    }

    async onDispose() {
        console.log("Disposed");
        await persistence.storeRoomState(this.roomId, this.state)
            .catch(error => {
                console.error("storeRoomState failed", error);
            });
    }

}
