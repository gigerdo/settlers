/**
 * Cube coordinates for hexagons.
 * Mainly used to easily calculate distances and rings.
 * Reference: https://www.redblobgames.com/grids/hexagons/
 */
export class CubeCoordinates {
    readonly q: number;
    readonly r: number;
    readonly s: number;

    static readonly DIRECTIONS = [
        new CubeCoordinates(+1, 0, -1),
        new CubeCoordinates(+1, -1, 0),
        new CubeCoordinates(0, -1, +1),
        new CubeCoordinates(-1, 0, +1),
        new CubeCoordinates(-1, +1, 0),
        new CubeCoordinates(0, +1, -1),
    ]

    constructor(q: number, r: number, s: number) {
        this.q = q;
        this.r = r;
        this.s = s;
    }

    static fromOffsetCoordinates(x: number, y: number) {
        // From offset-coordinates
        const offset = y % 2;
        const q = x - (y - offset) / 2
        const r = y
        const s = -q - r;
        return new CubeCoordinates(q, r, s);
    }

    add(b: CubeCoordinates): CubeCoordinates {
        return new CubeCoordinates(
            this.q + b.q,
            this.r + b.r,
            this.s + b.s
        )
    }

    subtract(b: CubeCoordinates): CubeCoordinates {
        return new CubeCoordinates(
            this.q - b.q,
            this.r - b.r,
            this.s - b.s
        )
    }

    distanceTo(b: CubeCoordinates): number {
        const vec = this.subtract(b);
        return Math.max(
            Math.abs(vec.q),
            Math.abs(vec.r),
            Math.abs(vec.s)
        );
    }

    scale(factor: number): CubeCoordinates {
        return new CubeCoordinates(
            this.q * factor,
            this.r * factor,
            this.s * factor
        )
    }

    neighbor(direction: number): CubeCoordinates {
        return this.add(CubeCoordinates.DIRECTIONS[direction]);
    }

    ring(radius: number): CubeCoordinates[] {
        let ringHex = this.add(CubeCoordinates.DIRECTIONS[4].scale(radius));
        let results: CubeCoordinates[] = []
        for (let i = 0; i < 6; i++) {
            for (let j = 0; j < radius; j++) {
                results.push(ringHex);
                ringHex = ringHex.neighbor(i);
            }
        }
        return results;
    }

    toOffsetCoordinates(): { x: number, y: number } {
        const x = this.q + (this.r - (this.r % 2)) / 2;
        const y = this.r;
        return {x, y}
    }
}