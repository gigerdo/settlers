import {Schema, SetSchema, type} from "@colyseus/schema";
import {ResourcesSchema} from "./ResourcesSchema";

export interface NewTradeState {
    give: TradeResources;
    receive: TradeResources;
}

export interface TradeResources {
    lumber: number;
    brick: number;
    wool: number;
    grain: number;
    ore: number;
    paper: number;
    cloth: number;
    coin: number;
}

export class TradeSchema extends Schema {
    @type("string") id: string;
    @type(ResourcesSchema) give: ResourcesSchema;
    @type(ResourcesSchema) receive: ResourcesSchema;

    @type({set: "string"}) rejected = new SetSchema<string>();
    @type({set: "string"}) accepted = new SetSchema<string>();

    constructor(id: string, offer: NewTradeState) {
        super();
        this.id = id;
        this.give = new ResourcesSchema(offer.give.lumber, offer.give.brick, offer.give.wool, offer.give.grain,
            offer.give.ore, offer.give.paper, offer.give.cloth, offer.give.coin);
        this.receive = new ResourcesSchema(offer.receive.lumber, offer.receive.brick, offer.receive.wool,
            offer.receive.grain, offer.receive.ore, offer.receive.paper, offer.receive.cloth, offer.receive.coin);
    }
}