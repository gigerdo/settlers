import {ArraySchema, filter, Schema, SetSchema, type} from "@colyseus/schema";
import {EdgeSchema, HarborType} from "./EdgeSchema";
import {ResourcesSchema} from "./ResourcesSchema";
import {DevelopmentCardSchema} from "./DevelopmentCardSchema";
import {Client} from "colyseus";
import {CityUpgradeSchema} from "./CityUpgradeSchema";

let onlyForCurrentPlayer = function (this: PlayerSchema, client: Client) {
    return this.id === client.userData.playerId;
};

export class PlayerSchema extends Schema {
    @type("string") id: string;
    @type("string") name: string;
    @type("string") color: string;

    @type("boolean") connected: boolean = true;

    @filter(onlyForCurrentPlayer)
    @type("boolean") canBuildRoad: boolean = false;

    @filter(onlyForCurrentPlayer)
    @type("boolean") canBuildSettlement: boolean = false;

    @filter(onlyForCurrentPlayer)
    @type("boolean") canBuildCity: boolean = false;

    @filter(onlyForCurrentPlayer)
    @type("boolean") canBuyDevelopmentCard: boolean = false;

    @filter(onlyForCurrentPlayer)
    @type("boolean") canBuildWall: boolean = false;

    @filter(onlyForCurrentPlayer)
    @type("boolean") canBuyKnight: boolean = false;

    @filter(onlyForCurrentPlayer)
    @type("boolean") canPromoteKnight: boolean = false;

    @type("boolean") hasPlayedDevelopmentCard: boolean = false;

    @type("boolean") hasDiscardedThisTurn: boolean = false;

    @filter(function (this: PlayerSchema, client: Client) {
        return this.id === client.userData.playerId;
    })
    @type(ResourcesSchema) resources: ResourcesSchema = new ResourcesSchema(0, 0, 0, 0, 0, 0, 0, 0);
    @type("number") resourcesTotal: number = 0;

    @type("number") receivedResourcesThisTurn: number = 0;
    @type("boolean") hasChosenResourceThisTurn: boolean = false;

    @type("number") settlements = 5;
    @type("number") roads = 15;
    @type("number") cities = 4;
    @type("number") walls = 3;
    @type("number") basicKnights = 2;
    @type("number") strongKnights = 2;
    @type("number") mightyKnights = 2;

    @filter(function (this: PlayerSchema, client: Client) {
        return this.id === client.userData.playerId;
    })
    @type([DevelopmentCardSchema]) developmentCards = new ArraySchema<DevelopmentCardSchema>();
    @type("number") totalDevelopmentCards: number = 0;

    @type("number") victoryPointsVisible = 0;

    @filter(function (this: PlayerSchema, client: Client) {
        return this.id === client.userData.playerId;
    })
    @type("number") victoryPointsHidden = 0;

    @type("number") knights = 0;
    @type("boolean") largestArmy: boolean = false;

    @type([EdgeSchema]) longestRoadPath = new ArraySchema<EdgeSchema>();
    @type("boolean") longestRoad: boolean = false;

    @type({set: "string"}) harbors = new SetSchema<HarborType>();

    @type(CityUpgradeSchema) cityUpgrades: CityUpgradeSchema = new CityUpgradeSchema();

    constructor(id: string) {
        super();
        this.id = id;
        this.name = id;
        this.color = "black";
    }
}

export const PLAYER_COLORS = ["blue", "green", "red", "orange"]