import {Schema, type} from "@colyseus/schema";
import {Resource} from "./ResourcesSchema";
import {VertexLoc} from "./VertexSchema";

export type EdgeLoc = "north" | "east" | "south";

export type HarborType = "three-to-one" | Resource;

export class EdgeSchema extends Schema {
    @type("number") x: number;
    @type("number") y: number;
    @type("string") loc: EdgeLoc;
    @type("string") ownerPlayerId: string = "";
    @type("boolean") canPlace: boolean = false;

    @type("string") harbor: HarborType | undefined;

    constructor(x: number, y: number, loc: EdgeLoc) {
        super();
        this.x = x;
        this.y = y;
        this.loc = loc;
    }
}

export function isSameEdge(e1: EdgeSchema, e2: EdgeSchema) {
    return e1.x === e2.x && e1.y === e2.y && e1.loc === e2.loc
}


export function isEdgeLoc(loc: VertexLoc | EdgeLoc): loc is EdgeLoc {
    return loc === "north" || loc === "east" || loc === "south";
}