import {Schema, SetSchema, type} from "@colyseus/schema";
import {TerrainType} from "./TerrainType";
import {VertexSchema} from "./VertexSchema";
import {EdgeSchema} from "./EdgeSchema";

export class TileSchema extends Schema {
    @type("string") terrainType: TerrainType;
    @type("number") x: number;
    @type("number") y: number;
    @type("number") number: number;

    @type(VertexSchema) top: VertexSchema;
    @type(VertexSchema) bottom: VertexSchema;

    @type(EdgeSchema) north: EdgeSchema;
    @type(EdgeSchema) east: EdgeSchema;
    @type(EdgeSchema) south: EdgeSchema;

    @type({set: "string"}) players = new SetSchema<string>();

    constructor(terrainType: TerrainType, x: number, y: number, number: number) {
        super();
        this.terrainType = terrainType;
        this.x = x;
        this.y = y;
        this.number = number;

        this.top = new VertexSchema(x, y, "top");
        this.bottom = new VertexSchema(x, y, "bottom");

        this.north = new EdgeSchema(x, y, "north");
        this.east = new EdgeSchema(x, y, "east");
        this.south = new EdgeSchema(x, y, "south");
    }
}