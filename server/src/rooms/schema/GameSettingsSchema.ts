import {Schema, type} from "@colyseus/schema";

export interface GameSettings {
    moreDesert: boolean;
    cardDice: boolean;
    addonCitiesAndKnights: boolean;
}

export class GameSettingsSchema extends Schema {
    @type("boolean") moreDesert: boolean = false;
    @type("boolean") cardDice: boolean = false;
    @type("boolean") addonCitiesAndKnights: boolean = false;

    update(settings: GameSettings) {
        this.moreDesert = settings.moreDesert;
        this.cardDice = settings.cardDice;
        this.addonCitiesAndKnights = settings.addonCitiesAndKnights;
    }
}