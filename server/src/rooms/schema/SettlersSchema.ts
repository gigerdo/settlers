import {ArraySchema, filter, MapSchema, Schema, type} from "@colyseus/schema";
import {PlayerSchema} from "./PlayerSchema";
import {BoardSchema} from "./BoardSchema";
import {BankSchema} from "./BankSchema";
import {TradeSchema} from "./TradeSchema";
import {GameSettingsSchema} from "./GameSettingsSchema";
import {DiceCardsSchema} from "./DiceCardsSchema";
import {CityUpgradeColor} from "./CityUpgradeSchema";
import {KnightLevel, KnightSchema} from "./KnightSchema";
import {VertexLoc} from "./VertexSchema";

export type GamePhase =
    "setup"
    | "placing-settlement"
    | "placing-road"
    | "roll-dice"
    | "playing"
    | "place-robber"
    | "road-building-1"
    | "road-building-2"
    | "finished"
    | "dropping-cards"
    | "placing-metropolis"
    | "choosing-resource"
    | "displacing-knight"
    | "destroying-city"
    | "choosing-progresscard"
    | "move-robber-bishop"
    | "removing-knight"
    | "placing-knight";

export type EventDie = "barbarian" | "blue" | "yellow" | "green"

export class SettlersSchema extends Schema {
    @type(GameSettingsSchema) settings: GameSettingsSchema = new GameSettingsSchema();

    @type({map: PlayerSchema}) players = new MapSchema<PlayerSchema>();
    @type(BoardSchema) board = new BoardSchema();
    @type(BankSchema) bank = new BankSchema();

    @type("string") phase: GamePhase = "setup";
    @type(["string"]) playerOrder = new ArraySchema<string>();
    @type("number") currentPlayerIndex: number = 0;
    @type("boolean") reverseOrder: boolean = false;

    @type("number") rolledNumber: number = -1;
    @type("number") yellowDie: number = -1;
    @type("number") redDie: number = -1;
    @type("string") eventDie?: EventDie;

    @type(["number"]) rollStatistics = new ArraySchema<number>(0, 0, 0, 0, 0, 0);
    @type(["number"]) rollStatisticsTwoDice = new ArraySchema<number>(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

    @type("number") nextTradeId: number = 0;
    @type({map: TradeSchema}) openTrades = new MapSchema<TradeSchema>();

    @type("string") winner: string = "";

    // Cities&Knights
    @type("number") barbarianAttacks: number = 0;
    @type("number") barbarianPosition: number = 0;

    // Who owns the metropolis of each color
    @type({map: "string"}) metropolisOwners = new MapSchema<string>();
    @type("string") metropolisBeingPlaced?: CityUpgradeColor;

    @type(KnightSchema) knightBeingDisplaced?: KnightSchema;
    @type("number") knightBeingDisplacedIndex?: number;
    @type("string") knightBeingDisplacedLoc?: VertexLoc;
    @type("string") deserterPlayerId?: string;
    @type("string") placingKnightLevel?: KnightLevel;
    @type("boolean") placingKnightActivated?: boolean;

    @type(["string"]) playersDestroyingCity = new ArraySchema<string>;
    @type(["string"]) playersWinningBarbarianAttack = new ArraySchema<string>;

    @type("boolean") saboteurPlayed: boolean = false;

    @filter(function (): boolean {
        return false;
    })
    @type("number") seed: number = +new Date();

    // https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
    rand() {
        this.seed += 0x6D2B79F5;
        let t = this.seed;
        t = Math.imul(t ^ t >>> 15, t | 1);
        t ^= t + Math.imul(t ^ t >>> 7, t | 61);
        return ((t ^ t >>> 14) >>> 0) / 4294967296;
    }

    @filter(function (): boolean {
        return false;
    })
    @type(DiceCardsSchema) diceCards: DiceCardsSchema = new DiceCardsSchema();
}
