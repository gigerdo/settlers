import {Schema, type} from "@colyseus/schema";
import {Resource} from "./ResourcesSchema";

export type CityUpgradeColor = "yellow" | "blue" | "green";

export class CityUpgradeSchema extends Schema {
    // Levels
    // 0 -> no bonus
    // 1 -> Card on 1,2
    // 2 -> Crd on 1,2,3
    // 3 -> Card on 1,2,3,4 / additional bonus
    // 4 -> Card on 1,2,3,4,5
    // 5 -> Max level, always get card / additional bonus
    @type("number") blue = 0;
    @type("number") yellow = 0;
    @type("number") green = 0;
}

export function colorToResource(color: CityUpgradeColor): Resource {
    switch (color) {
        case "yellow":
            return "cloth";
        case "blue":
            return "coin";
        case "green":
            return "paper";
    }
}