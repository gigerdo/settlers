import {Schema, type} from "@colyseus/schema";

export type KnightLevel = "basic" | "strong" | "mighty";

export function getKnightStrength(level: KnightLevel): 1 | 2 | 3 {
    switch (level) {
        case "basic":
            return 1;
        case "mighty":
            return 2;
        case "strong":
            return 3;
    }
    throw new Error("unknown level");
}

export class KnightSchema extends Schema {
    @type("string") ownerPlayerId: string;
    @type("string") level: KnightLevel;
    @type("boolean") activated: boolean;
    @type("boolean") actionTaken: boolean;
    @type("boolean") promoted: boolean;


    constructor() {
        super();
        this.ownerPlayerId = "";
        this.level = "basic";
        this.activated = false;
        this.actionTaken = false;
        this.promoted = false;
    }
}