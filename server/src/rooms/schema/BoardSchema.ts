import {ArraySchema, Schema, type} from "@colyseus/schema";
import {TileSchema} from "./TileSchema";
import {TerrainType} from "./TerrainType";
import {shuffle} from "./shuffle";
import {VertexLoc, VertexSchema} from "./VertexSchema";
import {EdgeLoc, EdgeSchema, HarborType} from "./EdgeSchema";
import {RobberSchema} from "./RobberSchema";
import {GameSettings} from "./GameSettingsSchema";
import {CubeCoordinates} from "../coordinates/CubeCoordinates";

export class BoardSchema extends Schema {
    @type([TileSchema]) tiles = new ArraySchema<TileSchema>();
    @type(RobberSchema) robber = new RobberSchema(-1, -1);

    // Merchant
    @type("number") merchantIndex?: number = undefined;
    @type("string") merchantOwner?: string = undefined;

    constructor() {
        super();
    }

    setupBoard(settings: GameSettings) {
        let numbers: number[];
        let shuffledTerrain;

        if (settings.moreDesert) {
            numbers = [...reducedNumbers];
            shuffledTerrain = shuffle(reducedTerrain);
        } else {
            numbers = [...availableNumbers];
            shuffledTerrain = shuffle(availableTerrain);
        }

        // Place tiles
        const outerTiles = []
        for (let j = 0; j < 7; j++) {
            for (let i = 0; i < 7; i++) {
                if ((j === 0)
                    || (j === 1 && (i === 0 || i === 1 || i === 5 || i === 6))
                    || (j === 2 && (i === 0 || i === 1 || i === 6))
                    || (j === 3 && (i === 0 || i === 6))
                    || (j === 4 && (i === 0 || i === 1 || i === 6))
                    || (j === 5 && (i === 0 || i === 1 || i === 5 || i === 6))
                    || (j === 6)
                ) {
                    this.tiles.push(new TileSchema("water", i, j, -1))
                } else {
                    let terrain = shuffledTerrain.pop()!;
                    let number = -1;
                    if (terrain === "desert") {
                        this.robber = new RobberSchema(i, j);
                    }
                    this.tiles.push(new TileSchema(terrain, i, j, number));
                }
            }
        }

        // Place numbers
        // Algo
        // - First take random element on outer ring, go counter-clockwise along outer ring placing numbers
        // - Then search closest neighbor on inner ring, go counter clockwise again
        // - Place last number on center
        const center = CubeCoordinates.fromOffsetCoordinates(3, 3); // 3,3 is the center of the map
        const outerRing = center.ring(2);

        // Take random offset to start assigning numbers
        const random = Math.floor(Math.random() * outerRing.length)
        let currentTile = center;
        for (let i = 0; i < outerRing.length; i++) {
            currentTile = outerRing[(random + i) % outerRing.length];
            const {x, y} = currentTile.toOffsetCoordinates();
            const tile = this.getTile(x, y)!;
            if (tile.terrainType !== "desert") {
                tile.number = numbers.shift()!;
            }
        }
        currentTile.subtract(center)

        // Start inner ring on closest neighbor to the last tile placed on the outer ring
        const innerRing = center.ring(1);
        const offset = innerRing.indexOf(innerRing.filter(t => t.distanceTo(currentTile) == 1).pop()!);
        for (let i = 0; i < innerRing.length; i++) {
            currentTile = innerRing[(i + offset) % innerRing.length];
            const {x, y} = currentTile.toOffsetCoordinates();
            const tile = this.getTile(x, y)!;
            if (tile.terrainType !== "desert") {
                tile.number = numbers.shift()!;
            }
        }

        const centerCoord = center.toOffsetCoordinates();
        const centerTile = this.getTile(centerCoord.x, centerCoord.y)!;
        if (centerTile.terrainType !== "desert") {
            centerTile.number = numbers.shift()!;
        }

        // Neighbouring tiles can't have two 6 or 8
        let queue = [...this.tiles];
        while (queue.length > 0) {
            let tile = queue.shift()!;
            let tileNumber = tile.number;
            if (tileNumber === 6 || tileNumber == 8) {
                let neighbours = this.getNeighboursOf(tile.x, tile.y);
                let foundNeighbours = [];
                let swappableNeighbours = [];
                for (let neighbour of neighbours) {
                    if (neighbour.number === 6 || neighbour.number == 8) {
                        // Swap with another tile
                        foundNeighbours.push(neighbour);
                    } else {
                        swappableNeighbours.push(neighbour);
                    }
                }
                if (foundNeighbours.length > 0) {
                    if (swappableNeighbours.length > 0) {
                        let neighborToSwap = shuffle(swappableNeighbours)[0];
                        let neighborNumber = neighborToSwap.number;
                        tile.number = neighborNumber;
                        neighborToSwap.number = tileNumber;
                        queue.push(neighborToSwap);
                    } else {
                        queue.push(tile);
                    }
                }
            }
        }

        // Place harbors
        let shuffledHarbors = shuffle(availableHarbors);
        for (let edge of harborEdges) {
            let tile = this.getTile(edge.x, edge.y)!;
            tile[edge.loc].harbor = shuffledHarbors.shift();
        }
    }

    getTilesOfVertex(index: number, loc: VertexLoc) {
        let ownerTile = this.tiles[index];
        let {x, y} = ownerTile;

        let tiles = [ownerTile];
        if (loc === "top") {
            let topLeft = this.getTopLeftOf(x, y);
            if (topLeft) {
                tiles.push(topLeft)
            }

            let topRight = this.getTopRightOf(x, y)
            if (topRight) {
                tiles.push(topRight)
            }
        } else {
            let bottomLeft = this.getBottomLeftOf(x, y);
            if (bottomLeft) {
                tiles.push(bottomLeft)
            }

            let bottomRight = this.getBottomRightOf(x, y);
            if (bottomRight) {
                tiles.push(bottomRight)
            }
        }
        return tiles;
    }

    getTopLeftOf(x: number, y: number): TileSchema | undefined {
        let topLeftX;
        let topLeftY;
        if (y % 2 === 0) {
            topLeftX = x - 1;
            topLeftY = y - 1;
        } else {
            topLeftX = x;
            topLeftY = y - 1;
        }
        return this.getTile(topLeftX, topLeftY);
    }

    getTopRightOf(x: number, y: number): TileSchema | undefined {
        let topRightX;
        let topRightY;
        if (y % 2 === 0) {
            topRightX = x;
            topRightY = y - 1;
        } else {
            topRightX = x + 1;
            topRightY = y - 1;
        }
        return this.getTile(topRightX, topRightY);
    }

    getRightOf(x: number, y: number): TileSchema | undefined {
        return this.getTile(x + 1, y);
    }

    getBottomRightOf(x: number, y: number): TileSchema | undefined {
        let bottomRightX;
        let bottomRightY;
        if (y % 2 === 0) {
            bottomRightX = x;
            bottomRightY = y + 1;
        } else {
            bottomRightX = x + 1;
            bottomRightY = y + 1;
        }
        return this.getTile(bottomRightX, bottomRightY);
    }

    getBottomLeftOf(x: number, y: number): TileSchema | undefined {
        let bottomLeftX;
        let bottomLeftY;
        if (y % 2 === 0) {
            bottomLeftX = x - 1;
            bottomLeftY = y + 1;
        } else {
            bottomLeftX = x;
            bottomLeftY = y + 1;
        }
        return this.getTile(bottomLeftX, bottomLeftY);
    }

    getTile(x: number, y: number): TileSchema | undefined {
        if (x >= 0 && x < 7 && y >= 0 && y < 7) {
            return this.tiles[y * 7 + x];
        }
        return undefined;
    }

    getTilesOfEdge(x: number, y: number, loc: EdgeLoc): TileSchema[] {
        let ownerTile = this.getTile(x, y)!;

        let tiles = [ownerTile];

        switch (loc) {
            case "north":
                let topRight = this.getTopRightOf(x, y);
                if (topRight) {
                    tiles.push(topRight);
                }
                break;
            case "east":
                let right = this.getRightOf(x, y);
                if (right) {
                    tiles.push(right);
                }
                break;
            case "south":
                let bottomRight = this.getBottomRightOf(x, y);
                if (bottomRight) {
                    tiles.push(bottomRight);
                }
                break;
        }

        return tiles;
    }

    getVerticesOfEdge(x: number, y: number, loc: EdgeLoc): VertexSchema[] {
        let ownerTile = this.getTile(x, y)!;

        switch (loc) {
            case "north": {
                let vertices = [ownerTile.top];
                let topRightOf = this.getTopRightOf(x, y);
                if (topRightOf) {
                    vertices.push(topRightOf.bottom);
                }
                return vertices;
            }
            case "east": {
                let vertices = [];
                let topRight = this.getTopRightOf(x, y);
                if (topRight) {
                    vertices.push(topRight.bottom);
                }
                let bottomRight = this.getBottomRightOf(x, y);
                if (bottomRight) {
                    vertices.push(bottomRight.top);
                }
                return vertices;
            }
            case "south": {
                let vertices = [ownerTile.bottom];
                let bottomRight = this.getBottomRightOf(x, y);
                if (bottomRight) {
                    vertices.push(bottomRight.top);
                }
                return vertices;
            }
        }
    }

    getEdgesOfVertex(x: number, y: number, loc: VertexLoc): EdgeSchema[] {
        let ownerTile = this.getTile(x, y)!;

        let edges = [];
        if (loc === "top") {
            edges.push(ownerTile.north);
            let topLeft = this.getTopLeftOf(x, y);
            if (topLeft) {
                edges.push(topLeft.east);
                edges.push(topLeft.south);
            }
        } else {
            edges.push(ownerTile.south);
            let bottomLeft = this.getBottomLeftOf(x, y);
            if (bottomLeft) {
                edges.push(bottomLeft.north);
                edges.push(bottomLeft.east);
            }
        }
        return edges;
    }

    getNeighboursOf(x: number, y: number): TileSchema[] {
        return [
            this.getTopLeftOf(x, y),
            this.getTopRightOf(x, y),
            this.getRightOf(x, y),
            this.getBottomRightOf(x, y),
            this.getBottomLeftOf(x, y),
            this.getLeftOf(x, y),
        ]
            .filter(t => t !== undefined)
            .filter(t => t!.terrainType !== "desert" && t!.terrainType !== "water") as TileSchema[];
    }

    private getLeftOf(x: number, y: number) {
        return this.getTile(x - 1, y);
    }

    getAllVerticesOfTile(x: number, y: number): VertexSchema[] {
        let tile = this.getTile(x, y)!;
        let vertices = [tile.top, tile.bottom];
        let topLeft = this.getTopLeftOf(x, y);
        if (topLeft) {
            vertices.push(topLeft.bottom);
        }
        let bottomLeft = this.getBottomLeftOf(x, y);
        if (bottomLeft) {
            vertices.push(bottomLeft.top);
        }
        let bottomRight = this.getBottomRightOf(x, y);
        if (bottomRight) {
            vertices.push(bottomRight.top);
        }
        let topRight = this.getTopRightOf(x, y);
        if (topRight) {
            vertices.push(topRight.bottom);
        }
        return vertices;
    }
}

const availableTerrain: TerrainType[] = [
    "lumber",
    "lumber",
    "lumber",
    "lumber",
    "wool",
    "wool",
    "wool",
    "wool",
    "brick",
    "brick",
    "brick",
    "grain",
    "grain",
    "grain",
    "grain",
    "ore",
    "ore",
    "ore",
    "desert",
];

// Numbers are ordered to be distributed in a spiral
const availableNumbers: readonly number[] = [
    5,
    2,
    6,
    3,
    8,
    10,
    9,
    12,
    11,
    4,
    8,
    10,
    9,
    4,
    5,
    6,
    3,
    11,
];

// 3 more deserts for reduced resources
const reducedTerrain: TerrainType[] = [
    "lumber",
    "lumber",
    "lumber",
    "desert",
    "wool",
    "wool",
    "wool",
    "desert",
    "brick",
    "brick",
    "brick",
    "grain",
    "grain",
    "grain",
    "desert",
    "ore",
    "ore",
    "ore",
    "desert",
];

// Remove 3,4,11 when using 4 deserts (3-player mode)
const reducedNumbers: readonly number[] = [
    5,
    2,
    6,
    3,
    8,
    10,
    9,
    12,
    11,
    8,
    10,
    9,
    4,
    5,
    6,
];

const harborEdges: { x: number; y: number; loc: EdgeLoc; }[] = [
    {x: 2, y: 0, loc: "south"},
    {x: 3, y: 1, loc: "north"},
    {x: 5, y: 2, loc: "north"},
    {x: 5, y: 3, loc: "east"},
    {x: 5, y: 4, loc: "south"},
    {x: 3, y: 5, loc: "south"},
    {x: 2, y: 6, loc: "north"},
    {x: 1, y: 4, loc: "east"},
    {x: 1, y: 2, loc: "east"},
];

const availableHarbors: HarborType[] = [
    "three-to-one",
    "three-to-one",
    "three-to-one",
    "three-to-one",
    "lumber",
    "brick",
    "wool",
    "grain",
    "ore",
];