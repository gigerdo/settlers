import {ArraySchema, filter, Schema, type} from "@colyseus/schema";
import {shuffle} from "./shuffle";
import {
    AVAILABLE_POLITICS_CARDS,
    AVAILABLE_SCIENCE_CARDS,
    AVAILABLE_TRADE_CARDS,
    DevelopmentCardSchema,
    DevelopmentCardType,
    PoliticsCardType,
    ProgressCard,
    ScienceCardType,
    TradeCardType,
} from "./DevelopmentCardSchema";
import {PlayerSchema} from "./PlayerSchema";
import {Resource} from "./ResourcesSchema";
import {KnightSchema} from "./KnightSchema";
import {CityUpgradeColor} from "./CityUpgradeSchema";

export class BankSchema extends Schema {
    @type("number") brick: number = 19;
    @type("number") lumber: number = 19;
    @type("number") wool: number = 19;
    @type("number") grain: number = 19;
    @type("number") ore: number = 19;
    @type("number") paper: number = 12;
    @type("number") cloth: number = 12;
    @type("number") coin: number = 12;

    @filter(() => false)
    @type(["string"]) developmentCards: ArraySchema<DevelopmentCardType>;
    @type("number") developmentCardsTotal: number;

    @filter(() => false)
    @type(["string"]) scienceCards: ArraySchema<ScienceCardType>;
    @type("number") scienceCardsTotal: number;

    @filter(() => false)
    @type(["string"]) politicsCards: ArraySchema<PoliticsCardType>;
    @type("number") politicsCardsTotal: number;

    @filter(() => false)
    @type(["string"]) tradeCards: ArraySchema<TradeCardType>;
    @type("number") tradeCardsTotal: number;

    constructor() {
        super();
        this.developmentCards = new ArraySchema(...shuffle(availableDevelopmentCards));
        this.developmentCardsTotal = this.developmentCards.length;

        this.scienceCards = new ArraySchema(...shuffle(AVAILABLE_SCIENCE_CARDS));
        this.scienceCardsTotal = this.scienceCards.length;

        this.politicsCards = new ArraySchema(...shuffle(AVAILABLE_POLITICS_CARDS));
        this.politicsCardsTotal = this.politicsCards.length;

        this.tradeCards = new ArraySchema(...shuffle(AVAILABLE_TRADE_CARDS));
        this.tradeCardsTotal = this.tradeCards.length;
    }

    buyRoad(player: PlayerSchema) {
        player.resources.lumber -= 1;
        player.resources.brick -= 1;
        this.brick += 1;
        this.lumber += 1;
    }

    buySettlement(player: PlayerSchema) {
        player.resources.lumber -= 1;
        player.resources.brick -= 1;
        player.resources.wool -= 1;
        player.resources.grain -= 1;
        this.lumber += 1;
        this.brick += 1;
        this.wool += 1;
        this.grain += 1;
    }

    buyCity(player: PlayerSchema, useMedicineCard: Boolean) {
        if (useMedicineCard) {
            player.resources.grain -= 1;
            player.resources.ore -= 2;
            this.grain += 1;
            this.ore += 2;
        } else {
            player.resources.grain -= 2;
            player.resources.ore -= 3;
            this.grain += 2;
            this.ore += 3;
        }
    }

    buyWall(player: PlayerSchema) {
        player.resources.brick -= 2;
        this.brick += 2;
    }

    buyDevelopmentCard(player: PlayerSchema) {
        player.resources.wool -= 1;
        player.resources.grain -= 1;
        player.resources.ore -= 1;
        this.wool += 1;
        this.grain += 1;
        this.ore += 1;
        let devCard = this.developmentCards.pop()!;
        this.developmentCardsTotal = this.developmentCards.length;
        return devCard;
    }

    buyKnight(player: PlayerSchema) {
        player.resources.wool -= 1;
        player.resources.ore -= 1;
        this.wool += 1;
        this.ore += 1;
        player.basicKnights -= 1;
    }

    takeResource(player: PlayerSchema, resource: Resource) {
        if (this[resource] >= 1) {
            player.resources[resource] += 1;
            this[resource] -= 1;
            player.receivedResourcesThisTurn += 1;
        }
    }

    giveResource(player: PlayerSchema, resource: Resource, amount: number) {
        if (player.resources[resource] >= amount) {
            player.resources[resource] -= amount;
            this[resource] += amount;
        }
    }

    activateKnight(player: PlayerSchema, knight: KnightSchema) {
        player.resources.grain -= 1;
        this.grain += 1;
        knight.activated = true;
        knight.actionTaken = true; // Knight may not take action after being activated
    }

    promoteKnight(player: PlayerSchema, knight: KnightSchema) {
        player.resources.wool -= 1;
        player.resources.ore -= 1;
        this.wool += 1;
        this.ore += 1;
        if (knight.level === "basic") {
            player.basicKnights += 1;
            player.strongKnights -= 1;
            knight.level = "strong";
        } else if (knight.level === "strong") {
            player.mightyKnights -= 1;
            player.strongKnights += 1;
            knight.level = "mighty";
        } else {
            throw new Error(`unknown level ${knight}`);
        }
        knight.promoted = true;
    }

    pickProgressCard(player: PlayerSchema, color: CityUpgradeColor) {
        let card: ProgressCard | undefined = undefined;
        switch (color) {
            case "green":
                card = this.scienceCards.pop();
                this.scienceCardsTotal = this.scienceCards.length;
                break;
            case "blue":
                card = this.politicsCards.pop();
                this.politicsCardsTotal = this.politicsCards.length;
                break;
            case "yellow":
                card = this.tradeCards.pop();
                this.tradeCardsTotal = this.tradeCards.length;
                break;
        }
        if (card !== undefined) {
            if (card === "constitution" || card === "printer") {
                player.victoryPointsVisible += 1;
            }
            player.developmentCards.push(new DevelopmentCardSchema(card, color));
        }

        return card;
    }
}

const availableDevelopmentCards: DevelopmentCardType[] = [
    "knight",
    "knight",
    "knight",
    "knight",
    "knight",
    "knight",
    "knight",
    "knight",
    "knight",
    "knight",
    "knight",
    "knight",
    "knight",
    "knight",
    "victory-point",
    "victory-point",
    "victory-point",
    "victory-point",
    "victory-point",
    "road-building",
    "road-building",
    "year-of-plenty",
    "year-of-plenty",
    "monopoly",
    "monopoly",
];
