import {Schema, type} from "@colyseus/schema";

const resources = ["ore", "lumber", "grain", "wool", "brick", "paper", "cloth", "coin"] as const;
export const citiesAndKnightsResources = ["paper", "cloth", "coin"] as const;
export type Resource = (typeof resources)[number];
export type Commodity = (typeof citiesAndKnightsResources)[number]

export function isResource(resource: string): resource is Resource {
    return resources.indexOf(resource as Resource) >= 0;
}

export function isCommodity(resource: string): resource is Resource {
    return citiesAndKnightsResources.indexOf(resource as Commodity) >= 0;
}

export class ResourcesSchema extends Schema {
    @type("number") lumber: number;
    @type("number") brick: number;
    @type("number") wool: number;
    @type("number") grain: number;
    @type("number") ore: number;
    @type("number") paper: number;
    @type("number") cloth: number;
    @type("number") coin: number;

    constructor(lumber: number, brick: number, wool: number, grain: number, ore: number, paper: number, cloth: number, coin: number) {
        super();
        this.lumber = lumber;
        this.brick = brick;
        this.grain = grain;
        this.ore = ore;
        this.wool = wool;
        this.paper = paper;
        this.cloth = cloth;
        this.coin = coin;
    }
}