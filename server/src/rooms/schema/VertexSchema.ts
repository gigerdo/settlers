import {Schema, type} from "@colyseus/schema";
import {CityUpgradeColor} from "./CityUpgradeSchema";
import {KnightSchema} from "./KnightSchema";
import {EdgeLoc} from "./EdgeSchema";

export type VertexLoc = "top" | "bottom";

export class VertexSchema extends Schema {
    @type("number") x: number;
    @type("number") y: number;
    @type("string") loc: VertexLoc;

    @type("string") ownerPlayerId: string;
    @type("boolean") city: boolean;
    @type("boolean") canPlace: boolean;
    @type("boolean") canPlaceKnight: boolean;
    @type("boolean") wall: boolean;
    @type("string") metropolis?: CityUpgradeColor;
    @type(KnightSchema) knight?: KnightSchema;

    constructor(x: number, y: number, loc: VertexLoc) {
        super();
        this.x = x;
        this.y = y;
        this.loc = loc;
        this.ownerPlayerId = "";
        this.city = false;
        this.canPlace = false;
        this.canPlaceKnight = false;
        this.wall = false;
    }
}

export function isVertexLoc(loc: VertexLoc | EdgeLoc): loc is VertexLoc {
    return loc === "top" || loc === "bottom";
}