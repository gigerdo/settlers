import {Schema, type} from "@colyseus/schema";
import {CityUpgradeColor} from "./CityUpgradeSchema";

export class DevelopmentCardSchema extends Schema {
    @type("string") type: SpecialCards;
    @type("boolean") sameTurn: boolean = true;
    @type("string") color?: CityUpgradeColor;

    constructor(type: SpecialCards, color?: CityUpgradeColor) {
        super();
        this.type = type;
        this.color = color;
        if (this.color !== undefined) {
            this.sameTurn = false;
        }
    }
}

export type SpecialCards = DevelopmentCardType | ProgressCard;

export type DevelopmentCardType = "knight" | "victory-point" | "road-building" | "year-of-plenty" | "monopoly";

export type ProgressCard = TradeCardType | ScienceCardType | PoliticsCardType;

export type TradeCardType = (typeof AVAILABLE_TRADE_CARDS)[number]

export function isTradeCard(card: ProgressCard): card is TradeCardType {
    return AVAILABLE_TRADE_CARDS.includes(card as TradeCardType);
}

export type ScienceCardType = (typeof AVAILABLE_SCIENCE_CARDS)[number]

export function isScienceCard(card: ProgressCard): card is ScienceCardType {
    return AVAILABLE_SCIENCE_CARDS.includes(card as ScienceCardType);
}

export type PoliticsCardType = (typeof AVAILABLE_POLITICS_CARDS)[number]

export function isPoliticsCard(card: ProgressCard): card is PoliticsCardType {
    return AVAILABLE_POLITICS_CARDS.includes(card as PoliticsCardType);
}

export const AVAILABLE_SCIENCE_CARDS = [
    "alchemist",
    "alchemist",
    "crane",
    "crane",
    "engineer",
    // "inventor",
    // "inventor",
    "irrigation",
    "irrigation",
    "medicine",
    "medicine",
    "mining",
    "mining",
    "printer",
    "road-building",
    "road-building",
    // "smith",
    // "smith",
] as const;

export const AVAILABLE_POLITICS_CARDS = [
    "bishop",
    "bishop",
    "constitution",
    "deserter",
    "deserter",
    "diplomat",
    "diplomat",
    "intrigue",
    "intrigue",
    "saboteur",
    "saboteur",
    // "spy",
    // "spy",
    // "spy",
    // "warlord",
    // "warlord",
    // "wedding",
    // "wedding",
] as const;

export const AVAILABLE_TRADE_CARDS = [
    // "commercial-harbor",
    // "commercial-harbor",
    // "master-merchant",
    // "master-merchant",
    "merchant",
    "merchant",
    "merchant",
    "merchant",
    "merchant",
    "merchant",
    // "merchant-fleet",
    // "merchant-fleet",
    "resource-monopoly",
    "resource-monopoly",
    "resource-monopoly",
    "resource-monopoly",
    "trade-monopoly",
    "trade-monopoly",
] as const;