import {Schema, type} from "@colyseus/schema";

export class DiceCardSchema extends Schema {
    @type("number") roll1: number = -1;
    @type("number") roll2: number = -1;

    constructor(roll1: number, roll2: number) {
        super();
        this.roll1 = roll1;
        this.roll2 = roll2;
    }
}