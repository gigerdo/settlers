import {shuffle} from "./shuffle";
import {ArraySchema, Schema, type} from "@colyseus/schema";
import {DiceCardSchema} from "./DiceCardSchema";

export class DiceCardsSchema extends Schema {
    private fullDeck: DiceCardSchema[];
    @type([DiceCardSchema]) currentDeck = new ArraySchema<DiceCardSchema>();

    constructor() {
        super();
        this.fullDeck = [];
        for (let i = 1; i <= 6; i++) {
            for (let j = 1; j <= 6; j++) {
                this.fullDeck.push(new DiceCardSchema(i, j));
            }
        }
    }

    shuffleDeck() {
        let shuffledCards = shuffle(this.fullDeck)
            .slice(5); // Remove the last 5 cards
        this.currentDeck = new ArraySchema(...shuffledCards);
    }

    nextCard(): DiceCardSchema {
        if (this.currentDeck.length === 0) {
            this.shuffleDeck();
        }
        return this.currentDeck.pop()!;
    }
}

