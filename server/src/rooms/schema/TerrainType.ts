export type TerrainType = "desert" | "water" | UsefulTerrain;


type Narrowable = string | number | boolean | symbol |
    object | {} | void | null | undefined;
const tuple = <T extends Narrowable[]>(...args: T) => args;

const usefulTerrain = ["ore", "lumber", "grain", "wool", "brick"] as const;
export type UsefulTerrain = (typeof usefulTerrain)[number];

export function isUsefulTerrain(terrain: TerrainType): terrain is UsefulTerrain {
    return usefulTerrain.indexOf(terrain as UsefulTerrain) >= 0;
}
