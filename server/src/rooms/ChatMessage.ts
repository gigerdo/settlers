export interface ChatMessage {
    player: string;
    color: string;
    message: string;
}