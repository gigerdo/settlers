import {TileSchema} from "./schema/TileSchema";
import {SettlersSchema} from "./schema/SettlersSchema";
import {VertexLoc} from "./schema/VertexSchema";

export function updateCanBuildSettlement(state: SettlersSchema, playerId: string) {
    let updateCanBuildSettlement = (tile: TileSchema, loc: VertexLoc) => {

        let hasNeighbouringRoad = false;
        let hasNeighbouringSettlement = false;
        let edges = state.board.getEdgesOfVertex(tile.x, tile.y, loc);
        for (let edge of edges) {
            if (edge.ownerPlayerId === playerId) {
                hasNeighbouringRoad = true;
            }
            let vertices = state.board.getVerticesOfEdge(edge.x, edge.y, edge.loc);
            for (let vertex of vertices) {
                if (vertex.ownerPlayerId !== "") {
                    hasNeighbouringSettlement = true;
                    break;
                }
            }
        }
        tile[loc].canPlace = !hasNeighbouringSettlement && hasNeighbouringRoad && tile[loc].knight === undefined;

        if (state.settings.addonCitiesAndKnights) {
            tile[loc].canPlaceKnight = hasNeighbouringRoad && tile[loc].ownerPlayerId === "" && tile[loc].knight === undefined;
        }
    }

    for (let tile of state.board.tiles) {
        updateCanBuildSettlement(tile, "top");
        updateCanBuildSettlement(tile, "bottom");
    }
}