import {updatePlayers} from "../updatePlayers";
import {VertexLoc} from "../schema/VertexSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class BuildMetropolisCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: VertexLoc;

    constructor(clientId: string, index: number, loc: VertexLoc) {
        super("BuildMetropolisCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
    }

    validate(): boolean {
        if (!this.state.settings.addonCitiesAndKnights) {
            return false;
        }

        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let vertex = this.state.board.tiles[this.index][this.loc];
        let ownCity = vertex.ownerPlayerId === this.playerId && vertex.city;
        let noWall = !vertex.wall;
        let player = this.state.players.get(this.playerId)!;

        return this.state.phase === "placing-metropolis"
            && isPlayersTurn
            && ownCity
            && vertex.metropolis == undefined;
    }

    tracedExecute() {
        let tile = this.state.board.tiles[this.index];
        tile[this.loc].metropolis = this.state.metropolisBeingPlaced;
        this.state.metropolisBeingPlaced = undefined;
        this.state.phase = "playing";

        updatePlayers(this.state);
    }
}