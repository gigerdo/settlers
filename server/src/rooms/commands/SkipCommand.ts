import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {updatePlayers} from "../updatePlayers";
import {GamePhase} from "../schema/SettlersSchema";

export class SkipCommand extends TracedSettlersCommand {
    private playerId: string;

    constructor(playerId: string) {
        super("SkipCommand");
        this.playerId = playerId;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let allowedPhases: GamePhase[] = ["placing-knight", "road-building-2"];
        return isPlayersTurn && allowedPhases.some(p => p === this.state.phase);
    }

    tracedExecute() {
        if (this.state.phase === "placing-knight") {
            this.state.placingKnightActivated = undefined;
            this.state.placingKnightLevel = undefined;
            this.state.deserterPlayerId = undefined;
        }
        if (this.state.phase === "road-building-2") {
            // nothing to do
        }

        this.state.phase = "playing";

        updatePlayers(this.state);
    }
}