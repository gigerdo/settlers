import {updateCanBuildRoad} from "../updateCanBuildRoad";
import {updatePlayers} from "../updatePlayers";
import {updateCanBuildSettlement} from "../updateCanBuildSettlement";
import {EdgeLoc} from "../schema/EdgeSchema";
import {updateLongestRoad} from "../updateLongestRoad";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class BuildRoadCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: EdgeLoc;

    constructor(clientId: string, index: number, loc: EdgeLoc) {
        super("BuildRoadCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let edge = this.state.board.tiles[this.index][this.loc];
        let noRoadPlaced = edge.ownerPlayerId === "";
        let player = this.state.players.get(this.playerId)!;

        if (this.state.phase === "road-building-1" || this.state.phase === "road-building-2") {
            return isPlayersTurn && noRoadPlaced && edge.canPlace;
        } else {
            return (this.state.phase === "playing")
                && isPlayersTurn && noRoadPlaced && edge.canPlace
                && player.resources.brick >= 1 && player.resources.lumber >= 1 && player.roads > 0;
        }
    }

    tracedExecute() {
        let tile = this.state.board.tiles[this.index];
        tile[this.loc].ownerPlayerId = this.playerId;
        tile[this.loc].canPlace = false;

        let player = this.state.players.get(this.playerId)!;
        player.roads -= 1;
        if (this.state.phase === "playing") {
            this.state.bank.buyRoad(player);
        } else if (this.state.phase === "road-building-1") {
            this.state.phase = "road-building-2";
        } else if (this.state.phase === "road-building-2") {
            if (this.state.rolledNumber == -1) {
                this.state.phase = "roll-dice";
            } else {
                this.state.phase = "playing";
            }
        }

        updatePlayers(this.state);
        updateCanBuildRoad(this.state, this.playerId);
        updateCanBuildSettlement(this.state, this.playerId);
        updateLongestRoad(this.state);
    }
}

