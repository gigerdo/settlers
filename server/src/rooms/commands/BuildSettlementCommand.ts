import {updateCanBuildRoad} from "../updateCanBuildRoad";
import {updatePlayers} from "../updatePlayers";
import {updateCanBuildSettlement} from "../updateCanBuildSettlement";
import {updateLongestRoad} from "../updateLongestRoad";
import {VertexLoc} from "../schema/VertexSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";


export class BuildSettlementCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: VertexLoc;

    constructor(clientId: string, index: number, loc: VertexLoc) {
        super("BuildSettlementCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let vertex = this.state.board.tiles[this.index][this.loc];
        let noSettlementPlaced = vertex.ownerPlayerId === "" && vertex.knight === undefined;
        let player = this.state.players.get(this.playerId)!;
        return this.state.phase === "playing" && isPlayersTurn && noSettlementPlaced && vertex.canPlace
            && player.resources.brick >= 1 && player.resources.lumber >= 1 && player.resources.wool >= 1
            && player.resources.grain >= 1 && player.settlements > 0;
    }

    tracedExecute() {
        let tile = this.state.board.tiles[this.index];
        tile[this.loc].ownerPlayerId = this.playerId;
        tile[this.loc].canPlace = false;

        let player = this.state.players.get(this.playerId)!;
        this.state.bank.buySettlement(player);
        player.settlements -= 1;
        player.victoryPointsVisible += 1;

        let tilesOfVertex = this.state.board.getTilesOfVertex(this.index, this.loc);
        for (let surroundingTile of tilesOfVertex) {
            surroundingTile.players.add(this.playerId);
        }

        let edgesOfVertex = this.state.board.getEdgesOfVertex(tile.x, tile.y, this.loc);
        for (let edge of edgesOfVertex) {
            if (edge.harbor) {
                player.harbors.add(edge.harbor);
            }
        }

        updatePlayers(this.state);
        updateCanBuildRoad(this.state, this.playerId);
        updateCanBuildSettlement(this.state, this.playerId);
        updateLongestRoad(this.state);
    }
}