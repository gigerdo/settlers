import {checkPlayerHasNecessaryResources} from "../checkPlayerHasNecessaryResources";
import {PlayerSchema} from "../schema/PlayerSchema";
import {Resource, ResourcesSchema} from "../schema/ResourcesSchema";
import {updatePlayers} from "../updatePlayers";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class ExecuteTradeCommand extends TracedSettlersCommand {
    private playerId: string;
    private tradeOfferId: string;
    private otherPlayerId: string;

    constructor(clientId: string, tradeOfferId: string, otherPlayerId: string) {
        super("ExecuteTradeCommand");
        this.playerId = clientId;
        this.tradeOfferId = tradeOfferId;
        this.otherPlayerId = otherPlayerId;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;

        let player = this.state.players.get(this.playerId)!;
        let otherPlayer = this.state.players.get(this.otherPlayerId)!;
        let tradeOffer = this.state.openTrades.get(this.tradeOfferId)!;
        let playerHasResources = checkPlayerHasNecessaryResources(player, tradeOffer.give);
        let otherPlayerHasResources = checkPlayerHasNecessaryResources(otherPlayer, tradeOffer.receive);

        return this.state.phase === "playing" && isPlayersTurn && playerHasResources && otherPlayerHasResources;
    }

    tracedExecute() {
        console.log("Command: New Trade Offer");
        let tradeOffer = this.state.openTrades.get(this.tradeOfferId);
        if (tradeOffer) {
            this.state.openTrades.delete(this.tradeOfferId);
            let player = this.state.players.get(this.playerId)!;
            let otherPlayer = this.state.players.get(this.otherPlayerId)!;

            this.exchangeResources(player, otherPlayer, tradeOffer.give);
            this.exchangeResources(otherPlayer, player, tradeOffer.receive);
        }

        updatePlayers(this.state);
    }

    exchangeResources(playerA: PlayerSchema, playerB: PlayerSchema, resources: ResourcesSchema) {
        Object.entries(resources)
            .forEach(([key, value]) => {
                playerA.resources[key as Resource] -= value;
                playerB.resources[key as Resource] += value;
            });
    }
}