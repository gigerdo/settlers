import {updatePlayers} from "../updatePlayers";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {VertexLoc} from "../schema/VertexSchema";
import {updateCanBuildRoad} from "../updateCanBuildRoad";
import {updateCanBuildSettlement} from "../updateCanBuildSettlement";
import {updateLongestRoad} from "../updateLongestRoad";
import {KnightSchema} from "../schema/KnightSchema";

export class BuyKnightCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: VertexLoc;

    constructor(clientId: string, index: number, loc: VertexLoc) {
        super("BuyKnightCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
    }

    validate(): boolean {
        let player = this.state.players.get(this.playerId)!;
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let vertex = this.state.board.tiles[this.index][this.loc];

        if (this.state.phase === "placing-knight") {
            // Placing free knight from deserter card
            return isPlayersTurn && vertex.canPlaceKnight;
        }

        return isPlayersTurn && this.state.phase === "playing"
            && player.resources["wool"] >= 1 && player.resources["ore"] >= 1
            && player.basicKnights > 0
            && vertex.canPlaceKnight;
    }

    tracedExecute() {
        let tile = this.state.board.tiles[this.index];

        let knight = new KnightSchema();
        knight.ownerPlayerId = this.playerId;
        tile[this.loc].knight = knight;

        let player = this.state.players.get(this.playerId)!;

        if (this.state.phase === "placing-knight") {
            knight.activated = this.state.placingKnightActivated ?? false;
            switch (this.state.placingKnightLevel) {
                case "mighty":
                    if (player.mightyKnights > 0) {
                        player.mightyKnights -= 1;
                        knight.level = "mighty";
                        break;
                    }
                case "strong":
                    if (player.strongKnights > 0) {
                        player.strongKnights -= 1;
                        knight.level = "strong";
                        break;
                    }
                case "basic":
                    if (player.basicKnights > 0) {
                        player.basicKnights -= 1;
                        knight.level = "basic";
                    } else {
                        // There are no knights available, no knight should be placed
                        tile[this.loc].knight = undefined;
                    }
            }

            this.state.placingKnightLevel = undefined;
            this.state.placingKnightActivated = undefined;
            this.state.phase = "playing";
        } else {
            this.state.bank.buyKnight(player);
        }

        updatePlayers(this.state);
        updateCanBuildRoad(this.state, this.playerId);
        updateCanBuildSettlement(this.state, this.playerId);
        updateLongestRoad(this.state);
    }

}

