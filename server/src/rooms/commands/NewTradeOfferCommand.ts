import {NewTradeState, TradeSchema} from "../schema/TradeSchema";
import {checkPlayerHasNecessaryResources} from "../checkPlayerHasNecessaryResources";
import {updatePlayers} from "../updatePlayers";
import {Client} from "colyseus";
import {citiesAndKnightsResources, Resource} from "../schema/ResourcesSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class NewTradeOfferCommand extends TracedSettlersCommand {
    private playerId: string;
    private tradeOffer: TradeSchema;
    private client: Client;

    constructor(clientId: string, tradeOffer: NewTradeState, client: Client) {
        super("NewTradeOfferCommand");
        this.playerId = clientId;
        this.client = client;
        this.tradeOffer = new TradeSchema("none", tradeOffer);
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let player = this.state.players.get(this.playerId)!;

        let citiesAndKnightsCheck = true;
        if (!this.state.settings.addonCitiesAndKnights) {
            citiesAndKnightsCheck = citiesAndKnightsResources
                .map(resource => this.tradeOffer.receive[resource] == 0)
                .reduce((a: boolean, b: boolean) => a && b, true);
        }

        return this.state.phase === "playing" && isPlayersTurn
            && checkPlayerHasNecessaryResources(player, this.tradeOffer.give)
            && citiesAndKnightsCheck;
    }

    tracedExecute() {
        console.log("Command: New Trade Offer");
        let player = this.state.players.get(this.playerId)!;
        let giveResources = Object.entries(this.tradeOffer.give)
            .filter(([key, value]) => value > 0) as [Resource, number][];
        let takeResources = Object.entries(this.tradeOffer.receive)
            .filter(([key, value]) => value > 0) as [Resource, number][];

        let hasMerchant = false;
        if (
            this.state.settings.addonCitiesAndKnights
            && this.state.board.merchantOwner === this.playerId
            && this.state.board.merchantIndex !== undefined
        ) {
            let tile = this.state.board.tiles[this.state.board.merchantIndex];
            let resource = tile.terrainType;
            if (giveResources.length === 1 && giveResources[0][0] === resource) {
                hasMerchant = true;
            }
        }

        let tradeTwoToOne = giveResources.length === 1 && giveResources[0][1] === 2
            && takeResources.length == 1 && takeResources[0][1] === 1
            && (player.harbors.has(giveResources[0][0]) || hasMerchant);
        let tradeThreeToOne = giveResources.length === 1 && giveResources[0][1] === 3
            && takeResources.length == 1 && takeResources[0][1] === 1
            && player.harbors.has("three-to-one");
        let tradeWithBank = giveResources.length === 1 && giveResources[0][1] === 4
            && takeResources.length == 1 && takeResources[0][1] === 1;

        // Can trade commodities 2:1 for commodity or resource
        // if trading-house is built (yellow upgrade on lvl 3)
        let tradeWithTradingHouse = this.state.settings.addonCitiesAndKnights
            && player.cityUpgrades.yellow >= 3
            && giveResources.length == 1 && giveResources[0][1] === 2
            && takeResources.length == 1 && takeResources[0][1] === 1
            && ["paper", "cloth", "coin"].includes(giveResources[0][0]);

        if (tradeTwoToOne) {
            let giveResource = giveResources[0][0];
            let takeResource = takeResources[0][0];

            if (this.state.bank[takeResource] >= 1) {
                this.state.bank[takeResource] -= 1;
                player.resources[takeResource] += 1;
                player.resources[giveResource] -= 2;
                this.state.bank[giveResource] += 2;

                if (hasMerchant) {
                    this.client.send("trade", "traded-two-to-one-merchant");
                } else {
                    this.client.send("trade", "traded-two-to-one");
                }
            }
        } else if (tradeThreeToOne) {
            let giveResource = giveResources[0][0];
            let takeResource = takeResources[0][0];

            if (this.state.bank[takeResource] >= 1) {
                this.state.bank[takeResource] -= 1;
                player.resources[takeResource] += 1;
                player.resources[giveResource] -= 3;
                this.state.bank[giveResource] += 3;

                this.client.send("trade", "traded-three-to-one");
            }
        } else if (tradeWithBank) {
            let giveResource = giveResources[0][0];
            let takeResource = takeResources[0][0];

            if (this.state.bank[takeResource] >= 1) {
                this.state.bank[takeResource] -= 1;
                player.resources[takeResource] += 1;
                player.resources[giveResource] -= 4;
                this.state.bank[giveResource] += 4;

                this.client.send("trade", "traded-with-bank");
            }
        } else if (tradeWithTradingHouse) {
            let giveResource = giveResources[0][0];
            let takeResource = takeResources[0][0];

            if (this.state.bank[takeResource] >= 1) {
                this.state.bank[takeResource] -= 1;
                player.resources[takeResource] += 1;
                player.resources[giveResource] -= 2;
                this.state.bank[giveResource] += 2;

                this.client.send("trade", "traded-with-trading-house");
            }
        } else {
            let tradeId = `trade-${this.state.nextTradeId}`;
            this.state.nextTradeId += 1;
            this.tradeOffer.id = tradeId;
            this.state.openTrades.set(this.tradeOffer.id, this.tradeOffer);
        }

        updatePlayers(this.state);
    }
}