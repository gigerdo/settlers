import {GamePhase, SettlersSchema} from "../../schema/SettlersSchema";
import {
    isPoliticsCard,
    isScienceCard,
    isTradeCard,
    ProgressCard,
    SpecialCards,
} from "../../schema/DevelopmentCardSchema";
import {PlayerSchema} from "../../schema/PlayerSchema";

export function progressCardBaseValidation(
    state: SettlersSchema,
    playerId: string,
    card: ProgressCard,
    expectedPhase: GamePhase = "playing",
) {
    const isCitiesAndKnights = state.settings.addonCitiesAndKnights;
    const playerIndex = state.playerOrder.indexOf(playerId);
    const isPlayersTurn = state.currentPlayerIndex === playerIndex;
    const player = state.players.get(playerId)!;
    const playerHasCard = getCardIndex(player, card) >= 0;
    return isCitiesAndKnights && state.phase === expectedPhase && isPlayersTurn && playerHasCard;
}

export function moveProgressCardBackToBank(state: SettlersSchema, playerId: string, card: ProgressCard) {
    const player = state.players.get(playerId)!;
    const cardIndex = getCardIndex(player, card);
    player.developmentCards.splice(cardIndex, 1);

    if (isTradeCard(card)) {
        state.bank.tradeCards.unshift(card);
        state.bank.tradeCardsTotal = state.bank.tradeCards.length;
    } else if (isScienceCard(card)) {
        state.bank.scienceCards.unshift(card);
        state.bank.scienceCardsTotal = state.bank.scienceCards.length;
    } else if (isPoliticsCard(card)) {
        state.bank.politicsCards.unshift(card);
        state.bank.politicsCardsTotal = state.bank.politicsCards.length;
    } else {
        throw Error("Unknown card type: " + card);
    }
}

function getCardIndex(player: PlayerSchema, card: SpecialCards) {
    return player.developmentCards.findIndex(c => c.type === card);
}