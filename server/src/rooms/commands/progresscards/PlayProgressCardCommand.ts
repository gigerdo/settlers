import {moveProgressCardBackToBank, progressCardBaseValidation} from "./progessCardUtil";
import {updatePlayers} from "../../updatePlayers";
import {ProgressCard} from "../../schema/DevelopmentCardSchema";
import {isCommodity, isResource, Resource} from "../../schema/ResourcesSchema";
import {isVertexLoc, VertexLoc} from "../../schema/VertexSchema";
import {calculateVerticesTheKnightCanMoveTo} from "../MoveKnightCommand";
import {EdgeLoc, isEdgeLoc} from "../../schema/EdgeSchema";
import {BoardSchema} from "../../schema/BoardSchema";
import {updateCanBuildRoad} from "../../updateCanBuildRoad";
import {isUsefulTerrain} from "../../schema/TerrainType";
import {TracedSettlersCommand} from "../TracedSettlersCommand";

/**
 * Player receives 2 ore cards for every ore hex he has a settlement on.
 * It does not matter how many settlements or cities are on the hex (Always only 2 per hex).
 */
export class PlayProgressCardCommand extends TracedSettlersCommand {
    playerId: string;
    private card: ProgressCard;
    private resource?: Resource;
    private targetPlayerId?: string;
    private index?: number;
    private loc?: VertexLoc | EdgeLoc;

    constructor(
        playerId: string,
        card: ProgressCard,
        resource?: Resource,
        targetPlayerId?: string,
        index?: number,
        loc?: VertexLoc | EdgeLoc,
    ) {
        super("PlayProgressCardCommand");
        this.playerId = playerId;
        this.card = card;
        this.resource = resource;
        this.targetPlayerId = targetPlayerId;
        this.index = index;
        this.loc = loc;
    }


    validate(): boolean {
        let baseValidation = progressCardBaseValidation(this.state, this.playerId, this.card);

        switch (this.card) {
            default:
                return baseValidation;

            // Science
            case "alchemist":
                return progressCardBaseValidation(this.state, this.playerId, this.card, "roll-dice");

            case "resource-monopoly":
                return baseValidation && this.resource !== undefined
                    && isResource(this.resource) && !isCommodity(this.resource);

            case "trade-monopoly":
                return baseValidation && this.resource !== undefined && isCommodity(this.resource);

            case "bishop":
                return baseValidation;

            case "deserter":
                let knightsOfTarget = [...this.state.board.tiles]
                    .flatMap(tile => [tile.bottom, tile.top])
                    .filter(v => v.knight && v.knight.ownerPlayerId === this.targetPlayerId);
                return baseValidation && this.targetPlayerId !== undefined && this.targetPlayerId !== "" && knightsOfTarget.length > 0;

            case "intrigue":
                if (this.index === undefined || this.loc === undefined || !isVertexLoc(this.loc)) {
                    return false;
                }
                let vertex = this.state.board.tiles[this.index][this.loc];
                let knight = vertex.knight;
                if (knight === undefined) {
                    return false;
                }
                let knightBordersPlayer = this.state.board.getEdgesOfVertex(vertex.x, vertex.y, this.loc).some(e => e.ownerPlayerId === this.playerId);
                return baseValidation && knightBordersPlayer && knight.ownerPlayerId !== this.playerId;

            case "diplomat":
                if (this.index === undefined || this.loc === undefined || !isEdgeLoc(this.loc)) {
                    return false;
                }
                return baseValidation && checkIfRoadCanBeRemoved(this.state.board, this.index, this.loc);

            case "saboteur":
                let player = this.state.players.get(this.playerId)!;
                let playersVp = player.victoryPointsVisible + player.victoryPointsHidden;
                let playersWithMoreVp = Array.from(this.state.players.values())
                    .filter(p => p.id != player.id)
                    .filter(p => p.victoryPointsVisible + p.victoryPointsHidden >= playersVp);
                return baseValidation && playersWithMoreVp.length > 0;

            // Trade
            case "merchant":
                let tile = this.state.board.tiles[this.index!];
                return baseValidation && isUsefulTerrain(tile.terrainType);
        }
    }

    tracedExecute() {
        console.log(`Progress card: ${this.card}`);

        switch (this.card) {
            // Science
            case "irrigation":
                this.collect2PerHex("grain");
                break;
            case "mining":
                this.collect2PerHex("ore");
                break;
            case "road-building":
                this.state.phase = "road-building-1";
                break;

            // Politics

            case "bishop":
                this.state.phase = "move-robber-bishop";
                break;
            case "deserter":
                this.state.phase = "removing-knight";
                this.state.deserterPlayerId = this.targetPlayerId;
                break;
            case "intrigue":
                let vertex = this.state.board.tiles[this.index!][this.loc as VertexLoc];
                let verticesTheKnightCanMoveTo = calculateVerticesTheKnightCanMoveTo(this.state, vertex.knight!, vertex, false);
                if (verticesTheKnightCanMoveTo.length > 0) {
                    // Allow player to move his knight
                    this.state.phase = "displacing-knight";
                    this.state.knightBeingDisplaced = vertex.knight;
                    this.state.knightBeingDisplacedIndex = this.index;
                    this.state.knightBeingDisplacedLoc = this.loc as VertexLoc;
                } else {
                    // Knight can't move anywhere, remove it from board
                    this.state.players.get(vertex.knight!.ownerPlayerId)!.basicKnights += 1;
                }
                vertex.knight = undefined;
                break;
            case "diplomat":
                let edge = this.state.board.tiles[this.index!][this.loc as EdgeLoc];
                let ownerPlayerId = edge.ownerPlayerId;
                let ownerPlayer = this.state.players.get(ownerPlayerId)!;
                ownerPlayer.roads += 1;
                edge.ownerPlayerId = "";
                if (ownerPlayerId === this.playerId) {
                    this.state.phase = "road-building-2";
                }
                break;
            case "saboteur":
                this.state.phase = "dropping-cards";
                this.state.saboteurPlayed = true;
                break;

            // Trade
            case "resource-monopoly":
                this.resourceMonopoly();
                break;

            case "trade-monopoly":
                this.tradeMonopoly();
                break;

            case "merchant":
                if (this.state.board.merchantOwner !== undefined) {
                    let previousOwner = this.state.players.get(this.state.board.merchantOwner);
                    previousOwner!.victoryPointsVisible -= 1;
                }
                this.state.board.merchantIndex = this.index;
                this.state.board.merchantOwner = this.playerId;
                let player = this.state.players.get(this.playerId)!;
                player!.victoryPointsVisible += 1;
                break;
        }

        moveProgressCardBackToBank(this.state, this.playerId, this.card);

        updatePlayers(this.state);
        updateCanBuildRoad(this.state, this.playerId);
    }

    private collect2PerHex(resource: Resource) {
        const player = this.state.players.get(this.playerId)!;
        [...this.state.board.tiles]
            .filter(tile => tile.terrainType == resource)
            .filter(tile => this.state.board.getAllVerticesOfTile(tile.x, tile.y)
                .some(vertex => vertex.ownerPlayerId === this.playerId))
            .flatMap(() => [1, 1])
            .forEach(() => {
                this.state.bank.takeResource(player, resource);
            });
    }

    private resourceMonopoly() {
        if (this.resource === undefined) {
            return;
        }
        let allPlayers = Array.from(this.state.players.values());
        let player = this.state.players.get(this.playerId)!;
        let stolenGoods = 0;
        for (let otherPlayer of allPlayers) {
            if (player.id === otherPlayer.id) {
                continue;
            }
            let toSteal = Math.min(otherPlayer.resources[this.resource], 2);
            stolenGoods += toSteal;
            otherPlayer.resources[this.resource] -= toSteal;
        }
        player.resources[this.resource] += stolenGoods;
    }

    private tradeMonopoly() {
        if (this.resource === undefined) {
            return;
        }
        let allPlayers = Array.from(this.state.players.values());
        let player = this.state.players.get(this.playerId)!;
        let stolenGoods = 0;
        for (let otherPlayer of allPlayers) {
            if (player.id === otherPlayer.id) {
                continue;
            }
            let toSteal = Math.min(otherPlayer.resources[this.resource], 1);
            stolenGoods += toSteal;
            otherPlayer.resources[this.resource] -= toSteal;
        }
        player.resources[this.resource] += stolenGoods;
    }
}

function checkIfRoadCanBeRemoved(board: BoardSchema, index: number, loc: EdgeLoc) {
    let tile = board.tiles[index];
    let x = tile.x;
    let y = tile.y;
    let edge = tile[loc];
    let ownerPlayerId = edge.ownerPlayerId;
    if (ownerPlayerId === "") {
        return false;
    }

    for (let vertexSchema of board.getVerticesOfEdge(x, y, loc)) {
        let noKnight = vertexSchema.knight === undefined || vertexSchema.knight.ownerPlayerId !== ownerPlayerId;
        let noSettlement = vertexSchema.ownerPlayerId !== ownerPlayerId;
        let noRoad = !board.getEdgesOfVertex(vertexSchema.x, vertexSchema.y, vertexSchema.loc)
            .filter(e => !(e.x === x && e.y === y && e.loc == loc))
            .some(e => e.ownerPlayerId === ownerPlayerId);
        if (noKnight && noSettlement && noRoad) {
            return true;
        }
    }
    return false;
}