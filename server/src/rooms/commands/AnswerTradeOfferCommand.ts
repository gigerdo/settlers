import {checkPlayerHasNecessaryResources} from "../checkPlayerHasNecessaryResources";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class AnswerTradeOfferCommand extends TracedSettlersCommand {
    private playerId: string;
    private tradeOfferId: string;
    private acceptOffer: boolean;

    constructor(clientId: string, tradeOfferId: string, acceptOffer: boolean) {
        super("AnswerTradeOfferCommand");
        this.playerId = clientId;
        this.tradeOfferId = tradeOfferId;
        this.acceptOffer = acceptOffer;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;

        if (this.acceptOffer) {
            let player = this.state.players.get(this.playerId)!;
            let trade = this.state.openTrades.get(this.tradeOfferId)!;
            return this.state.phase === "playing" && !isPlayersTurn && trade && checkPlayerHasNecessaryResources(player, trade.receive);
        } else {
            return this.state.phase === "playing" && !isPlayersTurn;
        }
    }

    tracedExecute() {
        console.log("Command: New Trade Offer");
        let trade = this.state.openTrades.get(this.tradeOfferId)!;
        if (trade) {
            if (this.acceptOffer) {
                trade.accepted.add(this.playerId);
                trade.rejected.delete(this.playerId);
            } else {
                trade.rejected.add(this.playerId);
                trade.accepted.delete(this.playerId);
            }
        }
    }
}
