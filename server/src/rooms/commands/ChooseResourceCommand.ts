import {updatePlayers} from "../updatePlayers";
import {Resource} from "../schema/ResourcesSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class ChooseResourceCommand extends TracedSettlersCommand {
    private playerId: string;
    private resource: Resource | undefined;

    constructor(playerId: string, resource: Resource | undefined) {
        super("ChooseResourceCommand");
        this.playerId = playerId;
        this.resource = resource;
    }

    validate(): boolean {
        let player = this.state.players.get(this.playerId)!;
        return this.state.phase === "choosing-resource"
            && player.cityUpgrades.green >= 3
            && !player.hasChosenResourceThisTurn
            && player.receivedResourcesThisTurn === 0;
    }

    tracedExecute() {
        let player = this.state.players.get(this.playerId)!;

        if (this.resource !== undefined) {
            this.state.bank.takeResource(player, this.resource);
        }

        player.hasChosenResourceThisTurn = true;

        if (Array.from(this.state.players.values())
            .every(p => p.cityUpgrades.green < 3 || p.receivedResourcesThisTurn > 0 || p.hasChosenResourceThisTurn)) {
            this.state.phase = "playing";
            this.state.players.forEach(p => {
                p.hasChosenResourceThisTurn = false;
            });
        }

        updatePlayers(this.state);
    }
}

