import {DevelopmentCardSchema} from "../schema/DevelopmentCardSchema";
import {updatePlayers} from "../updatePlayers";
import {updateCanBuildRoad} from "../updateCanBuildRoad";
import {updateCanBuildSettlement} from "../updateCanBuildSettlement";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class BuyDevelopmentCardCommand extends TracedSettlersCommand {
    private playerId: string;

    constructor(clientId: string) {
        super("BuyDevelopmentCardCommand");
        this.playerId = clientId;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let player = this.state.players.get(this.playerId)!;
        return this.state.phase === "playing" && isPlayersTurn
            && player.resources.wool >= 1 && player.resources.grain >= 1 && player.resources.ore >= 1
            && this.state.bank.developmentCards.length > 0
            && !this.state.settings.addonCitiesAndKnights;
    }

    tracedExecute() {
        console.log("Command: Buy development card");
        let player = this.state.players.get(this.playerId)!;
        let card = this.state.bank.buyDevelopmentCard(player);
        if (card === "victory-point") {
            player.victoryPointsHidden += 1;
        }
        player.developmentCards.push(new DevelopmentCardSchema(card));

        updatePlayers(this.state);
        updateCanBuildRoad(this.state, this.playerId);
        updateCanBuildSettlement(this.state, this.playerId);
    }
}