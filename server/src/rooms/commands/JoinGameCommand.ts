import {PlayerSchema} from "../schema/PlayerSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class JoinGameCommand extends TracedSettlersCommand {
    private clientId: string;
    private userName: string;

    constructor(clientId: string, userName: string) {
        super("JoinGameCommand");
        this.clientId = clientId;
        this.userName = userName;
    }

    validate(): boolean {
        return this.state.phase === "setup";
    }

    tracedExecute() {
        let playerSchema = new PlayerSchema(this.clientId);
        playerSchema.name = this.userName;
        this.state.players.set(this.clientId, playerSchema);
    }

}