import {updatePlayers} from "../updatePlayers";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {CityUpgradeColor} from "../schema/CityUpgradeSchema";
import {handleRegularDiceRoll} from "./RollDiceCommand";

export class ChooseProgressCardCommand extends TracedSettlersCommand {
    private playerId: string;
    private color: CityUpgradeColor;

    constructor(playerId: string, color: string) {
        super("ChooseProgressCardCommand");
        this.playerId = playerId;
        this.color = color as CityUpgradeColor;
    }

    validate(): boolean {
        let player = this.state.players.get(this.playerId)!;
        return this.state.phase === "choosing-progresscard"
            && this.state.playersWinningBarbarianAttack.find(p => p === player.id) !== undefined
            && this.canPickColor(this.color);
    }

    tracedExecute() {
        let player = this.state.players.get(this.playerId)!;

        let idx = this.state.playersWinningBarbarianAttack
            .findIndex(p => p === this.playerId);
        this.state.playersWinningBarbarianAttack.deleteAt(idx);

        this.state.bank.pickProgressCard(player, this.color);

        if (this.state.playersWinningBarbarianAttack.length === 0) {
            this.state.barbarianPosition = 0;
            // Run remaining roll-dice code
            handleRegularDiceRoll(this.state);
        }

        updatePlayers(this.state);
    }

    private canPickColor(color: CityUpgradeColor) {
        switch (color) {
            case "green":
                return this.state.bank.scienceCardsTotal > 0;
            case "blue":
                return this.state.bank.politicsCardsTotal > 0;
            case "yellow":
                return this.state.bank.tradeCardsTotal > 0;
        }
    }
}

