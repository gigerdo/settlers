import {updatePlayers} from "../updatePlayers";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {VertexLoc} from "../schema/VertexSchema";
import {KnightSchema} from "../schema/KnightSchema";

export class ActivateKnightCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: VertexLoc;

    constructor(clientId: string, index: number, loc: VertexLoc) {
        super("ActivateKnightCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
    }

    validate(): boolean {
        let player = this.state.players.get(this.playerId)!;
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let vertex = this.state.board.tiles[this.index][this.loc];
        let knight = vertex.knight;
        if (knight === undefined) {
            return false;
        }

        return isPlayersTurn && this.state.phase === "playing"
            && this.state.settings.addonCitiesAndKnights
            && !knight.activated
            && knight.ownerPlayerId === this.playerId
            && player.resources["grain"] >= 1;
    }

    tracedExecute() {
        let tile = this.state.board.tiles[this.index];
        let knight = tile[this.loc].knight!!;
        let player = this.state.players.get(this.playerId)!;
        this.state.bank.activateKnight(player, knight);

        let newKnight = new KnightSchema();
        newKnight.assign(knight);
        tile[this.loc].knight = newKnight;

        updatePlayers(this.state);
    }

}

