import {updatePlayers} from "../updatePlayers";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {VertexLoc, VertexSchema} from "../schema/VertexSchema";
import {updateCanBuildRoad} from "../updateCanBuildRoad";
import {updateCanBuildSettlement} from "../updateCanBuildSettlement";
import {updateLongestRoad} from "../updateLongestRoad";
import {KnightSchema} from "../schema/KnightSchema";
import {SettlersSchema} from "../schema/SettlersSchema";

export class MoveKnightCommand extends TracedSettlersCommand {
    private playerId: string;
    private fromIndex: number;
    private fromLoc: VertexLoc;
    private toIndex: number;
    private toLoc: VertexLoc;

    constructor(clientId: string, fromIndex: number, fromLoc: VertexLoc, toIndex: number, toLoc: VertexLoc) {
        super("MoveKnightCommand");
        this.fromIndex = fromIndex;
        this.fromLoc = fromLoc;
        this.toIndex = toIndex;
        this.toLoc = toLoc;
        this.playerId = clientId;
    }

    validate(): boolean {
        let player = this.state.players.get(this.playerId)!;
        let fromVertex = this.state.board.tiles[this.fromIndex][this.fromLoc];
        let toVertex = this.state.board.tiles[this.toIndex][this.toLoc];

        if (this.state.phase === "displacing-knight") {
            if (this.state.knightBeingDisplaced === undefined) {
                // broken state
                return true;
            }
            if (this.state.knightBeingDisplacedIndex === this.toIndex && this.state.knightBeingDisplacedLoc === this.toLoc) {
                // Can't place the knight back to where it was displaced from
                return false;
            }

            // This knight is being displaced by another knight moving to its space
            let knightHasPathToTarget = calculateVerticesTheKnightCanMoveTo(this.state, this.state.knightBeingDisplaced!, fromVertex, false)
                .find(v => v.y * 7 + v.x == this.toIndex && v.loc == this.toLoc) !== undefined;

            return this.state.knightBeingDisplaced?.ownerPlayerId == this.playerId
                && knightHasPathToTarget
                && toVertex.knight === undefined;
        }

        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;

        let knight = fromVertex.knight;
        if (knight === undefined) {
            return false;
        }

        let verticesTheKnightCanMoveTo = calculateVerticesTheKnightCanMoveTo(this.state, knight, fromVertex, true);
        let knightHasPathToTarget = verticesTheKnightCanMoveTo
            .find(v => v.y * 7 + v.x == this.toIndex && v.loc == this.toLoc) !== undefined;

        return isPlayersTurn && this.state.phase === "playing"
            && knight.ownerPlayerId === this.playerId
            && knight.activated
            && !knight.actionTaken
            && knightHasPathToTarget;
    }

    tracedExecute() {
        let fromVertex = this.state.board.tiles[this.fromIndex][this.fromLoc];
        let toVertex = this.state.board.tiles[this.toIndex][this.toLoc];

        if (this.state.phase === "displacing-knight") {
            // Special handling when knight is being displaced
            toVertex.knight = this.state.knightBeingDisplaced;
            this.state.knightBeingDisplaced = undefined;
            this.state.knightBeingDisplacedIndex = undefined;
            this.state.knightBeingDisplacedLoc = undefined;
            this.state.phase = "playing";
        } else {
            if (toVertex.knight !== undefined) {
                let verticesTheKnightCanMoveTo = calculateVerticesTheKnightCanMoveTo(this.state, toVertex.knight, toVertex, false);
                if (verticesTheKnightCanMoveTo.length > 0) {
                    // Allow player to move his knight
                    this.state.phase = "displacing-knight";
                    this.state.knightBeingDisplaced = toVertex.knight;
                    this.state.knightBeingDisplacedIndex = this.toIndex;
                    this.state.knightBeingDisplacedLoc = this.toLoc;
                } else {
                    // Knight can't move anywhere, remove it from board
                    this.state.players.get(toVertex.knight.ownerPlayerId)!.basicKnights += 1;
                }
            }

            toVertex.knight = fromVertex.knight;
            fromVertex.knight = undefined;
            toVertex.knight!.actionTaken = true;
            toVertex.knight!.activated = false;
        }

        updatePlayers(this.state);
        updateCanBuildRoad(this.state, this.playerId);
        updateCanBuildSettlement(this.state, this.playerId);
        updateLongestRoad(this.state);
    }
}

export function calculateVerticesTheKnightCanMoveTo(state: SettlersSchema, knight: KnightSchema, vertex: VertexSchema, canDisplace: boolean): VertexSchema[] {
    let remainingVertices = [vertex];
    let visited = new Set();
    let selectableVertices = [];
    while (remainingVertices.length > 0) {
        let cv = remainingVertices.pop()!;
        let id = `${cv.x}-${cv.y}-${cv.loc}`;
        if (visited.has(id)) {
            continue;
        }
        visited.add(id);
        let edges = state.board.getEdgesOfVertex(cv.x, cv.y, cv.loc)
            .filter(e => e.ownerPlayerId === knight!.ownerPlayerId);
        for (let edge of edges) {
            let edgeVertices = state.board.getVerticesOfEdge(edge.x, edge.y, edge.loc)
                .filter(v => v.ownerPlayerId === "" || v.ownerPlayerId === knight!.ownerPlayerId);
            for (let edgeVertex of edgeVertices) {
                if (edgeVertex.knight === undefined || edgeVertex.knight.ownerPlayerId === knight!.ownerPlayerId) {
                    remainingVertices.push(edgeVertex);
                    if (edgeVertex.ownerPlayerId === "" && edgeVertex.knight === undefined) {
                        selectableVertices.push(edgeVertex);
                    }
                }
                if (canDisplace
                    && edgeVertex.knight !== undefined
                    && edgeVertex.knight.ownerPlayerId !== knight!.ownerPlayerId
                    && isLowerLevel(edgeVertex.knight, knight!)
                ) {
                    selectableVertices.push(edgeVertex);
                }
            }
        }
    }
    return selectableVertices;
}

function isLowerLevel(a: KnightSchema, b: KnightSchema): boolean {
    if (a.level === "basic" && (b.level === "strong" || b.level === "mighty")) {
        return true;
    }
    if (a.level === "strong" && b.level === "mighty") {
        return true;
    }
    return false;
}
