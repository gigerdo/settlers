import {updatePlayers} from "../updatePlayers";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {VertexLoc} from "../schema/VertexSchema";
import {updateCanBuildRoad} from "../updateCanBuildRoad";
import {updateCanBuildSettlement} from "../updateCanBuildSettlement";
import {updateLongestRoad} from "../updateLongestRoad";

export class RemoveKnightCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: VertexLoc;

    constructor(clientId: string, index: number, loc: VertexLoc) {
        super("RemoveKnightCommand");
        this.index = index;
        this.loc = loc;
        this.playerId = clientId;
    }

    validate(): boolean {
        let vertex = this.state.board.tiles[this.index][this.loc];
        let knight = vertex.knight;
        if (knight === undefined) {
            return false;
        }

        return this.state.phase === "removing-knight"
            && this.state.deserterPlayerId === this.playerId
            && knight.ownerPlayerId === this.playerId;
    }

    tracedExecute() {
        let vertex = this.state.board.tiles[this.index][this.loc];

        let knight = vertex.knight;
        vertex.knight = undefined;

        // Put knight back
        let player = this.state.players.get(this.playerId);
        switch (knight!.level) {
            case "basic":
                player!.basicKnights += 1;
                break;
            case "strong":
                player!.strongKnights += 1;
                break;
            case "mighty":
                player!.mightyKnights += 1;
                break;
        }

        let currentPlayerId = this.state.playerOrder[this.state.currentPlayerIndex];
        updatePlayers(this.state);
        updateCanBuildRoad(this.state, currentPlayerId);
        updateCanBuildSettlement(this.state, currentPlayerId);
        updateLongestRoad(this.state);

        // Check if player who played deserter card can actually place a knight anywhere
        let canPlaceKnight = [...this.state.board.tiles]
            .flatMap(t => [t.top, t.bottom])
            .find(t => t.canPlaceKnight);
        if (canPlaceKnight !== undefined) {
            // Let player that played card place a knight
            this.state.phase = "placing-knight";
            this.state.placingKnightLevel = knight?.level;
            this.state.placingKnightActivated = knight?.activated;
        } else {
            this.state.phase = "playing";
        }
    }
}

