import {updatePlayers} from "../updatePlayers";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {VertexLoc} from "../schema/VertexSchema";
import {handleRegularDiceRoll} from "./RollDiceCommand";

export class DestroyCityCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: VertexLoc;

    constructor(clientId: string, index: number, loc: VertexLoc) {
        super("DestroyCityCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
    }

    validate(): boolean {
        let player = this.state.players.get(this.playerId)!;
        let vertex = this.state.board.tiles[this.index][this.loc];
        let playerMustDestroyCity = this.state.playersDestroyingCity.find(p => p === this.playerId) !== undefined;

        return this.state.phase === "destroying-city"
            && vertex.city
            && vertex.metropolis === undefined
            && vertex.ownerPlayerId === this.playerId
            && playerMustDestroyCity;
    }

    tracedExecute() {
        let vertex = this.state.board.tiles[this.index][this.loc];
        let player = this.state.players.get(this.playerId)!;

        player.cities += 1;
        vertex.city = false;
        player.victoryPointsVisible -= 1;

        if (vertex.wall) {
            player.walls += 1;
            vertex.wall = false;
        }

        let idx = this.state.playersDestroyingCity.findIndex(p => p === this.playerId);
        this.state.playersDestroyingCity.deleteAt(idx);

        if (this.state.playersDestroyingCity.length === 0) {
            this.state.barbarianPosition = 0;
            // Run remaining roll-dice code
            handleRegularDiceRoll(this.state);
        }

        updatePlayers(this.state);
    }

}

