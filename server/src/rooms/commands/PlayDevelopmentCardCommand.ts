import {DevelopmentCardType} from "../schema/DevelopmentCardSchema";
import {updatePlayers} from "../updatePlayers";
import {isResource, Resource} from "../schema/ResourcesSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class PlayDevelopmentCardCommand extends TracedSettlersCommand {
    private playerId: string;
    private card: DevelopmentCardType;
    private firstResource: Resource;
    private secondResource: Resource;
    private monopoly: Resource;

    constructor(playerId: string, card: DevelopmentCardType, firstResource: Resource, secondResource: Resource,
                monopoly: Resource) {
        super("PlayDevelopmentCardCommand");
        this.playerId = playerId;
        this.card = card;
        this.firstResource = firstResource;
        this.secondResource = secondResource;
        this.monopoly = monopoly;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let player = this.state.players.get(this.playerId)!;
        let playerHasCard = player.developmentCards.findIndex(card => !card.sameTurn && card.type === this.card) >= 0;

        let genericCheck = !this.state.settings.addonCitiesAndKnights
            && (this.state.phase === "roll-dice" || this.state.phase === "playing")
            && isPlayersTurn && !player.hasPlayedDevelopmentCard
            && playerHasCard;
        if (this.card === "year-of-plenty") {
            return genericCheck && this.state.bank[this.firstResource] >= 1 && this.state.bank[this.secondResource] >= 1;
        } else if (this.card === "monopoly") {
            return genericCheck && isResource(this.monopoly);
        }
        return genericCheck;
    }

    tracedExecute() {
        console.log("Command: Play dev card");
        let player = this.state.players.get(this.playerId)!;

        let cardIndex = player.developmentCards.findIndex(card => !card.sameTurn && card.type === this.card);
        if (cardIndex >= 0) {
            let playedCard = player.developmentCards.splice(cardIndex, 1)[0];
            player.hasPlayedDevelopmentCard = true;
            let allPlayers = Array.from(this.state.players.values());
            switch (playedCard.type) {
                case "knight":
                    this.state.phase = "place-robber";
                    player.knights += 1;
                    let anyoneHasLargestArmy = allPlayers.some(p => p.largestArmy);
                    if (anyoneHasLargestArmy) {
                        for (let p of allPlayers) {
                            if (p.largestArmy && player.knights > p.knights) {
                                p.largestArmy = false;
                                p.victoryPointsVisible -= 2;
                                player.largestArmy = true;
                                player.victoryPointsVisible += 2;
                            }
                        }
                    } else if (player.knights >= 3) {
                        player.largestArmy = true;
                        player.victoryPointsVisible += 2;
                    }
                    break;
                case "road-building":
                    this.state.phase = "road-building-1";
                    break;
                case "year-of-plenty":
                    player.resources[this.firstResource] += 1;
                    player.resources[this.secondResource] += 1;
                    this.state.bank[this.firstResource] -= 1;
                    this.state.bank[this.secondResource] -= 1;
                    break;
                case "monopoly":
                    let stolenGoods = 0;
                    for (let otherPlayer of allPlayers) {
                        stolenGoods += otherPlayer.resources[this.monopoly];
                        otherPlayer.resources[this.monopoly] = 0;
                    }
                    player.resources[this.monopoly] = stolenGoods;
                    break;
            }
            updatePlayers(this.state);
        }
    }
}