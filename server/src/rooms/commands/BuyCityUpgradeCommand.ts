import {updatePlayers} from "../updatePlayers";
import {CityUpgradeColor, colorToResource} from "../schema/CityUpgradeSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {moveProgressCardBackToBank, progressCardBaseValidation} from "./progresscards/progessCardUtil";

export class BuyCityUpgradeCommand extends TracedSettlersCommand {
    private playerId: string;
    private color: CityUpgradeColor;
    private useCrane: Boolean;

    constructor(playerId: string, color: CityUpgradeColor, useCrane: Boolean) {
        super("BuyCityUpgradeCommand");
        this.playerId = playerId;
        this.color = color;
        this.useCrane = useCrane;
    }

    validate(): boolean {
        const player = this.state.players.get(this.playerId)!;
        const resource = colorToResource(this.color);
        const newLevel = player.cityUpgrades[this.color] + 1;


        let amountToPay = newLevel;
        if (this.useCrane) {
            amountToPay -= 1;
        }

        const playerIndex = this.state.playerOrder.indexOf(this.playerId);
        const isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        const playerHasEnoughResources = player.resources[resource] >= amountToPay;
        const citiesBuilt = 4 - player.cities;
        const playerHasAtLeastOneCity = citiesBuilt > 0; // Can only upgrade with at least 1 city built

        const metropolisesOwnedByPlayer = Array.from(this.state.metropolisOwners.values())
            .filter(id => player.id == id)
            .reduce((p, c) => p + 1, 0);
        const citiesWithoutMetropolis = citiesBuilt - metropolisesOwnedByPlayer;
        // Can only buy upgrades > 3 if player has city without metropolis (or has metropolis of this color)
        const playerOwnsMetropolisOfColor = this.state.metropolisOwners.get(this.color) == player.id;
        const playerCanBuyUpgrade = newLevel <= 3 || citiesWithoutMetropolis > 0 || playerOwnsMetropolisOfColor;

        return isPlayersTurn && this.state.phase === "playing"
            && playerHasEnoughResources
            && playerHasAtLeastOneCity
            && playerCanBuyUpgrade
            && (!this.useCrane || progressCardBaseValidation(this.state, this.playerId, "crane", "playing"));
    }

    tracedExecute() {
        const player = this.state.players.get(this.playerId)!;
        const resource = colorToResource(this.color);
        const newLevel = player.cityUpgrades[this.color] + 1;
        let amountToPay = newLevel;
        if (this.useCrane) {
            amountToPay -= 1;
            moveProgressCardBackToBank(this.state, this.playerId, "crane");
        }

        this.state.bank.giveResource(player, resource, amountToPay);
        player.cityUpgrades[this.color] += 1;

        const currentOwnerId = this.state.metropolisOwners.get(this.color);
        if (newLevel == 4 && currentOwnerId == undefined) {
            // Give metropolis if nobody already has it
            this.state.metropolisOwners.set(this.color, player.id);
            player.victoryPointsVisible += 2;
            this.state.phase = "placing-metropolis";
            this.state.metropolisBeingPlaced = this.color;
        } else if (newLevel == 5) {
            if (currentOwnerId != undefined) {
                const currentOwner = this.state.players.get(currentOwnerId);
                if (currentOwnerId != player.id && (currentOwner?.cityUpgrades[this.color] ?? 0) < 5) {
                    // Current owner does not have max level upgrades, so the metropolis is transferred to new owner

                    // Remove metropolis from previous owner
                    this.removeMetropolisFromTile();

                    // Add metropolis to new owner
                    this.state.metropolisOwners.set(this.color, player.id);
                    player.victoryPointsVisible += 2;
                    this.state.phase = "placing-metropolis";
                    this.state.metropolisBeingPlaced = this.color;
                }
            } else {
                // if first person on 5, give metropolis, take away from previous owner
                this.state.metropolisOwners.set(this.color, player.id);
                player.victoryPointsVisible += 2;
                this.state.phase = "placing-metropolis";
                this.state.metropolisBeingPlaced = this.color;
            }
        }

        updatePlayers(this.state);
    }

    private removeMetropolisFromTile() {
        for (let tile of this.state.board.tiles) {
            for (let vertex of [tile.top, tile.bottom]) {
                if (vertex.metropolis == this.color) {
                    vertex.metropolis = undefined;
                    let owningPlayer = this.state.players.get(vertex.ownerPlayerId);
                    if (owningPlayer) {
                        owningPlayer.victoryPointsVisible -= 2;
                    }
                    return;
                }
            }
        }
    }
}

