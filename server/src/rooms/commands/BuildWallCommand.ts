import {updatePlayers} from "../updatePlayers";
import {VertexLoc} from "../schema/VertexSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {moveProgressCardBackToBank, progressCardBaseValidation} from "./progresscards/progessCardUtil";

export class BuildWallCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: VertexLoc;
    private useEngineerCard: Boolean;

    constructor(clientId: string, index: number, loc: VertexLoc, useEngineerCard: Boolean) {
        super("BuildWallCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
        this.useEngineerCard = useEngineerCard;
    }

    validate(): boolean {
        if (!this.state.settings.addonCitiesAndKnights) {
            return false;
        }
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let vertex = this.state.board.tiles[this.index][this.loc];
        let ownCity = vertex.ownerPlayerId === this.playerId && vertex.city;
        let noWall = !vertex.wall;
        let player = this.state.players.get(this.playerId)!;
        let resourceCheck = this.useEngineerCard ? progressCardBaseValidation(this.state, this.playerId, "engineer", "playing") : player.resources.brick >= 2;

        return this.state.phase === "playing"
            && isPlayersTurn && ownCity && !vertex.wall
            && resourceCheck
            && player.walls > 0;
    }

    tracedExecute() {
        let tile = this.state.board.tiles[this.index];
        tile[this.loc].wall = true;

        let player = this.state.players.get(this.playerId)!;

        if (this.useEngineerCard) {
            moveProgressCardBackToBank(this.state, this.playerId, "engineer");
        } else {
            this.state.bank.buyWall(player);
        }
        player.walls -= 1;

        updatePlayers(this.state);
    }
}