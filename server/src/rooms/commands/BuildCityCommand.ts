import {updateCanBuildRoad} from "../updateCanBuildRoad";
import {updatePlayers} from "../updatePlayers";
import {updateCanBuildSettlement} from "../updateCanBuildSettlement";
import {updateLongestRoad} from "../updateLongestRoad";
import {VertexLoc} from "../schema/VertexSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {moveProgressCardBackToBank, progressCardBaseValidation} from "./progresscards/progessCardUtil";

export class BuildCityCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: VertexLoc;
    private useMedicineCard: Boolean;

    constructor(clientId: string, index: number, loc: VertexLoc, useMedicineCard: Boolean) {
        super("BuildCityCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
        this.useMedicineCard = useMedicineCard;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let vertex = this.state.board.tiles[this.index][this.loc];
        let ownSettlement = vertex.ownerPlayerId === this.playerId;
        let player = this.state.players.get(this.playerId)!;
        let resourceCheck = this.useMedicineCard ? progressCardBaseValidation(this.state, this.playerId, "medicine", "playing") && player.resources.grain >= 1 && player.resources.ore >= 2 : player.resources.grain >= 2 && player.resources.ore >= 3;

        return this.state.phase === "playing" && isPlayersTurn && ownSettlement && !vertex.city
            && resourceCheck
            && player.cities > 0;
    }

    tracedExecute() {
        let tile = this.state.board.tiles[this.index];
        tile[this.loc].city = true;

        let player = this.state.players.get(this.playerId)!;
        this.state.bank.buyCity(player, this.useMedicineCard);
        player.cities -= 1;
        player.settlements += 1;
        player.victoryPointsVisible += 1;
        if (this.useMedicineCard) {
            moveProgressCardBackToBank(this.state, this.playerId, "medicine");
        }

        updatePlayers(this.state);
        updateCanBuildRoad(this.state, this.playerId);
        updateCanBuildSettlement(this.state, this.playerId);
        updateLongestRoad(this.state);
    }
}