import {TradeResources} from "../schema/TradeSchema";
import {calculateTotalCards} from "../calculateTotalCards";
import {updatePlayers} from "../updatePlayers";
import {Resource} from "../schema/ResourcesSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class DiscardCardsCommand extends TracedSettlersCommand {
    private playerId: string;
    private resources: TradeResources;
    private maximumCards: number = 0;
    private cardsToDiscard: number = 0;
    private totalResourcesDiscarding: number = 0;

    constructor(playerId: string, resources: TradeResources) {
        super("DiscardCardsCommand");
        this.playerId = playerId;
        this.resources = resources;
    }

    validate(): boolean {
        let player = this.state.players.get(this.playerId)!;
        let playerHasEnoughResources = Object.entries(this.resources).every(([type, amount]) => {
            return player.resources[type as Resource] >= amount;
        });

        let totalCards = calculateTotalCards(player.resources);
        this.totalResourcesDiscarding = calculateTotalCards(this.resources);
        this.maximumCards = 7 + (3 - player.walls) * 2;
        if (this.state.saboteurPlayed) {
            this.cardsToDiscard = Math.floor(totalCards / 2);
        } else {
            this.cardsToDiscard = totalCards > this.maximumCards ? Math.floor(totalCards / 2) : 0;
        }

        return this.state.phase === "dropping-cards"
            && playerHasEnoughResources
            && !player.hasDiscardedThisTurn
            && this.cardsToDiscard == this.totalResourcesDiscarding;
    }

    tracedExecute() {
        let player = this.state.players.get(this.playerId)!;
        if (this.totalResourcesDiscarding === this.cardsToDiscard && this.cardsToDiscard > 0) {
            Object.entries(this.resources).forEach(([type, amount]) => {
                this.state.bank[type as Resource] += amount;
                player.resources[type as Resource] -= amount;
            });
            player.hasDiscardedThisTurn = true;
        }

        if (this.state.saboteurPlayed) {
            let currentPlayerId = this.state.playerOrder[this.state.currentPlayerIndex];
            let activePlayer = this.state.players.get(currentPlayerId)!;
            let activePlayersVp = activePlayer.victoryPointsVisible + activePlayer.victoryPointsHidden;
            let playersToDiscard = Array.from(this.state.players.values())
                .filter(p => p.id != activePlayer.id)
                .filter(p => p.victoryPointsVisible + p.victoryPointsHidden >= activePlayersVp)
                .filter(p => !p.hasDiscardedThisTurn);
            if (playersToDiscard.length == 0) {
                this.state.phase = "playing";
                this.state.saboteurPlayed = false;
                this.state.players.forEach(p => p.hasDiscardedThisTurn = false);
            }
        } else if (Array.from(this.state.players.values())
            .every(p => calculateTotalCards(p.resources) <= this.maximumCards || p.hasDiscardedThisTurn)) {
            if (this.state.settings.addonCitiesAndKnights && this.state.barbarianAttacks == 0) {
                // The robber can only be moved after the first barbarian attack
                this.state.phase = "playing";
            } else {
                this.state.phase = "place-robber";
            }
            this.state.players.forEach(p => p.hasDiscardedThisTurn = false);
        }

        updatePlayers(this.state);
    }
}

