import {Command} from "@colyseus/command";
import {SettlersRoom, tracer} from "../SettlersRoom";

export abstract class TracedSettlersCommand extends Command<SettlersRoom, {}> {
    private commandName: string;

    constructor(commandName: string) {
        super();
        this.commandName = commandName;
    }


    execute(payload: this["payload"]): Array<Command> | Command | void | Promise<Array<Command>> | Promise<Command> | Promise<unknown> {
        tracer.startActiveSpan(this.commandName, span => {
            console.log("Command: " + this.commandName);
            this.tracedExecute(payload);
            span.end();
        });
    }

    abstract tracedExecute(payload: this["payload"]): Array<Command> | Command | void | Promise<Array<Command>> | Promise<Command> | Promise<unknown>;
}