import {VertexLoc} from "../schema/VertexSchema";
import {isUsefulTerrain} from "../schema/TerrainType";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class PlaceSettlementCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: VertexLoc;

    constructor(clientId: string, index: number, loc: VertexLoc) {
        super("PlaceSettlementCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let vertex = this.state.board.tiles[this.index][this.loc];
        let noSettlementPlaced = vertex.ownerPlayerId === "" && vertex.knight === undefined;
        return this.state.phase === "placing-settlement" && isPlayersTurn && noSettlementPlaced && vertex.canPlace;
    }

    tracedExecute() {
        let player = this.state.players.get(this.playerId)!;
        let isSecondSettlementOfPlayer = player.victoryPointsVisible == 1;

        let tile = this.state.board.tiles[this.index];
        tile[this.loc].ownerPlayerId = this.playerId;

        if (this.state.settings.addonCitiesAndKnights && isSecondSettlementOfPlayer) {
            console.log("Placing city");
            tile[this.loc].city = true;
            player.cities -= 1;
            player.victoryPointsVisible += 2;
        } else {
            console.log("Placing settlement");
            player.settlements -= 1;
            player.victoryPointsVisible += 1;
        }

        let tilesOfVertex = this.state.board.getTilesOfVertex(this.index, this.loc);
        for (let surroundingTile of tilesOfVertex) {
            surroundingTile.players.add(this.playerId);
        }

        if (isSecondSettlementOfPlayer) {
            for (let surroundingTile of tilesOfVertex) {
                if (isUsefulTerrain(surroundingTile.terrainType)) {
                    player.resources[surroundingTile.terrainType] += 1;
                    this.state.bank[surroundingTile.terrainType] -= 1;
                }
            }
        }

        let edgesOfVertex = this.state.board.getEdgesOfVertex(tile.x, tile.y, this.loc);
        for (let edge of edgesOfVertex) {
            if (edge.harbor) {
                player.harbors.add(edge.harbor);
            }
        }

        if (this.loc === "top") {
            let topRightOf = this.state.board.getTopRightOf(tile.x, tile.y);
            if (topRightOf) {
                topRightOf.bottom.canPlace = false;
                let topLeftOfTopRight = this.state.board.getTopLeftOf(topRightOf.x, topRightOf.y);
                if (topLeftOfTopRight) {
                    topLeftOfTopRight.bottom.canPlace = false;
                }
            }
            let topLeftOf = this.state.board.getTopLeftOf(tile.x, tile.y);
            if (topLeftOf) {
                topLeftOf.bottom.canPlace = false;
            }
        } else {
            let bottomLeftOf = this.state.board.getBottomLeftOf(tile.x, tile.y)
            let bottomRightOf = this.state.board.getBottomRightOf(tile.x, tile.y)
            if (bottomLeftOf) {
                bottomLeftOf.top.canPlace = false;
            }
            if (bottomRightOf) {
                bottomRightOf.top.canPlace = false;
                let bottomLeftOfBottomRight = this.state.board.getBottomLeftOf(bottomRightOf.x, bottomRightOf.y)
                if (bottomLeftOfBottomRight) {
                    bottomLeftOfBottomRight.top.canPlace = false;
                }
            }
        }

        this.state.phase = "placing-road";
        for (let i = 0; i < this.state.board.tiles.length; i++) {
            this.state.board.tiles[i]["north"].canPlace = false;
            this.state.board.tiles[i]["east"].canPlace = false;
            this.state.board.tiles[i]["south"].canPlace = false;
        }

        let edges = this.state.board.getEdgesOfVertex(tile.x, tile.y, this.loc);
        edges.forEach(e => {
            let tilesOfEdge = this.state.board.getTilesOfEdge(e.x, e.y, e.loc);
            let hasLand = tilesOfEdge.some(t => t.terrainType !== "water");
            let nothingPlaced = e.ownerPlayerId === "";
            e.canPlace = hasLand && nothingPlaced;
        });
    }

}