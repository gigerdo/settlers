import {updatePlayers} from "../updatePlayers";
import {shuffle} from "../schema/shuffle";
import {Resource} from "../schema/ResourcesSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class PlaceRobberCommand extends TracedSettlersCommand {
    private playerId: string;
    private x: number;
    private y: number;
    private stealFromPlayer: string;
    private playersOnTile: Set<string>;

    constructor(clientId: string, x: number, y: number, stealFromPlayer: string) {
        super("PlaceRobberCommand");
        this.playerId = clientId;
        this.x = x;
        this.y = y;
        this.stealFromPlayer = stealFromPlayer;
        this.playersOnTile = new Set();
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let tile = this.state.board.getTile(this.x, this.y)!;
        let robber = this.state.board.robber;
        this.playersOnTile = new Set(this.state.board
            .getAllVerticesOfTile(tile.x, tile.y)
            .map(t => t.ownerPlayerId)
            .filter(v => v !== undefined && v !== ""));

        let baseValidation = isPlayersTurn
            && tile.terrainType !== "water"
            && (this.x !== robber.x || this.y !== robber.y);

        if (this.state.phase === "place-robber") {
            return baseValidation && (this.playersOnTile.has(this.stealFromPlayer) || this.stealFromPlayer === "");
        } else if (this.state.phase == "move-robber-bishop") {
            return baseValidation;
        } else {
            return false;
        }
    }

    tracedExecute() {
        if (this.state.phase === "move-robber-bishop") {
            for (let player of this.playersOnTile.values()) {
                if (player !== this.playerId) {
                    this.steal(player);
                }
            }
        }

        console.log("Command: Place Robber");
        if (this.state.rolledNumber == -1) {
            this.state.phase = "roll-dice";
        } else {
            this.state.phase = "playing";
        }
        this.state.board.robber.x = this.x;
        this.state.board.robber.y = this.y;
        if (this.stealFromPlayer !== "") {
            this.steal(this.stealFromPlayer);
        }

        updatePlayers(this.state);
    }

    private steal(targetPlayerId: string) {
        let player = this.state.players.get(this.playerId)!;
        let playerToStealFrom = this.state.players.get(targetPlayerId)!;

        let playersCards: Resource[] = Object.entries(playerToStealFrom.resources)
            .flatMap(([key, value]) => new Array(value).fill(key));

        let pickedCard = shuffle(playersCards).shift();
        if (pickedCard) {
            playerToStealFrom.resources[pickedCard] -= 1;
            player.resources[pickedCard] += 1;
        }
    }
}