import {updatePlayers} from "../updatePlayers";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {VertexLoc} from "../schema/VertexSchema";
import {KnightSchema} from "../schema/KnightSchema";

export class ChaseAwayRobberCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: VertexLoc;

    constructor(clientId: string, index: number, loc: VertexLoc) {
        super("ChaseAwayRobberCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
    }

    validate(): boolean {
        let player = this.state.players.get(this.playerId)!;
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let vertex = this.state.board.tiles[this.index][this.loc];
        let knight = vertex.knight;
        if (knight === undefined) {
            return false;
        }

        let robber = this.state.board.robber;
        let robberIsAdjacent = this.state.board.getTilesOfVertex(this.index, this.loc)
            .find(t => t.x === robber.x && t.y === robber.y) !== undefined;

        return isPlayersTurn && this.state.phase === "playing"
            && knight.ownerPlayerId === this.playerId
            && !knight.actionTaken
            && knight.activated
            && this.state.settings.addonCitiesAndKnights
            && this.state.barbarianAttacks > 0 // barbarians have already attacked once
            && robberIsAdjacent;

    }

    tracedExecute() {
        this.state.phase = "place-robber";

        let vertex = this.state.board.tiles[this.index][this.loc];

        let newKnight = new KnightSchema();
        newKnight.assign(vertex.knight!);
        newKnight.activated = false;
        newKnight.actionTaken = true;
        vertex.knight = newKnight;

        updatePlayers(this.state);
    }

}

