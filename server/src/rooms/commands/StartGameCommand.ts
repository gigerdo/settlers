import {shuffle} from "../schema/shuffle";
import {ArraySchema} from "@colyseus/schema";
import {PLAYER_COLORS} from "../schema/PlayerSchema";
import {VertexLoc} from "../schema/VertexSchema";
import {GameSettings} from "../schema/GameSettingsSchema";
import {updatePlayers} from "../updatePlayers";
import {tracer} from "../SettlersRoom";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class StartGameCommand extends TracedSettlersCommand {
    private playerId: string;
    private settings: GameSettings;

    constructor(clientId: string, settings: GameSettings) {
        super("StartGameCommand");
        this.playerId = clientId;
        this.settings = settings;
    }

    validate(): boolean {
        return this.state.phase === "setup";
    }

    async tracedExecute() {
        console.log("Command: Start Game");
        await this.room.setPrivate(true);

        this.state.settings.update(this.settings);

        tracer.startActiveSpan("setupBoard", (span) => {
            this.state.board.setupBoard(this.settings);
            span.end();
        });

        this.state.phase = "placing-settlement";
        this.state.currentPlayerIndex = 0;

        let playerOrder = Array.from(this.state.players.values()).map(p => p.id);
        this.state.playerOrder = new ArraySchema<string>(...shuffle(playerOrder));
        this.state.playerOrder.forEach((playerId, index) => {
            this.state.players.get(playerId)!.color = PLAYER_COLORS[index];
        });

        const updateCanPlace = (i: number, loc: VertexLoc) => {
            let tilesOfVertex = this.state.board.getTilesOfVertex(i, loc);
            let hasLand = tilesOfVertex.some(t => t.terrainType !== "water");
            this.state.board.tiles[i][loc].canPlace = hasLand;
        };

        for (let i = 0; i < this.state.board.tiles.length; i++) {
            updateCanPlace(i, "top");
            updateCanPlace(i, "bottom");
        }

        updatePlayers(this.state);
    }
}