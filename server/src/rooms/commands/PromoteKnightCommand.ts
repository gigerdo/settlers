import {updatePlayers} from "../updatePlayers";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {VertexLoc} from "../schema/VertexSchema";
import {KnightSchema} from "../schema/KnightSchema";

export class PromoteKnightCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: VertexLoc;

    constructor(clientId: string, index: number, loc: VertexLoc) {
        super("PromoteKnightCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
    }

    validate(): boolean {
        let player = this.state.players.get(this.playerId)!;
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let vertex = this.state.board.tiles[this.index][this.loc];
        let knight = vertex.knight;
        if (knight === undefined) {
            return false;
        }

        return isPlayersTurn && this.state.phase === "playing"
            && knight.ownerPlayerId === this.playerId
            && player.resources["wool"] >= 1 && player.resources["ore"] >= 1
            && !knight.promoted // Knight can only be promoted once per turn
            && ((knight.level === "basic" && player.strongKnights > 0)
                || (knight.level === "strong" && player.mightyKnights > 0 && player.cityUpgrades.blue >= 3));
    }

    tracedExecute() {
        let tile = this.state.board.tiles[this.index];
        let knight = tile[this.loc].knight!!;
        let player = this.state.players.get(this.playerId)!;
        this.state.bank.promoteKnight(player, knight);

        let newKnight = new KnightSchema();
        newKnight.assign(knight);
        tile[this.loc].knight = newKnight;

        updatePlayers(this.state);
    }

}

