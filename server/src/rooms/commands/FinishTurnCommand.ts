import {updateCanBuildRoad} from "../updateCanBuildRoad";
import {updateCanBuildSettlement} from "../updateCanBuildSettlement";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {resetKnightStatus} from "../resetKnightStatus";

export class FinishTurnCommand extends TracedSettlersCommand {
    private playerId: string;

    constructor(clientId: string) {
        super("FinishTurnCommand");
        this.playerId = clientId;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        return this.state.phase === "playing" && isPlayersTurn;
    }

    tracedExecute() {
        console.log("Command: Finish Turn");
        let player = this.state.players.get(this.playerId)!;
        player.developmentCards.forEach(card => card.sameTurn = false);
        player.hasPlayedDevelopmentCard = false;
        this.state.rolledNumber = -1;
        const pointsNecessaryToWin = this.state.settings.addonCitiesAndKnights ? 13 : 10;
        if (player.victoryPointsHidden + player.victoryPointsVisible >= pointsNecessaryToWin) {
            this.state.phase = "finished";
            this.state.winner = player.id;
        } else {
            this.state.phase = "roll-dice";
            this.state.currentPlayerIndex = (this.state.currentPlayerIndex + 1) % this.state.playerOrder.length;
            let nextPlayer = this.state.playerOrder[this.state.currentPlayerIndex];
            updateCanBuildRoad(this.state, nextPlayer);
            updateCanBuildSettlement(this.state, nextPlayer);
            resetKnightStatus(this.state);
            this.state.openTrades.clear();
        }

    }
}