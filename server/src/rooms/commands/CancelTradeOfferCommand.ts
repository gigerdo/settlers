import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class CancelTradeOfferCommand extends TracedSettlersCommand {
    private playerId: string;
    private tradeOfferId: string;

    constructor(clientId: string, tradeOfferId: string) {
        super("CancelTradeOfferCommand");
        this.playerId = clientId;
        this.tradeOfferId = tradeOfferId;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        return this.state.phase === "playing" && isPlayersTurn;
    }

    tracedExecute() {
        console.log("Command: Cancelled Trade Offer");
        this.state.openTrades.delete(this.tradeOfferId);
    }
}