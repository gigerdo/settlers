import {EdgeLoc} from "../schema/EdgeSchema";
import {updateCanBuildRoad} from "../updateCanBuildRoad";
import {updateCanBuildSettlement} from "../updateCanBuildSettlement";
import {TracedSettlersCommand} from "./TracedSettlersCommand";

export class PlaceRoadCommand extends TracedSettlersCommand {
    private playerId: string;
    private index: number;
    private loc: EdgeLoc;

    constructor(clientId: string, index: number, loc: EdgeLoc) {
        super("PlaceRoadCommand");
        this.playerId = clientId;
        this.index = index;
        this.loc = loc;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let edge = this.state.board.tiles[this.index][this.loc];
        let noRoadPlaced = edge.ownerPlayerId === "";
        return this.state.phase === "placing-road" && isPlayersTurn && noRoadPlaced && edge.canPlace;
    }

    tracedExecute() {
        let tile = this.state.board.tiles[this.index];
        tile[this.loc].ownerPlayerId = this.playerId;
        tile[this.loc].canPlace = false;
        let player = this.state.players.get(this.playerId)!;
        player.roads -= 1;

        if (this.state.currentPlayerIndex === this.state.playerOrder.length - 1 && !this.state.reverseOrder) {
            this.state.reverseOrder = true;
            this.state.phase = "placing-settlement";
        } else if (this.state.currentPlayerIndex == 0 && this.state.reverseOrder) {
            this.state.reverseOrder = false;
            this.state.phase = "roll-dice";
            let nextPlayer = this.state.playerOrder[0];
            updateCanBuildRoad(this.state, nextPlayer);
            updateCanBuildSettlement(this.state, nextPlayer);
        } else {
            if (this.state.reverseOrder) {
                this.state.currentPlayerIndex = this.state.currentPlayerIndex - 1;
            } else {
                this.state.currentPlayerIndex = this.state.currentPlayerIndex + 1;
            }
            this.state.phase = "placing-settlement";
        }
    }
}