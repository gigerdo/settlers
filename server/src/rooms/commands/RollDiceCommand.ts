import {EventDie, SettlersSchema} from "../schema/SettlersSchema";
import {TileSchema} from "../schema/TileSchema";
import {isUsefulTerrain} from "../schema/TerrainType";
import {updatePlayers} from "../updatePlayers";
import {calculateTotalCards} from "../calculateTotalCards";
import {DevelopmentCardSchema} from "../schema/DevelopmentCardSchema";
import {TracedSettlersCommand} from "./TracedSettlersCommand";
import {getKnightStrength} from "../schema/KnightSchema";
import {ArraySchema} from "@colyseus/schema";
import {moveProgressCardBackToBank} from "./progresscards/progessCardUtil";


type D6 = 1 | 2 | 3 | 4 | 5 | 6;

export class RollDiceCommand extends TracedSettlersCommand {
    private playerId: string;
    private redDie?: number;
    private yellowDie?: number;

    constructor(clientId: string, redDie?: number, yellowDie?: number) {
        super("RollDiceCommand");
        this.playerId = clientId;
        this.redDie = redDie;
        this.yellowDie = yellowDie;
    }

    validate(): boolean {
        let playerIndex = this.state.playerOrder.indexOf(this.playerId);
        let isPlayersTurn = this.state.currentPlayerIndex === playerIndex;
        let canRollDice = this.state.phase === "roll-dice" && isPlayersTurn;
        let player = this.state.players.get(this.playerId)!;

        if (this.state.settings.addonCitiesAndKnights && this.redDie !== undefined || this.yellowDie !== undefined) {
            // Instad of rolling dice, use alchemist card
            if (this.redDie === undefined || this.yellowDie === undefined) {
                return false;
            }
            return canRollDice
                && this.redDie >= 1 && this.redDie <= 6
                && this.yellowDie >= 1 && this.yellowDie <= 6
                && player.developmentCards.find(dcs => dcs.type === "alchemist") !== undefined;
        } else {
            return canRollDice;
        }
    }

    tracedExecute() {
        let rolledNumber: number;

        if (this.redDie !== undefined && this.yellowDie !== undefined) {
            this.state.redDie = this.redDie;
            this.state.yellowDie = this.yellowDie;
            rolledNumber = this.redDie + this.yellowDie;
            moveProgressCardBackToBank(this.state, this.playerId, "alchemist");
        } else if (this.state.settings.cardDice) {
            let [roll1, roll2] = this.pickDiceCard();
            rolledNumber = roll1 + roll2;
            this.state.redDie = roll1;
            this.state.yellowDie = roll2;
        } else {
            let roll1 = this.rollD6();
            let roll2 = this.rollD6();
            rolledNumber = roll1 + roll2;
            this.state.redDie = roll1;
            this.state.yellowDie = roll2;
        }

        this.state.eventDie = this.rollEventDie();

        this.state.rolledNumber = rolledNumber;
        this.state.rollStatisticsTwoDice[rolledNumber - 2]++;

        if (this.state.settings.addonCitiesAndKnights && this.state.eventDie === "barbarian") {
            this.state.barbarianPosition += 1;

            if (this.state.barbarianPosition >= 7) {
                this.state.barbarianAttacks += 1;

                // Determine strength of barbarians (number of cities/metropolises)
                let board = this.state.board;
                let allVertices = board.tiles.toArray()
                    .flatMap(t => [t.top, t.bottom]);
                let barbarianStrength = allVertices.filter(v => v.city).length;

                // Determine strength of defenders (strength of all active knights added up)
                let defenderStrength = new Map<string, number>();
                this.state.players.forEach(p => defenderStrength.set(p.id, 0));
                allVertices
                    .filter(v => v.knight !== undefined && v.knight.activated)
                    .forEach(v => {
                        let owner = v.knight!.ownerPlayerId;
                        let prevStrength = defenderStrength.get(owner)!;
                        defenderStrength.set(owner, prevStrength + getKnightStrength(v.knight!.level));
                    });


                let totalDefenderStrength = Array.from(defenderStrength.values()).reduce((a, b) => a + b, 0);

                if (barbarianStrength > totalDefenderStrength) {
                    // Barbarians win

                    // Find player with least knight-strength
                    // Each player loses 1 city (can choose which one)
                    let playersWithCities = new Set(allVertices
                        .filter(v => v.city && v.metropolis === undefined)
                        .map(v => v.ownerPlayerId));

                    let lowestStrength = Array.from(defenderStrength.values()).reduce((a, b) => Math.min(a, b), 99999);
                    let playersWithLowestStrengthAndCities = Array.from(playersWithCities)
                        .filter(d => defenderStrength.get(d) === lowestStrength);

                    if (playersWithLowestStrengthAndCities.length > 0) {
                        this.state.phase = "destroying-city";
                        this.state.playersDestroyingCity = new ArraySchema(...playersWithLowestStrengthAndCities);
                        updatePlayers(this.state);
                        return;
                    }
                } else {
                    // Defenders win

                    let maxStrength = Array.from(defenderStrength.values()).reduce((a, b) => Math.max(a, b), 0);
                    let playersWithHighestStrength = Array.from(this.state.players.values())
                        .filter(d => defenderStrength.get(d.id) === maxStrength);

                    if (playersWithHighestStrength.length == 1) {
                        // Single winner, assign VP
                        let winner = playersWithHighestStrength[0];
                        winner.victoryPointsVisible += 1;
                        winner.developmentCards.push(new DevelopmentCardSchema("victory-point"));
                    } else if (playersWithHighestStrength.length > 1) {
                        // 2+ players -> Each player picks one progress card
                        if (this.state.bank.scienceCardsTotal + this.state.bank.politicsCardsTotal + this.state.bank.tradeCardsTotal > 0) {
                            this.state.phase = "choosing-progresscard";
                            this.state.playersWinningBarbarianAttack = new ArraySchema(...playersWithHighestStrength.map(v => v.id));
                            updatePlayers(this.state);
                            return;
                        }

                    }
                }

                this.state.barbarianPosition = 0;
            }

        }

        handleRegularDiceRoll(this.state);

        updatePlayers(this.state);
    }

    rollD6(): D6 {
        let number = Math.floor(this.state.rand() * 6 + 1);
        this.state.rollStatistics[number - 1]++;
        return number as D6;
    }

    pickDiceCard(): [number, number] {
        let card = this.state.diceCards.nextCard();
        this.state.rollStatistics[card.roll1 - 1]++;
        this.state.rollStatistics[card.roll2 - 1]++;
        return [card.roll1, card.roll2];
    }

    rollEventDie(): EventDie {
        let roll = this.rollD6();

        switch (roll) {
            case 1:
                return "barbarian";
            case 2:
                return "barbarian";
            case 3:
                return "barbarian";
            case 4:
                return "blue";
            case 5:
                return "green";
            case 6:
                return "yellow";
        }
    }
}

export function handleRegularDiceRoll(state: SettlersSchema) {
    let rolledNumber = state.rolledNumber;
    if (rolledNumber === 7) {
        if (Array.from(state.players.values())
            .some(p => {
                let maximumCards = 7 + (3 - p.walls) * 2;
                return calculateTotalCards(p.resources) > maximumCards;
            })) {
            state.phase = "dropping-cards";
        } else {
            if (state.settings.addonCitiesAndKnights && state.barbarianAttacks == 0) {
                // The robber can only be moved after the first barbarian attack
                state.phase = "playing";
            } else {
                state.phase = "place-robber";
            }
        }
    } else {
        state.players.forEach(p => p.receivedResourcesThisTurn = 0);

        let rolledTiles: TileSchema[] = state.board.tiles
            .filter(t => t.number === rolledNumber);

        let robber = state.board.robber;
        for (let tile of rolledTiles) {
            if (robber.x === tile.x && robber.y === tile.y) {
                continue;
            }
            let vertices = state.board.getAllVerticesOfTile(tile.x, tile.y);
            for (let vertex of vertices) {
                if (vertex.ownerPlayerId !== "" && isUsefulTerrain(tile.terrainType)) {
                    let player = state.players.get(vertex.ownerPlayerId)!;
                    if (vertex.city) {
                        if (state.settings.addonCitiesAndKnights) {
                            switch (tile.terrainType) {
                                case "lumber":
                                    state.bank.takeResource(player, "paper");
                                    break;
                                case "wool":
                                    state.bank.takeResource(player, "cloth");
                                    break;
                                case "ore":
                                    state.bank.takeResource(player, "coin");
                                    break;
                                default:
                                    state.bank.takeResource(player, tile.terrainType);
                            }
                            state.bank.takeResource(player, tile.terrainType);
                        } else {
                            state.bank.takeResource(player, tile.terrainType);
                            state.bank.takeResource(player, tile.terrainType);
                        }
                    } else {
                        state.bank.takeResource(player, tile.terrainType);
                    }
                }
            }
        }

        if (state.settings.addonCitiesAndKnights && state.eventDie !== "barbarian") {
            // Distribute
            for (let i = 0; i < state.playerOrder.length; i++) {
                const index = (state.currentPlayerIndex + i) % state.playerOrder.length;
                const playerId = state.playerOrder[index];
                const player = state.players.get(playerId)!;

                const cityUpgradeColor = state.eventDie!;
                const cityUpgradeLevel = player.cityUpgrades[cityUpgradeColor];

                const playerGetsCard = (() => {
                    switch (cityUpgradeLevel) {
                        case 0:
                            return false;
                        case 1:
                            return state.redDie <= 2;
                        case 2:
                            return state.redDie <= 3;
                        case 3:
                            return state.redDie <= 4;
                        case 4:
                            return state.redDie <= 5;
                        case 5:
                            return true;
                    }
                })();

                if (playerGetsCard) {
                    state.bank.pickProgressCard(player, cityUpgradeColor);
                }
            }
        }

        // Players with green 3rd level upgrade can choose resource
        const playersReceivingNoResource = Array.from(state.players.values())
            .filter(p => p.cityUpgrades.green >= 3 && p.receivedResourcesThisTurn === 0);
        if (state.settings.addonCitiesAndKnights && playersReceivingNoResource.length > 0) {
            state.phase = "choosing-resource";
        } else {
            state.phase = "playing";
        }
    }
}
