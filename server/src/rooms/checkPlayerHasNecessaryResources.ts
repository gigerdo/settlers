import {PlayerSchema} from "./schema/PlayerSchema";
import {Resource, ResourcesSchema} from "./schema/ResourcesSchema";

export function checkPlayerHasNecessaryResources(player: PlayerSchema, tradeResources: ResourcesSchema): boolean {
    if (!tradeResources) {
        return false;
    }
    return !Object.entries(tradeResources).some(([key, value]) => {
        let availableResource = player.resources[key as Resource]!;
        return availableResource < value;
    });
}