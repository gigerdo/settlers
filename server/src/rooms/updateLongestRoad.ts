import {SettlersSchema} from "./schema/SettlersSchema";
import {EdgeLoc, EdgeSchema, isSameEdge} from "./schema/EdgeSchema";
import {VertexSchema} from "./schema/VertexSchema";
import {ArraySchema} from "@colyseus/schema";

export function updateLongestRoad(state: SettlersSchema) {
    let roadEdges = new Map<string, EdgeSchema[]>();
    for (let tile of state.board.tiles) {
        let edgeLocs: EdgeLoc[] = ["north", "east", "south"];
        for (let loc of edgeLocs) {
            let edge = tile[loc];
            let ownerId = edge.ownerPlayerId;
            if (ownerId !== "") {
                if (!roadEdges.has(ownerId)) {
                    roadEdges.set(ownerId, []);
                }
                roadEdges.get(ownerId)!.push(edge);
            }
        }
    }

    let currentPlayerWithLongestRoad = Array.from(state.players.values()).find(p => p.longestRoad);

    let overallLongestRoad = 0;
    let playerWithLongestRoadId: string;
    for (let [playerId, edges] of roadEdges.entries()) {
        let playersLongestRoadPath: EdgeSchema[] = [];

        let calculateRoad = (currentVertex: VertexSchema, walkedPath: EdgeSchema[]): EdgeSchema[] => {
            let longestPath = [...walkedPath];
            if (currentVertex.ownerPlayerId !== "" && currentVertex.ownerPlayerId !== playerId) {
                return longestPath;
            }

            if (state.settings.addonCitiesAndKnights && currentVertex.knight !== undefined && currentVertex.knight.ownerPlayerId !== playerId) {
                return longestPath;
            }

            let edges = state.board.getEdgesOfVertex(currentVertex.x, currentVertex.y, currentVertex.loc);
            for (let edge of edges) {
                if (edge.ownerPlayerId !== playerId) {
                    continue;
                }
                if (walkedPath.some(walkedEdge => isSameEdge(walkedEdge, edge))) {
                    continue;
                }
                let nextVertex = state.board.getVerticesOfEdge(edge.x, edge.y, edge.loc)
                    .filter(v => v.x !== currentVertex.x || v.y !== currentVertex.y || v.loc !== currentVertex.loc);
                if (nextVertex.length > 0) {
                    let path = calculateRoad(nextVertex[0], [...walkedPath, edge]);
                    if (path.length > longestPath.length) {
                        longestPath = path;
                    }
                }
            }
            return longestPath;
        }

        for (let edge of edges) {
            let vertices = state.board.getVerticesOfEdge(edge.x, edge.y, edge.loc);
            for (let vertex of vertices) {
                let path = calculateRoad(vertex, []);
                if (path.length > playersLongestRoadPath.length) {
                    playersLongestRoadPath = path;
                }
            }
        }

        if (playersLongestRoadPath.length > overallLongestRoad) {
            overallLongestRoad = playersLongestRoadPath.length;
            playerWithLongestRoadId = playerId;
        }
        state.players.get(playerId)!.longestRoadPath = new ArraySchema<EdgeSchema>(...playersLongestRoadPath);
    }

    let playerWithLongestRoad = state.players.get(playerWithLongestRoadId!)!;
    if (currentPlayerWithLongestRoad) {
        if (overallLongestRoad < 5) {
            currentPlayerWithLongestRoad.longestRoad = false;
            currentPlayerWithLongestRoad.victoryPointsVisible -= 2;
        } else if (overallLongestRoad > currentPlayerWithLongestRoad.longestRoadPath.length) {
            currentPlayerWithLongestRoad.longestRoad = false;
            currentPlayerWithLongestRoad.victoryPointsVisible -= 2;
            playerWithLongestRoad.longestRoad = true;
            playerWithLongestRoad.victoryPointsVisible += 2;
        }
    } else if (overallLongestRoad >= 5) {
        playerWithLongestRoad.longestRoad = true;
        playerWithLongestRoad.victoryPointsVisible += 2;
    }
}