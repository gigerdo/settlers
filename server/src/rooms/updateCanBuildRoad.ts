import {TileSchema} from "./schema/TileSchema";
import {SettlersSchema} from "./schema/SettlersSchema";
import {EdgeLoc} from "./schema/EdgeSchema";

export function updateCanBuildRoad(state: SettlersSchema, playerId: string) {
    let updateCanBuildRoad = (tile: TileSchema, loc: EdgeLoc) => {
        let vertices = state.board.getVerticesOfEdge(tile.x, tile.y, loc);
        let hasNeighbouringSettlement = vertices.some(v => v.ownerPlayerId === playerId);
        let hasLand = state.board.getTilesOfEdge(tile.x, tile.y, loc).some(t => t.terrainType !== "water");
        let hasNeighbouringRoad = false;
        for (let vertex of vertices) {
            if (vertex.ownerPlayerId !== "") continue;
            if (state.settings.addonCitiesAndKnights) {
                if (vertex.knight !== undefined && vertex.knight.ownerPlayerId !== playerId) {
                    continue;
                }
            }
            let edgesOfVertex = state.board.getEdgesOfVertex(vertex.x, vertex.y, vertex.loc);
            for (let edge of edgesOfVertex) {
                if (edge.ownerPlayerId === playerId) {
                    hasNeighbouringRoad = true;
                    break;
                }
            }
        }
        tile[loc].canPlace = hasLand && (hasNeighbouringSettlement || hasNeighbouringRoad);
    }

    for (let tile of state.board.tiles) {
        updateCanBuildRoad(tile, "north");
        updateCanBuildRoad(tile, "east");
        updateCanBuildRoad(tile, "south");
    }
}