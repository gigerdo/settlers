import {SettlersSchema} from "./schema/SettlersSchema";
import {calculateTotalCards} from "./calculateTotalCards";
import {tracer} from "./SettlersRoom";

export function updatePlayers(state: SettlersSchema) {
    tracer.startActiveSpan("updatePlayers", span => {
        for (let player of Array.from(state.players.values())) {
            player.canBuildRoad = player.resources.lumber >= 1 && player.resources.brick >= 1 && player.roads > 0;
            player.canBuildSettlement = player.resources.lumber >= 1 && player.resources.brick >= 1
                && player.resources.wool >= 1 && player.resources.grain >= 1 && player.settlements > 0;
            player.canBuildCity = player.resources.grain >= 2 && player.resources.ore >= 3 && player.cities > 0;
            player.canBuyDevelopmentCard = player.resources.wool >= 1 && player.resources.grain >= 1 && player.resources.ore >= 1;
            player.canBuildWall = state.settings.addonCitiesAndKnights && player.resources.brick >= 2 && player.walls > 0;
            player.canBuyKnight = state.settings.addonCitiesAndKnights && player.resources.wool >= 1 && player.resources.ore >= 1 && player.basicKnights > 0;
            player.canPromoteKnight = state.settings.addonCitiesAndKnights
                && player.resources.wool >= 1 && player.resources.ore >= 1
                && player.basicKnights < 2
                && (player.strongKnights > 0 || player.mightyKnights > 0);

            player.totalDevelopmentCards = player.developmentCards.length;
            player.resourcesTotal = calculateTotalCards(player.resources);
        }
        span.end();
    });
}