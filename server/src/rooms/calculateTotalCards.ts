import {TradeResources} from "./schema/TradeSchema";

export function calculateTotalCards(resources: TradeResources): number {
    return resources.lumber
        + resources.brick
        + resources.wool
        + resources.grain
        + resources.ore
        + resources.paper
        + resources.cloth
        + resources.coin;
}