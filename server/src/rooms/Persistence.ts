import {Pool} from "pg";
import {SettlersSchema} from "./schema/SettlersSchema";

class Persistence {
    private pool: Pool | undefined;

    constructor() {
        if (!process.env.DATABASE_URL) {
            console.log("Not connected to database");
            return
        }
        this.pool = new Pool({
            connectionString: process.env.DATABASE_URL,
            ssl: false,
        });
        console.log("Connected to database");

        this.migrate()
            .then(() => console.log("Migration finished"))
            .catch(error => console.error("Migration failed", error));
    }


    async migrate() {
        let versionTableExists: boolean = await this.pool!.query(` SELECT EXISTS (
                        SELECT FROM pg_tables
                        WHERE  schemaname = 'public'
                        AND    tablename  = 'version'
                        );`)
            .then(res => res.rows[0].exists);

        if (!versionTableExists) {
            await this.pool!.query(`create table version
                                    (
                                        current_version bigint
                                    );
            insert into version(current_version)
            values (0);`);
        }

        let currentVersion = await this.pool!.query(`select current_version
                                                     from version;`)
            .then(r => r.rows[0].current_version);

        console.log("Current version", currentVersion);
        let version = Number(currentVersion);
        if (version === 0) {
            console.log("Migration to 1: Create game_save table");
            await this.pool!.query(`
                create table game_save
                (
                    room_id text unique,
                    state   text
                );
                update version
                set current_version = 1;
            `);
        }
        if (version <= 1) {
            console.log("Migration to 2: Add timestamp column");
            await this.pool!.query(`
                alter table game_save
                    add column created_at timestamp default now();
                update version
                set current_version = 2;
            `);
        }
    }

    async storeRoomState(roomId: string, currentState: SettlersSchema) {
        if (!this.pool) return;
        console.log("Storing state to db");
        let serializedState = JSON.stringify(currentState.toJSON());
        await this.pool.query(`insert into game_save (room_id, state)
                               values ($1, $2) on conflict (room_id)
                                do
        update set state=excluded.state;`, [roomId, serializedState])
            .catch(err => {
                console.error(err);
            });
    }

    async loadRoomState(roomId: string): Promise<SettlersSchema | undefined> {
        if (!this.pool) return undefined;
        console.log("Loading state from db");
        return await this.pool.query(`select state
                                      from game_save
                                      where room_id = $1`, [roomId])
            .then(res => res.rows[0].state)
            .then(res => JSON.parse(res))
            .catch(err => {
                console.error(err);
            });
    }
}

export const persistence = new Persistence();