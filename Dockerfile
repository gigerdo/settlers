FROM node:20 AS build
COPY server /server
COPY client /client
WORKDIR /server
RUN npm ci && npm run build
WORKDIR /client
COPY client /client
RUN npm ci && npm run build

FROM node:20
COPY --from=build /server/lib /server/lib
COPY --from=build /server/node_modules /server/node_modules
COPY --from=build /client/build /client/build
CMD ["node", "--require", "./server/lib/instrumentation.js", "/server/lib/index.js"]