import React, {ReactElement, ReactNode, useContext} from "react";
import {RoomStateContext} from "../hooks/RoomStateContext";

interface Props {
    children: ReactNode;
}

/** Only show children if Cities&Knights addon is enabled. */
export function CitiesAndKnights(props: Props): ReactElement {
    const {roomState} = useContext(RoomStateContext);
    const citiesAndKnightsEnabled = roomState.settings.addonCitiesAndKnights;

    if (citiesAndKnightsEnabled) {
        return <>{props.children}</>;
    } else {
        return <></>
    }
}