import React, {ReactElement, ReactNode, useContext} from "react";
import {RoomStateContext} from "../hooks/RoomStateContext";

interface Props {
    children: ReactNode;
}

/** Only show children for vanilla game (no addons) */
export function VanillaOnly(props: Props): ReactElement {
    const {roomState} = useContext(RoomStateContext);
    const citiesAndKnightsDisabled = !roomState.settings.addonCitiesAndKnights;

    if (citiesAndKnightsDisabled) {
        return <>{props.children}</>;
    } else {
        return <></>
    }
}