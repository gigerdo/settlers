import {Room} from "colyseus.js";
import React, {useEffect, useRef, useState} from "react";
import {useNavigate, useParams} from "react-router";
import {client} from "./command/client";
import {SettlersSchema} from "../../server/src/rooms/schema/SettlersSchema";
import {Board} from "./panels/board/Board";
import {PlayerInfo} from "./panels/PlayerInfo";
import {CommandsDispatch} from "./command/CommandsDispatch";
import {ResourceInfo} from "./panels/ResourceInfo";
import {Actions} from "./panels/Actions";
import {BankInfo} from "./panels/BankInfo";
import {DiscardCardsModal} from "./modals/DiscardCardsModal";
import {Slide, toast, ToastContainer} from "react-toastify";
import {Chat} from "./panels/Chat";
import "./RoomView.css";
import {Modal} from "./modals/Modal";
import {GameSettings} from "../../server/src/rooms/schema/GameSettingsSchema";
import {useLocalStorage} from "./hooks/useLocalStorage";
import {RoomStateContext} from "./hooks/RoomStateContext";
import {CityUpgrades} from "./panels/CityUpgrades";
import {CitiesAndKnights} from "./addons/CitiesAndKnights";
import {VanillaOnly} from "./addons/VanillaOnly";
import {DevelopmentCardsInfo} from "./panels/DevelopmentCardsInfo";
import {ChooseResourceModal} from "./modals/ChooseResourceModal";
import {ChooseProgressCardModal} from "./modals/ChooseProgressCardModal";

interface Props {
}

interface Params {
    roomId: string;
}

export function RoomView(props: Props) {
    let navigate = useNavigate();
    let {roomId} = useParams();
    let roomRef = useRef<Room<SettlersSchema>>();

    let state = roomRef.current?.state;
    let [, setUpdated] = useState<number>(0);
    let commandsDispatch = new CommandsDispatch(roomRef.current!);
    let [showSpots, setShowSpots] = useState<string | undefined>(undefined);

    let [connected, setConnected] = useState<boolean>(true);

    let [settings, setSettings] = useState<GameSettings>({
        moreDesert: false,
        cardDice: true,
        addonCitiesAndKnights: false,
    });

    const {userId, userName} = useLocalStorage();

    function connectToRoom(userId: string, userName: string) {
        console.log("Starting room", roomId);
        client.joinById<SettlersSchema>(roomId, {userId: userId, userName: userName})
            .catch(err => {
                console.error("Join room failed", err);
                return client.create<SettlersSchema>("settlers", {
                    roomId: roomId,
                    userId: userId,
                    userName: userName
                });
            })
            .then(room => {
                console.log("Joined room", room.id);
                roomRef.current = room;
                setUpdated(prev => prev + 1);
                setConnected(true);
                room.onStateChange(state => {
                    console.log("State updated");
                    setUpdated(prev => prev + 1);
                });

                room.onLeave(number => {
                    console.log("Connection lost");
                    setConnected(false);
                });

                console.log("Registering trade onmessage");
                room.onMessage("trade", message => {
                    console.log("Trade message received", message);
                    switch (message) {
                        case "traded-with-bank":
                            toast("💸 Traded 4:1 with bank.");
                            break;
                        case "traded-three-to-one":
                            toast("🚢 Traded 3:1 with harbor.");
                            break;
                        case "traded-two-to-one":
                            toast("🚢 Traded 2:1 with harbor.");
                            break;
                        case "traded-two-to-one-merchant":
                            toast("⚖️ Traded 2:1 with merchant.");
                            break;
                        case "traded-with-trading-house":
                            toast("⚖️ Traded 2:1 with trading house.");
                            break;
                    }
                });

                room.onError((code, message) => {
                    console.error("onError", code, message);
                })
            })
            .catch(err => {
                console.error("Reconnect failed", err);
                navigate("/");
            });
    }

    useEffect(() => {
        if (!userId || !userName) {
            navigate("/");
            return;
        }
        connectToRoom(userId!, userName!);
        return () => {
            console.log("Leaving room", roomRef.current?.id);
            roomRef.current?.leave();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (!state) {
        return <></>;
    }

    let playerId = Array.from(state.players.values())
        .find(value => value.id === userId)
        ?.id;
    if (!playerId) {
        return <>Connection not possible: Game has already started.</>;
    }
    return (
        <RoomStateContext.Provider value={{
            playerId: playerId,
            roomState: state,
            dispatch: commandsDispatch,
            player: state.players.get(playerId)!,
            isPlayersTurn: state.currentPlayerIndex === state.playerOrder.indexOf(playerId),
        }}>
            {state.phase === "setup" &&
            <>
                <h2>Room Info</h2>
                <div>Room: {roomRef.current?.id}</div>
                <div>User: {userName}</div>
                <h2>Players</h2>
                <ul>
                    {Array.from(state.players.values()).map(p => <li key={p.id}>
                        {p.name}
                    </li>)}
                </ul>
                <h2>Settings</h2>
                <div>
                    <input
                        type="checkbox"
                        id="more-desert"
                        checked={settings.moreDesert}
                        onChange={event => {
                            setSettings(prev => ({...prev, moreDesert: event.target.checked}))
                        }}
                    />
                    <label htmlFor="more-desert">Replace 3 resources with desert to balance for 3 players.</label>
                </div>
                <div>
                    <input
                        type="checkbox"
                        id="card-dice"
                        checked={settings.cardDice}
                        onChange={event => {
                            setSettings(prev => ({...prev, cardDice: event.target.checked}))
                        }}
                    />
                    <label htmlFor="card-dice">Use cards instead of dice (Draw from a deck of 36 cards with all possible
                        combinations of dice rolls).</label>
                </div>
                <div>
                    <input
                        type="checkbox"
                        id="addon-cities-knights"
                        checked={settings.addonCitiesAndKnights}
                        onChange={event => {
                            setSettings(prev => ({...prev, addonCitiesAndKnights: event.target.checked}));
                        }}
                    />
                    <label htmlFor="addon-cities-knights">
                        Enable Citites&Knights addon. (Not fully implemented!)
                    </label>
                </div>
                <div style={{marginTop: "20px"}}>
                    <button onClick={() => commandsDispatch.startGame(settings)}>
                        <h1>Start Game</h1>
                    </button>
                </div>

            </>}
            {(state.phase !== "setup") &&
            <>
                {state.phase === "finished" &&
                <h1 style={{background: state.players.get(state.winner)!.color, padding: "10px"}}>
                    Winner: {state.players.get(state.winner)!.name} ({state.players.get(state.winner)!.color})
                </h1>}
                <div>
                    <ToastContainer
                        position={"top-center"}
                        closeButton={false}
                        transition={Slide}
                    />
                    <div
                        className="main-grid"
                        style={{
                            display: "grid",
                            gap: "5px"
                        }}
                    >
                        <div style={{
                            gridArea: "resources",
                            gridTemplateColumns: "auto auto auto",
                            display: "grid",
                            gap: "5px",
                        }}>
                            <div style={{gridColumn: "1/-1"}}>
                                <ResourceInfo
                                    playerId={playerId}
                                    state={state}
                                    dispatch={commandsDispatch}
                                />
                            </div>

                            <VanillaOnly>
                                <fieldset style={{gridColumn: "1/2"}}>
                                    <legend><b>Actions</b></legend>
                                    <Actions
                                        playerId={playerId}
                                        dispatch={commandsDispatch}
                                        state={state}
                                        setShowSpots={setShowSpots}
                                    />
                                </fieldset>
                                <DevelopmentCardsInfo
                                    setShowSpots={setShowSpots}
                                />
                            </VanillaOnly>
                            <CitiesAndKnights>
                                <fieldset>
                                    <legend><b>Actions</b></legend>
                                    <Actions
                                        playerId={playerId}
                                        dispatch={commandsDispatch}
                                        state={state}
                                        setShowSpots={setShowSpots}
                                    />
                                </fieldset>
                                <DevelopmentCardsInfo
                                    setShowSpots={setShowSpots}
                                />
                                <fieldset>
                                    <legend><b>City Upgrades</b></legend>
                                    <div style={{display: "grid", gridTemplateColumns: "repeat(3, 1fr)", gap: "10px"}}>
                                        <CityUpgrades color="yellow"/>
                                        <CityUpgrades color="blue"/>
                                        <CityUpgrades color="green"/>
                                    </div>
                                </fieldset>
                            </CitiesAndKnights>
                        </div>
                        <fieldset style={{gridArea: "players"}}>
                            <legend><b>Players</b></legend>
                            <PlayerInfo
                                playerId={playerId}
                                state={state}
                            />
                        </fieldset>
                        <fieldset style={{gridArea: "bank"}}>
                            <legend><b>Bank</b></legend>
                            <BankInfo state={state}/>
                        </fieldset>
                        <fieldset style={{gridArea: "board", width: "950px"}}>
                            <legend
                                title={`Phase:${state.phase}, show-spots:${showSpots}`}
                            >
                                <b>Board</b>
                            </legend>
                            <Board
                                playerId={playerId}
                                state={state}
                                dispatch={commandsDispatch}
                                showSpots={showSpots}
                                setShowSpots={setShowSpots}
                            />
                        </fieldset>
                        <fieldset style={{gridArea: "chat"}}>
                            <legend><b>Chat</b></legend>
                            <Chat
                                room={roomRef.current!}
                            />
                        </fieldset>
                    </div>
                </div>
                {state.phase === "dropping-cards" &&
                <DiscardCardsModal
                    state={state}
                    dispatch={commandsDispatch}
                    playerId={playerId}
                />}
                {state.phase === "choosing-resource" &&
                    <ChooseResourceModal
                        state={state}
                        dispatch={commandsDispatch}
                        playerId={playerId}
                    />}
                {state.phase === "choosing-progresscard" &&
                    <ChooseProgressCardModal
                        state={state}
                        dispatch={commandsDispatch}
                        playerId={playerId}
                    />}
            </>}
            {!connected && <Modal close={() => {
            }}>
                <h2>Disconnected!</h2>
                Try to reconnect:
                <button onClick={() => connectToRoom(userId!, userName!)}>Reconnect</button>
            </Modal>}
        </RoomStateContext.Provider>
    );
}
