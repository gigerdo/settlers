import {createContext} from "react";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {CommandsDispatch} from "../command/CommandsDispatch";
import {PlayerSchema} from "../../../server/src/rooms/schema/PlayerSchema";

interface RoomStateContextType {
    playerId: string;
    roomState: SettlersSchema;
    dispatch: CommandsDispatch;
    player: PlayerSchema;
    isPlayersTurn: boolean;
}

export const RoomStateContext = createContext<RoomStateContextType>({
    playerId: undefined!,
    roomState: undefined!,
    dispatch: undefined!,
    player: undefined!,
    isPlayersTurn: false,
});