import {useCallback, useState} from "react";

export function useLocalStorage() {
    const [, setState] = useState(0); // Counter just to force react re-render

    const setUserId = useCallback((userId: string) => {
        // Only set the user-id if none is set yet
        if (!localStorage.getItem("userId")) {
            console.log("Setting userId", userId);
            localStorage.setItem("userId", userId);
            setState(prev => prev + 1);
        }
    }, []);

    const setUserName = useCallback((userName: string) => {
        console.log("Setting username", userName);
        localStorage.setItem("userName", userName)
        setState(prev => prev + 1);
    }, []);

    return {
        userId: localStorage.getItem("userId"),
        setUserId: setUserId,
        userName: localStorage.getItem("userName"),
        setUserName: setUserName,
    }
}