import React from "react";
import {BrowserRouter, Route, Routes} from "react-router";
import {Menu} from "./Menu";
import {RoomView} from "./RoomView";
import "./App.css";

function App() {

    return (
        <BrowserRouter>
            <Routes>
                <Route path="/room/:roomId" element={<RoomView/>}/>
                <Route path="/" element={<Menu/>}/>
            </Routes>
            <pre style={{marginTop: "50px"}}>Source: <a href="https://gitlab.com/gigerdo/settlers"
                                                        rel="noreferrer"
                                                        target="_blank">https://gitlab.com/gigerdo/settlers</a>
            </pre>
        </BrowserRouter>
    );
}

export default App;
