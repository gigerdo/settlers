import React, {useRef, useState} from "react";

interface Props {
    userName: string | null;
    setUserName: (newUserName: string) => void;
}

export function UserNameInput({userName, setUserName}: Props) {
    const ref = useRef<HTMLInputElement>(null);
    const [editing, setEditing] = useState(userName == null);

    return <form>
        <div style={{display: "grid", gridTemplateColumns: "max-content min-content min-content", gap: "10px"}}>
            {userName && !editing && <>
                <div>Nickname:</div>
                <b>{userName}</b>
                <button onClick={() => setEditing(true)}>
                    Edit
                </button>
            </>}
            {editing && <>
                <div>Enter nickname:</div>
                <input ref={ref} defaultValue={userName ?? ""}/>
                <button onClick={() => {
                    ref.current?.value && setUserName(ref.current.value)
                    setEditing(false);
                }}>
                    Save
                </button>
            </>}
        </div>
    </form>
}