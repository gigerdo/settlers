import {Client} from "colyseus.js";

let HOST = window.location.origin.replace(/^http/, 'ws')
if (window.location.hostname === "localhost") {
    HOST = "ws://localhost:2567";
}
export let client = new Client(HOST);
