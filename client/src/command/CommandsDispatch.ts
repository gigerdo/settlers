import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {Room} from "colyseus.js";
import {DevelopmentCardType, ProgressCard} from "../../../server/src/rooms/schema/DevelopmentCardSchema";
import {EdgeLoc} from "../../../server/src/rooms/schema/EdgeSchema";
import {NewTradeState, TradeResources} from "../../../server/src/rooms/schema/TradeSchema";
import {VertexLoc} from "../../../server/src/rooms/schema/VertexSchema";
import {GameSettings} from "../../../server/src/rooms/schema/GameSettingsSchema";
import {CityUpgradeColor} from "../../../server/src/rooms/schema/CityUpgradeSchema";
import {Resource} from "../../../server/src/rooms/schema/ResourcesSchema";

export class CommandsDispatch {
    private room: Room<SettlersSchema>;

    constructor(room: Room<SettlersSchema>) {
        this.room = room;
    }

    startGame(settings: GameSettings) {
        console.log("Start game");
        this.room.send("command", {name: "start-game", settings: settings});
    }

    placeSettlement(index: number, loc: VertexLoc) {
        console.log("Place settlement");
        this.room.send("command", {name: "place-settlement", index: index, loc: loc});
    }

    buildSettlement(index: number, loc: VertexLoc) {
        console.log("Build settlement");
        this.room.send("command", {name: "build-settlement", index: index, loc: loc});
    }

    placeRoad(index: number, loc: EdgeLoc) {
        console.log("Place road");
        this.room.send("command", {name: "place-road", index: index, loc: loc});
    }

    buildRoad(index: number, loc: EdgeLoc) {
        console.log("Build road", index, loc);
        this.room.send("command", {name: "build-road", index: index, loc: loc});
    }

    rollDice(redDie?: number, yellowDie?: number) {
        console.log("Roll dice");
        this.room.send("command", {name: "roll-dice", redDie, yellowDie});
    }

    finishTurn() {
        console.log("Finish turn");
        this.room.send("command", {name: "finish-turn"});
    }

    buildCity(index: number, loc: VertexLoc, useMedicineCard: Boolean) {
        console.log("Build city");
        this.room.send("command", {name: "build-city", index: index, loc: loc, useMedicineCard: useMedicineCard});
    }

    buildWall(index: number, loc: VertexLoc, useEngineerCard: Boolean) {
        console.log("Build wall");
        this.room.send("command", {name: "build-wall", index: index, loc: loc, useEngineerCard: useEngineerCard});
    }

    buildMetropolis(index: number, loc: VertexLoc) {
        console.log("Build metropolis");
        this.room.send("command", {name: "build-metropolis", index: index, loc: loc});
    }

    buyKnight(index: number, loc: VertexLoc) {
        console.log("Buy Knight");
        this.room.send("command", {name: "buy-knight", index: index, loc: loc});
    }

    buyDevelopmentCard() {
        console.log("Buy development card");
        this.room.send("command", {name: "buy-development-card"});
    }

    placeRobber(x: number, y: number, stealFromPlayer: string) {
        console.log("Place robber");
        this.room.send("command", {name: "place-robber", x: x, y: y, stealFromPlayer: stealFromPlayer});
    }

    playDevelopmentCard(card: DevelopmentCardType) {
        console.log(`Play development card ${card}`);
        this.room.send("command", {name: "play-development-card", card: card});
    }

    playYearOfPlenty(first: string, second: string) {
        console.log(`Play year of plenty`);
        this.room.send("command", {
            name: "play-development-card",
            card: "year-of-plenty",
            firstResource: first,
            secondResource: second,
        });
    }

    playMonopoly(resource: string) {
        console.log(`Play year of plenty`);
        this.room.send("command", {
            name: "play-development-card",
            card: "monopoly",
            monopoly: resource,
        });
    }

    offerTrade(newTrade: NewTradeState) {
        console.log("New trade offer");
        this.room.send("command", {name: "new-trade-offer", trade: newTrade});
    }

    cancelTradeOffer(id: string) {
        console.log("Cancelled Offer");
        this.room.send("command", {name: "cancel-trade-offer", tradeOfferId: id});
    }

    acceptTrade(id: string) {
        console.log("Accepted trade");
        this.room.send("command", {name: "answer-trade-offer", tradeOfferId: id, acceptOffer: true});
    }

    declineTrade(id: string) {
        console.log("Declined trade");
        this.room.send("command", {name: "answer-trade-offer", tradeOfferId: id, acceptOffer: false});
    }

    executeTrade(tradeOfferId: string, playerId: string) {
        console.log("Execute trade");
        this.room.send("command", {name: "execute-trade", tradeOfferId: tradeOfferId, playerId: playerId});
    }

    discardCards(resourcesToDiscard: TradeResources) {
        console.log("Discarding Cards");
        this.room.send("command", {name: "discard-cards", resources: resourcesToDiscard});
    }

    chooseResource(resourceToReceive: Resource | undefined) {
        console.log("Choose Resource");
        this.room.send("command", {name: "choose-resource", resourceToReceive: resourceToReceive});
    }

    buyCityUpgrade(cityUpgradeColor: CityUpgradeColor, useCrane: boolean) {
        console.log("Buying upgrade for ", cityUpgradeColor);
        this.room.send("command", {name: "buy-city-upgrade", color: cityUpgradeColor, useCrane: useCrane});
    }

    activateKnight(index: number, loc: VertexLoc) {
        console.log("Activate knight", index, loc);
        this.room.send("command", {name: "activate-knight", index: index, loc: loc});
    }

    promoteKnight(index: number, loc: VertexLoc) {
        console.log("Promote knight", index, loc);
        this.room.send("command", {name: "promote-knight", index: index, loc: loc});
    }

    moveKnight(fromIndex: number, fromLoc: VertexLoc, toIndex: number, toLoc: VertexLoc) {
        console.log("Move knight");
        this.room.send("command", {name: "move-knight", fromIndex, fromLoc, toIndex, toLoc});
    }

    chaseAwayRobber(index: number, loc: VertexLoc) {
        console.log("Expelling robber");
        this.room.send("command", {name: "chase-away-robber", index: index, loc: loc});
    }

    destroyCity(index: number, loc: VertexLoc) {
        console.log("Destroy city");
        this.room.send("command", {name: "destroy-city", index: index, loc: loc});
    }

    chooseProgressCard(color: CityUpgradeColor) {
        console.log("Choosing progress card", color);
        this.room.send("command", {name: "choose-progress-card", color: color});
    }

    playProgressCard(
        card: ProgressCard,
        resource?: Resource,
        player?: string,
    ) {
        console.log("Play progress card", card);
        this.room.send("command", {
            name: "play-progress-card",
            card: card,
            resource: resource,
            player: player,
        });
    }

    playProgressCardLoc(
        card: ProgressCard,
        index: number,
        loc?: VertexLoc | EdgeLoc,
    ) {
        console.log("Play progress card", card);
        this.room.send("command", {
            name: "play-progress-card",
            card: card,
            index,
            loc,
        });
    }

    removeKnight(index: number, loc: VertexLoc) {
        console.log("Remove knight");
        this.room.send("command", {name: "remove-knight", index: index, loc: loc});
    }

    skip() {
        console.log("Skipping action");
        this.room.send("command", {name: "skip"});
    }
}