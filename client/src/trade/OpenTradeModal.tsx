import React, {useState} from "react";
import {Modal} from "../modals/Modal";
import {CommandsDispatch} from "../command/CommandsDispatch";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {NewTradeState} from "../../../server/src/rooms/schema/TradeSchema";
import {VisualizeResources} from "./VisualizeResources";
import {PlayerSchema} from "../../../server/src/rooms/schema/PlayerSchema";
import {Resource} from "../../../server/src/rooms/schema/ResourcesSchema";
import {TradeResourceButton} from "./TradeResourceButton";
import {HarborType} from "../../../server/src/rooms/schema/EdgeSchema";

interface Props {
    close: () => void;
    player: PlayerSchema;
    state: SettlersSchema;
    dispatch: CommandsDispatch;
}

const emptyTrade = {
    give: {lumber: 0, brick: 0, wool: 0, grain: 0, ore: 0, paper: 0, cloth: 0, coin: 0},
    receive: {lumber: 0, brick: 0, wool: 0, grain: 0, ore: 0, paper: 0, cloth: 0, coin: 0},
};

export function OpenTradeModal(props: Props) {
    let [newTrade, setNewTrade] = useState<NewTradeState>(emptyTrade);

    let trades = Array.from(props.state.openTrades.values());

    const update = (side: "give" | "receive", resource: Resource, amount: number) => {
        setNewTrade(prev => {
            const next = {give: {...prev.give}, receive: {...prev.receive}};
            next[side][resource] += amount;
            return next;
        });
    };

    const citiesAndKnightsResources: Resource[] = props.state.settings.addonCitiesAndKnights ? ["paper", "cloth", "coin"] : [];
    const resources: Resource[] = ["lumber", "brick", "wool", "grain", "ore", ...citiesAndKnightsResources];

    return <Modal close={props.close}>
        <button style={{float: "right"}} onClick={props.close}>X</button>
        <h2>Trading</h2>
        Available trade posts:
        <ul>
            {props.state.board.merchantOwner === props.player.id &&
                <li>2:1 {props.state.board.tiles[props.state.board.merchantIndex!].terrainType} (merchant)</li>}
            {Array.from(props.player.harbors.values())
                .sort((a, b) => {
                    if (a > b) {
                        return 1;
                    }
                    if (a < b) {
                        return -1;
                    }
                    return 0;
                })
                .map((h: HarborType) => {
                    switch (h) {
                        case "three-to-one":
                            return <li key={h}>3:1 any resource {props.state.settings.addonCitiesAndKnights && <>or
                                commodity</>} (port)</li>;
                        default:
                            return <li key={h}>2:1 {h} (port)</li>;
                    }
                })}
            {props.player.cityUpgrades.yellow >= 3 &&
                <li>2:1 any commodity (trading house)</li>}
            {!props.player.harbors.has("three-to-one") &&
                <li>4:1 any resource {props.state.settings.addonCitiesAndKnights && <>or commodity</>}</li>}
        </ul>
        <h3>New Trade</h3>
        <fieldset style={{height: "50px"}}>
            <legend style={{height: "25px"}}>
                <b>Give </b>
                {resources.map(resource => <TradeResourceButton
                    key={resource}
                    resource={resource}
                    resourceAmount={props.player.resources[resource] - newTrade.give[resource]}
                    onClick={() => update("give", resource, +1)}
                />)}
            </legend>
            {resources.map(resource => <TradeResourceButton
                key={resource}
                resource={resource}
                resourceAmount={newTrade.give[resource]}
                onClick={() => update("give", resource, -1)}
            />)}
        </fieldset>

        <div style={{height: "10px"}}/>
        <fieldset style={{height: "50px"}}>
            <legend style={{height: "25px"}}>
                <b>Receive </b>
                {resources.map(resource => <TradeResourceButton
                    key={resource}
                    resource={resource}
                    resourceAmount={1}
                    onClick={() => update("receive", resource, +1)}
                    hideAmount={true}
                />)}
            </legend>
            {resources.map(resource => <TradeResourceButton
                key={resource}
                resource={resource}
                resourceAmount={newTrade.receive[resource]}
                onClick={() => update("receive", resource, -1)}
                hideAmount={false}
            />)}
        </fieldset>
        <div style={{height: "20px"}}/>
        <button
            disabled={!Object.values(newTrade.give).some(v => v > 0)
                || !Object.values(newTrade.receive).some(v => v > 0)}
            onClick={() => {
                props.dispatch.offerTrade(newTrade);
                setNewTrade(emptyTrade);
            }}
        >
            Offer Trade
        </button>

        <h3>Open Trades</h3>
        <table style={{width: "100%"}}>
            <thead>
            <tr>
                <th>Give</th>
                <th>Receive</th>
                <th>Accepted</th>
                <th>Declined</th>
                <th/>
            </tr>
            </thead>
            <tbody>
            {trades.map(trade => <tr key={trade.id}>
                <td><VisualizeResources resources={trade.give}/></td>
                <td><VisualizeResources resources={trade.receive}/></td>
                <td>
                    {Array.from(trade.accepted.values()).map(id => {
                        let player = props.state.players.get(id)!;
                        return <button key={player.id} onClick={() => props.dispatch.executeTrade(trade.id, player.id)}>
                            {player.name}
                        </button>;
                    })}
                </td>
                <td>
                    {Array.from(trade.rejected.values()).map(id => {
                        let player = props.state.players.get(id)!;
                        return player.name;
                    }).join(", ")}
                </td>
                <td>
                    <button onClick={() => props.dispatch.cancelTradeOffer(trade.id)}>Cancel</button>
                </td>
            </tr>)}
            </tbody>
        </table>
    </Modal>;
}

