import React from "react";
import {Modal} from "../modals/Modal";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {VisualizeResources} from "./VisualizeResources";
import {CommandsDispatch} from "../command/CommandsDispatch";
import {PlayerSchema} from "../../../server/src/rooms/schema/PlayerSchema";
import {Resource, ResourcesSchema} from "../../../server/src/rooms/schema/ResourcesSchema";

interface Props {
    playerId: string;
    state: SettlersSchema;
    dispatch: CommandsDispatch;
}

export function TradeReceivedModal(props: Props) {
    let trades = Array.from(props.state.openTrades.values());
    let playerOfferingTradeId = props.state.playerOrder[props.state.currentPlayerIndex];
    let playerOfferingTrade = props.state.players.get(playerOfferingTradeId)!;
    let player = props.state.players.get(props.playerId)!;

    return <Modal close={() => {
    }}>
        <h1>Trade Offered</h1>
        <table style={{width: "100%"}}>
            <thead>
            <tr>
                <th>From</th>
                <th>Give</th>
                <th>Receive</th>
                <th>Status</th>
                <th/>
            </tr>
            </thead>
            <tbody>
            {trades.map(trade => <tr key={trade.id}>
                <td>{playerOfferingTrade.name}</td>
                <td><VisualizeResources resources={trade.receive}/></td>
                <td><VisualizeResources resources={trade.give}/></td>
                <td>
                    {trade.accepted.has(props.playerId) && "✔️"}
                    {trade.rejected.has(props.playerId) && "❌️"}
                </td>
                <td>
                    {checkPlayerHasNecessaryResources(player, trade.receive) &&
                    <button onClick={() => props.dispatch.acceptTrade(trade.id)}>Accept</button>}
                    <button onClick={() => props.dispatch.declineTrade(trade.id)}>Decline</button>
                </td>
            </tr>)}
            </tbody>
        </table>
    </Modal>
}

function checkPlayerHasNecessaryResources(player: PlayerSchema, tradeResources: ResourcesSchema): boolean {
    if (!tradeResources) {
        return false;
    }
    return !Object.entries(tradeResources).some(([key, value]) => {
        let availableResource = player.resources[key as Resource]!;
        return availableResource < value;
    });
}