import React from "react";
import {Resource} from "../../../server/src/rooms/schema/ResourcesSchema";

interface Props {
    resource: Resource;
    resourceAmount: number;
    onClick: () => void;
    hideAmount?: boolean;
    disabled?: boolean;
}

export function TradeResourceButton({resource, resourceAmount, onClick, hideAmount, disabled}: Props) {
    if (resourceAmount > 0) {
        return <button onClick={onClick} disabled={disabled}>
            {!!hideAmount ? null : <>{resourceAmount}x </>}{resource}</button>
    } else {
        return <></>
    }
}