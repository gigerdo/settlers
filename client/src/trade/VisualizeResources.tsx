import {TradeResources} from "../../../server/src/rooms/schema/TradeSchema";
import React from "react";

interface Props {
    resources: TradeResources;
}

export function VisualizeResources(props: Props) {
    return <>{mapper(props.resources)}</>
}

function mapper(obj: any) {
    return Object.entries(obj)
        .map(([k, v]) => {
            if (v === 0) {
                return undefined;
            }
            return `${v} ${k}`;
        })
        .filter(s => s !== undefined)
        .join((", "));
}