import React from "react";
import {Tooltip} from "react-tooltip";

export function ActionButton(props: React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>) {

    return <span>
        <button
            className="action-button"
            {...props}
            title={undefined}
            data-tooltip-id={props.title}
        >
            {props.children}
        </button>
        {props.title && <Tooltip
            id={props.title}
            // className="tooltip"
            // variant={"info"}
            delayHide={0}
            place={window.innerWidth > 1400 ? "right" : "bottom"}
        >
            {props.title}
        </Tooltip>}
    </span>;
}