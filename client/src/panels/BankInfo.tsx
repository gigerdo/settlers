import React from "react";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {VanillaOnly} from "../addons/VanillaOnly";
import {CitiesAndKnights} from "../addons/CitiesAndKnights";

interface Props {
    state: SettlersSchema;
}

export function BankInfo(props: Props) {
    let resources = props.state.bank;
    let totalRolls = props.state.rollStatistics.reduce((sum, next) => sum + next, 0);

    const formatStats = (i: number) => {
        return `${i}: ${props.state.rollStatistics[i - 1]}/${totalRolls} (${(props.state.rollStatistics[i - 1] / totalRolls * 100).toFixed(0)}%)`;
    };

    const formatStatsTwoDice = (i: number) => {
        return `${i}: ${props.state.rollStatisticsTwoDice[i - 2]}/${totalRolls / 2} (${(props.state.rollStatisticsTwoDice[i - 2] / (totalRolls / 2) * 100).toFixed(0)}%)`;
    };


    return <div
        title={`Dice Stats:
${formatStats(1)}
${formatStats(2)}
${formatStats(3)}
${formatStats(4)}
${formatStats(5)}
${formatStats(6)}

Two Dice:
${formatStatsTwoDice(2)}
${formatStatsTwoDice(3)}
${formatStatsTwoDice(4)}
${formatStatsTwoDice(5)}
${formatStatsTwoDice(6)}
${formatStatsTwoDice(7)}
${formatStatsTwoDice(8)}
${formatStatsTwoDice(9)}
${formatStatsTwoDice(10)}
${formatStatsTwoDice(11)}
${formatStatsTwoDice(12)}
`}
    >
        <div>🌳 Lumber: {resources.lumber}</div>
        <div>🧱 Brick: {resources.brick}</div>
        <div>🐑 Wool: {resources.wool}</div>
        <div>🌾 Grain: {resources.grain}</div>
        <div>⛰️ Ore: {resources.ore}</div>
        <CitiesAndKnights>
            <div>📜 Paper: {resources.paper}</div>
            <div>🥻 Cloth: {resources.cloth}</div>
            <div>💰 Coin: {resources.coin}</div>
            <div style={{borderLeft: "5px solid yellow", paddingLeft: "5px"}}>
                Trade Cards: {resources.tradeCardsTotal}
            </div>
            <div style={{borderLeft: "5px solid lightblue", paddingLeft: "5px"}}>
                Politics Cards: {resources.politicsCardsTotal}
            </div>
            <div style={{borderLeft: "5px solid limegreen", paddingLeft: "5px"}}>
                Science Cards: {resources.scienceCardsTotal}
            </div>
        </CitiesAndKnights>
        <VanillaOnly>
            <div>⚡ Development Cards: {resources.developmentCardsTotal}</div>
        </VanillaOnly>
    </div>;
}