import React from "react";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {CommandsDispatch} from "../command/CommandsDispatch";
import {VanillaOnly} from "../addons/VanillaOnly";
import {CitiesAndKnights} from "../addons/CitiesAndKnights";

interface Props {
    playerId: string;
    state: SettlersSchema;
    dispatch: CommandsDispatch;
}

export function ResourceInfo(props: Props) {
    let player = props.state.players.get(props.playerId)!;
    let resources = player.resources;

    if (!resources) return <></>;

    return <div
        style={{
            display: "grid",
            gridTemplateColumns: props.state.settings.addonCitiesAndKnights ? "repeat(3, auto)" : "repeat(2, auto)",
        }}
    >
        <fieldset style={{}}>
            <legend><b>Resources</b></legend>
            <div>🌳 Lumber: {resources.lumber}</div>
            <div>🧱 Brick: {resources.brick}</div>
            <div>🐑 Wool: {resources.wool}</div>
            <div>🌾 Grain: {resources.grain}</div>
            <div>⛰️ Ore: {resources.ore}</div>
        </fieldset>
        <CitiesAndKnights>
            <fieldset>
                <legend><b>Commodities</b></legend>
                <div style={{display: "grid", gridTemplateColumns: "auto"}}>
                    <div>📜 Paper: {resources.paper}</div>
                    <div>🥻 Cloth: {resources.cloth}</div>
                    <div>💰 Coin: {resources.coin}</div>
                </div>
            </fieldset>
        </CitiesAndKnights>
        <fieldset>
            <legend><b>Tokens</b></legend>
            <div>
                <div>🛣️ Roads: {player.roads}</div>
                <div>🏠 Settlements: {player.settlements}</div>
                <div>🏘️ Cities: {player.cities}</div>
                <VanillaOnly>
                    <div style={{height: "10px"}}/>
                    <div>⚔️ Knights: {player.knights} {player.largestArmy ? "[Largest Army]" : null}</div>
                </VanillaOnly>
                <CitiesAndKnights>
                    <div>🏰 Walls: {player.walls}</div>
                    <div>🗡️ Basic Knights: {player.basicKnights}</div>
                    <div>⚔️️ Strong Knights: {player.strongKnights}</div>
                    <div>🔱 Mighty Knights: {player.mightyKnights}</div>
                </CitiesAndKnights>
            </div>
        </fieldset>
    </div>;
}

