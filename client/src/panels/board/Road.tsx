import React from "react";
import {EdgeLoc} from "../../../../server/src/rooms/schema/EdgeSchema";
import {SettlersSchema} from "../../../../server/src/rooms/schema/SettlersSchema";
import {TileSchema} from "../../../../server/src/rooms/schema/TileSchema";
import {animated, useSpring} from "@react-spring/web";

interface Props {
    tile: TileSchema;
    loc: EdgeLoc;
    state: SettlersSchema;
}

export function Road(props: Props) {
    let player = props.state.players.get(props.tile[props.loc].ownerPlayerId)!;
    let color = player.color;
    let path = player.longestRoadPath;

    let onLongestPath = player.longestRoad && path.some(edge => isSameEdge(edge, {
        x: props.tile.x,
        y: props.tile.y,
        loc: props.loc
    }));

    let rotation: number;
    switch (props.loc) {
        case "north":
            rotation = 30;
            break;
        case "east":
            rotation = 90;
            break;
        case "south":
            rotation = 150;
            break;
    }

    const springs = useSpring({
        from: {y: -100},
        to: {y: 0},
        config: {
            duration: 500,
        },
    });

    return <span>
         <animated.svg
                xmlns="http://www.w3.org/2000/svg"
                style={{
                    fill: color,
                    position: "absolute", left: "-20px", top: "-20px",
                    stroke: onLongestPath ? "darkmagenta" : "black",
                    ...springs,
                }}
                width="40" height="40" viewBox="-25 -25 50 50"
            >
                <g transform={`rotate(${rotation} 0 0)`}>
                    <rect x="-25" y="-4" width='50' height='8'/>
                </g>
            </animated.svg>
    </span>;
}

function isSameEdge(e1: any, e2: any) {
    return e1.x === e2.x && e1.y === e2.y && e1.loc === e2.loc
}
