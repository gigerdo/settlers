import React from "react";
import {EdgeLoc} from "../../../../server/src/rooms/schema/EdgeSchema";
import {SettlersSchema} from "../../../../server/src/rooms/schema/SettlersSchema";
import {TileSchema} from "../../../../server/src/rooms/schema/TileSchema";

interface Props {
    tile: TileSchema;
    loc: EdgeLoc;
    state: SettlersSchema;
}

export function Harbor(props: Props) {
    let edge = props.tile[props.loc];

    let rotation;
    if (props.tile.terrainType === "water") {
        switch (props.loc) {
            case "north":
                rotation = 30;
                break;
            case "east":
                rotation = 90;
                break;
            case "south":
                rotation = 150;
                break;
        }
    } else {
        switch (props.loc) {
            case "north":
                rotation = 210;
                break;
            case "east":
                rotation = -90;
                break;
            case "south":
                rotation = 330;
                break;
        }
    }

    let text = "3 : 1";
    let text2 = "";
    if (edge.harbor !== "three-to-one") {
        text = "2 : 1";
        text2 = edge.harbor!;
    }

    return <svg
        xmlns="http://www.w3.org/2000/svg"
        style={{
            fill: "black",
            fontWeight: "bold",
            position: "absolute", left: "-100px", top: "-100px",
            pointerEvents: "none",
        }}
        width="200" height="200" viewBox="-100 -100 200 200"
    >
        <g transform={`rotate(${rotation} 0 0)`}>
            <line x1="-37" y1="7" x2="0" y2="30" style={{strokeWidth: "2px", stroke: "black"}}/>
            <line x1="37" y1="7" x2="0" y2="30" style={{strokeWidth: "2px", stroke: "black"}}/>
            <text x="0" y="45" textAnchor="middle">
                <tspan x="0">{text}</tspan>
                <tspan x="0" dy="1.0em">{text2}</tspan>
            </text>
        </g>
    </svg>;
}
