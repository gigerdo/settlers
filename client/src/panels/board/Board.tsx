import React, {ReactNode, useEffect, useMemo, useState} from "react";
import {SettlersSchema} from "../../../../server/src/rooms/schema/SettlersSchema";
import "./Board.css";
import {Circle} from "./Circle";
import {CommandsDispatch} from "../../command/CommandsDispatch";
import {Robber} from "./Robber";
import {StealModal} from "../../modals/StealModal";
import {EdgeLoc} from "../../../../server/src/rooms/schema/EdgeSchema";
import {Edge} from "./Edge";
import {VertexLoc} from "../../../../server/src/rooms/schema/VertexSchema";
import {Vertex} from "./Vertex";
import {CitiesAndKnights} from "../../addons/CitiesAndKnights";
import {ShowCitiesAndKnightsDice} from "./ShowCitiesAndKnightsDice";
import {getAllVerticesOfTile, getEdgesOfVertex, getVerticesOfEdge} from "../../utils/tileUtils";
import {KnightSchema} from "../../../../server/src/rooms/schema/KnightSchema";
import {BarbarianTile} from "./BarbarianTile";
import {Merchant} from "./Merchant";

interface Props {
    playerId: string;
    state: SettlersSchema;
    dispatch: CommandsDispatch;
    showSpots?: string;
    setShowSpots: (v?: string) => void;
}

export function Board(props: Props) {
    let [showStealModal, setShowStealModal] = useState(-1);

    let playersIndex = props.state.playerOrder.indexOf(props.playerId);
    let playersTurn = props.state.currentPlayerIndex === playersIndex;

    const [audio] = useState(new Audio("/assets/dice-1.wav"));
    useEffect(() => {
        if (props.state.rolledNumber > 0) {
            audio.play();
        }
    }, [props.state.rolledNumber, audio]);

    const [knightToMove, setKnightToMove] = useState<[number, VertexLoc] | undefined>(undefined);
    useEffect(() => {
        if (props.showSpots !== undefined) {
            setKnightToMove(undefined);
        }
    }, [props.showSpots, setKnightToMove]);
    let knightMoveableVertices = useMemo(() => {
        if (props.state.knightBeingDisplaced !== undefined && props.state.knightBeingDisplaced.ownerPlayerId === props.playerId) {
            return calculateVerticesTheKnightCanMoveTo(
                props.state.knightBeingDisplaced,
                props.state.knightBeingDisplacedIndex!,
                props.state.knightBeingDisplacedLoc!,
                props,
                false);
        }
        if (knightToMove === undefined) {
            return [];
        }

        let index = knightToMove[0];
        let loc = knightToMove[1];
        let vertex = props.state.board.tiles[index][loc];
        let knight = vertex.knight;
        if (knight === undefined) {
            return [];
        }

        return calculateVerticesTheKnightCanMoveTo(knight!, index, loc, props, true);
    }, [knightToMove, props.state.board, props.state.knightBeingDisplaced]);

    return (
        <div style={{overflow: "hidden", position: "relative"}}>
            <div
                style={{
                    position: "absolute",
                    fontSize: "20pt",
                    fontWeight: "bold",
                }}
            >
                {props.state.rolledNumber > 0 &&
                    <>
                        <div>Rolled: {props.state.rolledNumber}</div>
                        <CitiesAndKnights>
                            <div>
                                <span
                                    style={{background: "red", color: "white", padding: "0px 8px 0px 8px"}}
                                >{props.state.redDie}</span>
                                <span> </span>
                                <span
                                    style={{background: "yellow", padding: "0px 8px 0px 8px"}}
                                >{props.state.yellowDie}</span>
                            </div>
                        </CitiesAndKnights>
                        {props.state.rolledNumber !== 7 &&
                            <CitiesAndKnights>
                                <div
                                    style={{
                                        display: "grid",
                                        gridTemplateColumns: "auto auto",
                                        gap: "5px",
                                        alignItems: "center",
                                        justifyContent: "start",
                                    }}
                                >
                                    <ShowCitiesAndKnightsDice/>
                                </div>
                            </CitiesAndKnights>}
                    </>}
            </div>
            {playersTurn && props.state.phase === "roll-dice" &&
                <ShowWarning>Your Turn!</ShowWarning>}
            {playersTurn && (props.state.phase === "place-robber" || props.state.phase === "move-robber-bishop") &&
                <ShowWarning>Place the Robber</ShowWarning>}
            {playersTurn && props.state.phase === "placing-metropolis" &&
                <ShowWarning>Place your metropolis</ShowWarning>}
            {playersTurn && props.state.phase === "placing-settlement" &&
                <ShowWarning>Your turn to place!</ShowWarning>}
            {playersTurn && props.state.phase === "placing-road" &&
                <ShowWarning>Your turn to place!</ShowWarning>}
            {props.state.phase === "destroying-city" &&
                <ShowWarning>Barbarian attack!</ShowWarning>}
            {props.state.phase === "destroying-city" && props.state.playersDestroyingCity.find(p => p === props.playerId) !== undefined &&
                <ShowWarning>Select city to destroy:</ShowWarning>}
            {props.state.phase === "removing-knight" && props.state.deserterPlayerId === props.playerId &&
                <ShowWarning>Deserter! Select a knight to remove!</ShowWarning>}
            {props.state.phase === "placing-knight" && playersTurn &&
                <ShowWarning>Deserter! Place a knight! <button onClick={() => props.dispatch.skip()}>Skip</button>
                </ShowWarning>}
            {props.state.phase === "displacing-knight" && props.state.knightBeingDisplaced?.ownerPlayerId === props.playerId &&
                <ShowWarning>Intrigue! You must move your knight!</ShowWarning>}
            {(props.state.phase === "road-building-1" || props.state.phase === "road-building-2") && playersTurn &&
                <ShowWarning>
                    Place road!
                    <CitiesAndKnights>
                        <button onClick={() => props.dispatch.skip()}>Skip</button>
                    </CitiesAndKnights>
                </ShowWarning>}

            <svg viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg" height="0" width="0">
                {/*<defs>*/}
                {/*    <pattern id="water" x="0" y="0" width="96" height="96" patternUnits="userspaceOnUse">*/}
                {/*        <image width="96" height="96" href="/assets/sea.png"/>*/}
                {/*    </pattern>*/}
                {/*    <pattern id="lumber" x="0" y="0" width="96" height="96" patternUnits="userspaceOnUse">*/}
                {/*        <image width="96" height="96" href="/assets/woods.png"/>*/}
                {/*    </pattern>*/}
                {/*    <pattern id="brick" x="0" y="0" width="96" height="96" patternUnits="userspaceOnUse">*/}
                {/*        <image width="96" height="96" href="/assets/clay.png"/>*/}
                {/*    </pattern>*/}
                {/*    <pattern id="wool" x="0" y="0" width="96" height="96" patternUnits="userspaceOnUse">*/}
                {/*        <image width="96" height="96" href="/assets/grass.png"/>*/}
                {/*    </pattern>*/}
                {/*    <pattern id="grain" x="0" y="0" width="96" height="96" patternUnits="userspaceOnUse">*/}
                {/*        <image width="96" height="96" href="/assets/grains.png"/>*/}
                {/*    </pattern>*/}
                {/*    <pattern id="ore" x="0" y="0" width="96" height="96" patternUnits="userspaceOnUse">*/}
                {/*        <image width="96" height="96" href="/assets/rock.png"/>*/}
                {/*    </pattern>*/}
                {/*    <pattern id="desert" x="0" y="0" width="96" height="96" patternUnits="userspaceOnUse">*/}
                {/*        <image width="96" height="96" href="/assets/desert.png"/>*/}
                {/*    </pattern>*/}
                {/*</defs>*/}
            </svg>
            <div className="hexagon-container">
                {props.state.board.tiles.map((t, index) => {
                    let robber = props.state.board.robber;
                    let hasRobber = robber.x === t.x && robber.y === t.y;
                    return <div
                        className={`hex ${t.terrainType} ${hiddenTiles.includes(index) ? "hidden" : ""}`}
                        // title={`${t.x}-${t.y}`}
                        key={index}
                    >
                        {["top", "bottom"]
                            .map(loc => <Vertex
                                    key={loc}
                                    state={props.state}
                                    dispatch={props.dispatch}
                                    playerId={props.playerId}
                                    showSpots={props.showSpots}
                                    setShowSpots={props.setShowSpots}
                                    tile={t}
                                    loc={loc as VertexLoc}
                                    showKnightSelection={knightMoveableVertices.find(v => v.x === t.x && v.y === t.y && v.loc === loc) !== undefined}
                                    setKnightToMove={setKnightToMove}
                                    knightBeingMoved={knightToMove}
                                />,
                            )}
                        {["north", "east", "south"]
                            .map(loc => <Edge
                                    key={loc}
                                    state={props.state}
                                    dispatch={props.dispatch}
                                    showSpots={props.showSpots}
                                    setShowSpots={props.setShowSpots}
                                    playersTurn={playersTurn}
                                    tile={t}
                                    loc={loc as EdgeLoc}
                                />,
                            )}
                        <svg
                            version="1.1" xmlns="http://www.w3.org/2000/svg"
                            className="tile"
                            viewBox="-5 -5 110 110"
                        >
                            <polygon
                                points="50,0 92.5,25 92.5,75 50,100 7.5,75 7.5,25"
                                // fill={`url(#${t.terrainType})`}
                                fill={colorMapping[t.terrainType]}
                            />
                        </svg>

                        <div style={{display: "grid", alignItems: "center", justifyItems: "center"}}>
                            {/* Robber */}
                            <Robber visible={hasRobber}/>
                            {playersTurn && props.state.phase === "place-robber" && t.terrainType !== "water"
                                && (robber.x !== t.x || robber.y !== t.y) &&
                                <div style={{position: "relative", top: "-5px"}}>
                                    <Circle onClick={() => setShowStealModal(index)}/>
                                </div>}
                            {playersTurn && props.state.phase === "move-robber-bishop" && t.terrainType !== "water"
                                && (robber.x !== t.x || robber.y !== t.y) &&
                                <div style={{position: "relative", top: "-5px"}}>
                                    <Circle onClick={() => props.dispatch.placeRobber(t.x, t.y, "")}/>
                                </div>}

                            {/* Merchant */}
                            <Merchant visible={props.state.board.merchantIndex === index}/>
                            {playersTurn && t.terrainType !== "water"
                                && t.terrainType !== "desert"
                                && props.showSpots === "merchant"
                                && getAllVerticesOfTile(props.state.board, t.x, t.y).some(v => v.ownerPlayerId === props.playerId)
                                && <div style={{position: "relative", top: "-5px"}}>
                                    <Circle
                                        onClick={() => {
                                            props.dispatch.playProgressCardLoc("merchant", index, undefined);
                                            props.setShowSpots(undefined);
                                        }}
                                    />
                                </div>}

                            {/* Resources */}
                            {t.terrainType !== "water" &&
                                <div style={{padding: "5px"}}>
                                    <div>{nameMapping[t.terrainType]} {t.terrainType}</div>
                                    <CitiesAndKnights>
                                        {t.terrainType === "lumber" &&
                                            <div>📜 paper</div>}
                                        {t.terrainType === "wool" &&
                                            <div>🥻 cloth</div>}
                                        {t.terrainType === "ore" &&
                                            <div>💰 coin</div>}
                                    </CitiesAndKnights>
                                </div>}
                            {t.number >= 0 &&
                                <div
                                    style={{
                                        display: "flex",
                                        alignItems: "center",
                                        justifyContent: "center",
                                        width: "30px",
                                        height: "30px",
                                        background: props.state.rolledNumber === t.number && !hasRobber ? "red" : "linen",
                                        border: "1px solid black",
                                        borderRadius: "50%",
                                    }}
                                >
                                <span
                                    style={{
                                        fontWeight: "bold",
                                        ...numberStyle(t.number),
                                        color: props.state.rolledNumber === t.number && !hasRobber ? "white" : undefined,
                                    }}
                                >
                                    {t.number}
                                </span>
                                </div>}
                        </div>
                    </div>;
                })}
            </div>
            <CitiesAndKnights>
                <div style={{position: "absolute", top: "0px", right: "0px"}}>
                    <BarbarianTile state={props.state}/>
                </div>
            </CitiesAndKnights>
            {showStealModal >= 0 &&
                <StealModal
                    tileIndex={showStealModal}
                    playerId={props.playerId}
                    close={() => setShowStealModal(-1)}
                    state={props.state}
                    dispatch={props.dispatch}
                />}
        </div>
    );
}

function ShowWarning({children}: { children: ReactNode }) {
    return <div
        style={{
            position: "absolute",
            fontSize: "30pt", border: "2px solid red", width: "220px",
            background: "red",
            color: "white",
            fontWeight: "bold",
            textAlign: "center",
            zIndex: 90,
        }}
    >
        {children}
    </div>;
}

function numberStyle(n: number): React.CSSProperties {
    switch (n) {
        case 2:
        case 12:
            return {
                fontSize: 13,
            };
        case 3:
        case 11:
            return {
                fontSize: 15,
            };
        case 4:
        case 10:
            return {
                fontSize: 17,
            };
        case 5:
        case 9:
            return {
                fontSize: 19,
            };
        case 6:
        case 8:
            return {
                position: "relative",
                top: "-1px",
                fontSize: 21,
            };
        default:
            return {
                fontSize: 16,
            };
    }
}

const colorMapping = {
    water: "#59b7ea",
    lumber: "forestgreen",
    brick: "sandybrown",
    wool: "lightgreen",
    grain: "gold",
    ore: "grey",
    desert: "goldenrod",
};

const nameMapping = {
    lumber: "🌳",
    brick: "🧱",
    wool: "🐑",
    grain: "🌾",
    ore: "⛰️",
    desert: "🌵",
};

const hiddenTiles = [
    0, 1, 6, 7, 13, 14, 28, 35, 41, 42, 43, 48,
];

function calculateVerticesTheKnightCanMoveTo(knight: KnightSchema, index: number, loc: VertexLoc, props: Props, allowDisplace: Boolean) {
    let vertex = props.state.board.tiles[index][loc];

    let remainingVertices = [vertex];
    let visited = new Set();
    let selectableVertices = [];
    while (remainingVertices.length > 0) {
        let v = remainingVertices.pop()!;
        let id = `${v.x}-${v.y}-${v.loc}`;
        if (visited.has(id)) {
            continue;
        }
        visited.add(id);
        let edges = getEdgesOfVertex(props.state.board, v.x, v.y, v.loc)
            .filter(e => e.ownerPlayerId === knight!.ownerPlayerId);
        for (let edge of edges) {
            let edgeVertices = getVerticesOfEdge(props.state.board, edge.x, edge.y, edge.loc)
                .filter(v => v.ownerPlayerId === "" || v.ownerPlayerId === knight!.ownerPlayerId);
            for (let edgeVertex of edgeVertices) {
                if (edgeVertex.x === vertex.x && edgeVertex.y === vertex.y && edgeVertex.loc === vertex.loc) {
                    continue;
                }
                if (edgeVertex.knight === undefined || edgeVertex.knight.ownerPlayerId === knight!.ownerPlayerId) {
                    remainingVertices.push(edgeVertex);
                    if (edgeVertex.ownerPlayerId === "" && edgeVertex.knight === undefined) {
                        selectableVertices.push(edgeVertex);
                    }
                }
                if (allowDisplace
                    && edgeVertex.knight !== undefined
                    && edgeVertex.knight.ownerPlayerId !== knight!.ownerPlayerId
                    && isLowerLevel(edgeVertex.knight, knight!)
                ) {
                    selectableVertices.push(edgeVertex);
                }
            }
        }
    }
    return selectableVertices;
}

function isLowerLevel(a: KnightSchema, b: KnightSchema): boolean {
    if (a.level === "basic" && (b.level === "strong" || b.level === "mighty")) {
        return true;
    }
    if (a.level === "strong" && b.level === "mighty") {
        return true;
    }
    return false;
}