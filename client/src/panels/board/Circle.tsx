import React from "react";

interface Props {
    onClick: () => void;
}

export function Circle(props: Props) {
    return <div
        style={{
            cursor: "pointer",
            background: "black",
            width: "40px",
            height: "40px",
            borderRadius: "50%",
            opacity: "50%",
            position: "absolute",
            zIndex: 3,
            animationDirection: "alternate",
            animationName: "circle",
            animationIterationCount: "infinite",
            animationDuration: "0.75s",
            animationTimingFunction: "linear",
        }}
        onClick={props.onClick}
    />
}