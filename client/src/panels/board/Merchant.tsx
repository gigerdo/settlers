import React, {useContext} from "react";
import {animated, useTransition} from "@react-spring/web";
import {RoomStateContext} from "../../hooks/RoomStateContext";
import {Tooltip} from "react-tooltip";

interface Props {
    visible: boolean;
}

export function Merchant(props: Props) {
    const {roomState} = useContext(RoomStateContext);
    const transitions = useTransition([props.visible], {
        from: {transform: "translate3d(0px, -150px, 0)", opacity: 1},
        enter: {transform: "translate3d(0px, 0px, 0)", opacity: 1},
        leave: {transform: "translate3d(0px, -150px, 0)", opacity: 0},
        config: {duration: 500},
    });

    if (roomState.board.merchantOwner === undefined) {
        return null;
    }

    const merchantPlayer = roomState.board.merchantOwner;
    const color = roomState.players.get(merchantPlayer!)!.color;

    return transitions((style, visible) => visible && <>
        <Tooltip
                id={"merchant"}
                delayShow={400}
                place="bottom"
            className={"tooltip"}
            />
        <animated.svg
                version="1.1" id="Layer_1"
                xmlns="http://www.w3.org/2000/svg"
                width="50" height="50"
                viewBox="0 0 511.243 550.243"
                style={{
                    fill: color,
                    position: "absolute", right: "15px", top: "25px",
                    ...style,
                }}
            data-tooltip-id={"merchant"}
            data-tooltip-content={"Merchant: Allows the owner to trade the resource on this hex 2:1."}
            >
                <g
                    stroke="black"
                    strokeWidth="20px"
                >
                    <path
                        d="M411.867,425.525L265.669,6.432c-3.008-8.576-17.152-8.576-20.139,0L99.333,425.525
                                c31.467-13.888,83.627-20.928,156.267-20.928C328.24,404.597,380.4,411.616,411.867,425.525z"
                    />
                    <path
                        d="M426.267,468.597c0-0.405-0.235-0.747-0.277-1.152c-0.064-0.789-0.043-1.579-0.32-2.368l-0.725-2.069
                                c-0.555-1.515-1.408-2.944-2.389-4.331c-0.384-0.512-0.747-1.024-1.173-1.536c-2.987-3.52-7.211-6.656-12.565-9.429
                                c-1.493-0.789-3.115-1.557-4.779-2.325c-0.939-0.405-1.813-0.853-2.795-1.237c-26.88-11.093-74.56-17.6-137.28-18.155
                                c-3.669-0.043-6.741-0.064-8.341-0.064c-1.6,0-4.672,0.021-8.341,0.064c-62.741,0.533-110.4,7.061-137.28,18.155
                                c-0.981,0.384-1.856,0.832-2.795,1.237c-1.685,0.747-3.285,1.515-4.779,2.325c-5.355,2.773-9.579,5.909-12.565,9.429
                                c-0.427,0.512-0.789,1.024-1.173,1.536c-0.981,1.387-1.835,2.816-2.389,4.331l-0.725,2.069c-0.277,0.789-0.256,1.579-0.32,2.368
                                c-0.021,0.405-0.277,0.747-0.277,1.152c0,0.107,0.064,0.192,0.064,0.299c0,0.021-0.021,0.064,0,0.085
                                c0,0.384,0.149,0.747,0.171,1.131c0.043,0.235,0.213,0.405,0.277,0.619c5.44,39.275,153.003,40.512,170.155,40.512
                                c17.152,0,164.736-1.259,170.155-40.512c0.043-0.235,0.235-0.405,0.277-0.619c0.043-0.384,0.171-0.747,0.171-1.131
                                c0-0.021-0.021-0.064,0-0.085C426.203,468.789,426.267,468.704,426.267,468.597z"
                    />
                </g>
        </animated.svg>
    </>);
}