import {SettlersSchema} from "../../../../server/src/rooms/schema/SettlersSchema";

interface Props {
    state: SettlersSchema;
}

export function BarbarianTile({state}: Props) {
    if (!state.settings.addonCitiesAndKnights) {
        return null;
    }
    let circleStyle = {fill: "lightblue", stroke: "lightblue"};
    let [x, y] = getBarbarianPosition(state);
    return <div>
        <svg viewBox="-20 -15 120 150" version="1.1" xmlns="http://www.w3.org/2000/svg" width="120">

            <circle cx="80" cy="15" r="15" style={circleStyle}/>
            <circle cx="40" cy="15" r="15" style={circleStyle}/>
            <circle cx="0" cy="15" r="15" style={circleStyle}/>
            <line x1="45" y1="15" x2="65" y2="15" style={circleStyle} strokeWidth={"10"}/>
            <line x1="0" y1="15" x2="65" y2="15" style={circleStyle} strokeWidth={"10"}/>

            <line x1="0" y1="15" x2="0" y2="55" style={circleStyle} strokeWidth={"10"}/>
            <circle cx="0" cy="55" r="15" style={circleStyle}/>
            <circle cx="40" cy="55" r="15" style={circleStyle}/>
            <circle cx="80" cy="55" r="15" style={circleStyle}/>
            <line x1="0" y1="55" x2="80" y2="55" style={circleStyle} strokeWidth={"10"}/>

            <line x1="80" y1="55" x2="80" y2="95" style={circleStyle} strokeWidth={"10"}/>
            <circle cx="80" cy="95" r="15" style={circleStyle}/>

            <line x1="30" y1="110" x2="80" y2="95" style={circleStyle} strokeWidth={"10"}/>
            <circle cx="30" cy="110" r="20" style={{fill: "sandybrown"}}/>

            <text x={x} y={y} fontSize={"18pt"}>⛵</text>
            <text x="11" y="120" fontSize={"21pt"}>☠️</text>

            <text
                x="40" y="-5" fontSize={"10pt"} textAnchor="middle" style={{fontWeight: "bold"}}
            >Barbarians: {state.barbarianPosition}</text>
        </svg>
    </div>;
}

function getBarbarianPosition(state: SettlersSchema): number[] {
    switch (state.barbarianPosition) {
        case 0:
            return [65, 22];
        case 1:
            return [24, 22];
        case 2:
            return [-17, 22];
        case 3:
            return [-17, 63];
        case 4:
            return [22, 63];
        case 5:
            return [65, 63];
        case 6:
            return [65, 103];
        case 7:
            return [11, 120];
    }

    return [0, 0];
}