import React from "react";
import {animated, useSpring} from "@react-spring/web";
import {CityUpgradeColor} from "../../../../server/src/rooms/schema/CityUpgradeSchema";
import {CITY_PROPERTY_MAP} from "../CityUpgrades";

interface Props {
    color: string;
    wall: boolean;
    metropolis?: CityUpgradeColor;
    showTransition?: boolean;
}

export function City(props: Props) {
    const start = {x: 0, y: -100};
    const end = {x: 5, y: 2};
    const springs = useSpring({
        from: props.showTransition ?? true ? start : end,
        to: end,
        config: {
            duration: props.showTransition ?? true ? 500 : 0,
        },
    });
    return <animated.svg
            xmlns="http://www.w3.org/2000/svg" width="45" height="45" viewBox="-5 -5 60 60"
            style={{
                fill: props.color,
                stroke: "black",
                ...springs,
            }}
        >
            <polygon points="0,35 20,35 20,12 10,0 0,12"/>
            <polygon points="15,35 39,35 39,22 27,10 15,22"/>
            {props.metropolis &&
                <polygon
                    style={{fill: CITY_PROPERTY_MAP[props.metropolis].backgroundColor}}
                    points="0,35 0,10 5,5 10,10 10,20 35,20 35,5 40,1 45,5 45,35 35,35 35,30 10,30 10,35 "
                />}
            {props.wall &&
                <polygon
                    points="-5,30 0,30 0,33 5,33 5,30 10,30 10,33 15,33 15,30 20,30 20,33 25,33 25,30  30,30 30,33 35,33 35,30  40,30 40,33 45,33 45,30  50,30 50,45, -5,45"/>}
    </animated.svg>;
}