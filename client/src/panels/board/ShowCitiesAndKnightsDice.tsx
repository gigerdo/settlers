import {City} from "./City";
import React, {useContext} from "react";
import {RoomStateContext} from "../../hooks/RoomStateContext";
import {CITY_PROPERTY_MAP} from "../CityUpgrades";

export function ShowCitiesAndKnightsDice() {
    const {roomState: state} = useContext(RoomStateContext);

    if (!state.eventDie) {
        return <></>
    }
    switch (state.eventDie) {
        case "barbarian":
            return <>🏴‍☠️ Barbarians! 🏴‍☠️️</>;
        case "blue":
            return <><City
                color={CITY_PROPERTY_MAP.blue.backgroundColor} wall={false} showTransition={false}
            /> {state.redDie}</>;
        case "green":
            return <><City
                color={CITY_PROPERTY_MAP.green.backgroundColor} wall={false} showTransition={false}
            /> {state.redDie}</>;
        case "yellow":
            return <><City
                color={CITY_PROPERTY_MAP.yellow.backgroundColor} wall={false} showTransition={false}
            /> {state.redDie}</>;
    }
}