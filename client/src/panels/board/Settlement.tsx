import React from "react";
import {animated, useTransition} from "@react-spring/web";

interface Props {
    color: string;
    onClick?: () => void;
    visible: boolean;
}

export function Settlement(props: Props) {
    const transitions = useTransition([props.visible], {
        from: {transform: "translate3d(0px, -100px, 0)", opacity: 1},
        enter: {transform: "translate3d(0px, 0px, 0)", opacity: 1},
        leave: {transform: "translate3d(-50px, -50px, 0)", opacity: 0},
        config: {duration: 500},
    });
    return (
        transitions((style, visible) =>
            visible && <animated.svg
                xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="-2 -2 28 28"
                style={{
                    fill: props.color,
                    cursor: props.onClick ? "pointer" : "normal",
                    position: "absolute",
                    left: "-15px",
                    top: "-19px",
                    stroke: "black",
                    ...style,
                }}
                onClick={props.onClick}
            >
                <polygon points="0,24 24,24 24,12 12,0 0,12"/>
            </animated.svg>)
    );
}