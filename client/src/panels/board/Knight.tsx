import React, {useState} from "react";
import {
    FloatingFocusManager,
    FloatingPortal,
    useClick,
    useDismiss,
    useFloating,
    useInteractions,
    useRole,
} from "@floating-ui/react";
import {PlayerSchema} from "../../../../server/src/rooms/schema/PlayerSchema";
import {KnightSchema} from "../../../../server/src/rooms/schema/KnightSchema";
import {ActionButton} from "../ActionButton";
import {CommandsDispatch} from "../../command/CommandsDispatch";
import {VertexLoc} from "../../../../server/src/rooms/schema/VertexSchema";
import {SettlersSchema} from "../../../../server/src/rooms/schema/SettlersSchema";
import {getTilesOfVertex} from "../../utils/tileUtils";


interface KnightProps {
    state: SettlersSchema;
    player: PlayerSchema;
    knight: KnightSchema;
    disabled: boolean;
    dispatch: CommandsDispatch;
    index: number;
    loc: VertexLoc;
    setKnightToMove: (k: [number, VertexLoc]) => void;
}

export function Knight({state, player, knight, disabled, dispatch, index, loc, setKnightToMove}: KnightProps) {
    const [menuOpen, setMenuOpen] = useState(false);
    const {
        refs,
        floatingStyles,
        context,
    } = useFloating({
        placement: "bottom",
        open: menuOpen,
        onOpenChange: setMenuOpen,
    });

    const click = useClick(context);
    const dismiss = useDismiss(context);
    const role = useRole(context);
    const {getReferenceProps, getFloatingProps} = useInteractions([
        click,
        dismiss,
        role,
    ]);

    const activated = knight.activated;

    return <div>
        <div>
            <button
                disabled={disabled}
                ref={refs.setReference}
                className={disabled ? "" : "knight-button"}
                style={{
                    fontSize: "14pt",
                    borderRadius: "50px",
                    padding: "3px",
                    backgroundColor: `light${player.color}`,
                    border: `4px solid ${activated ? player.color : "rgba(0, 0, 255, 0.3)"}`,
                    position: "absolute", left: "-20px", top: "-20px",
                }}
                {...getReferenceProps()}
            >
                <span
                    style={{
                        // color: "transparent",
                        // textShadow: `0 0 0 ${active ? player.color : `${player.color}` }`,
                        opacity: activated ? "1.0" : "0.4",
                    }}
                >
                    {knight.level === "basic" && <>🗡️</>}
                    {knight.level === "strong" && <>⚔️</>}
                    {knight.level === "mighty" && <>🔱</>}
                </span>

            </button>
        </div>
        <FloatingPortal>
            {menuOpen && !disabled &&
                <FloatingFocusManager context={context} modal={false}>
                    <div
                        ref={refs.setFloating}
                        style={{
                            zIndex: 999,
                            ...floatingStyles,
                        }}
                        {...getFloatingProps()}
                    >
                        <div
                            style={{
                                background: "seashell",
                                padding: "5px",
                                borderRadius: "5px",
                                border: "2px solid black",
                                display: "grid",
                                gap: "5px",
                            }}
                        >
                            {knight.level === "basic" && <b>Basic Knight</b>}
                            {knight.level === "strong" && <b>Strong Knight</b>}
                            {knight.level === "mighty" && <b>Might Knight</b>}
                            <ActionButton
                                disabled={activated || player.resources["grain"] < 1}
                                title="🌾"
                                onClick={() => {
                                    dispatch.activateKnight(index, loc);
                                }}
                            >
                                Activate
                            </ActionButton>
                            <ActionButton
                                disabled={!activated || knight.actionTaken}
                                onClick={() => {
                                    setKnightToMove([index, loc]);
                                    setMenuOpen(false);
                                }}
                            >
                                Move
                            </ActionButton>
                            {knight.level !== "mighty" && <ActionButton
                                disabled={knight.promoted || !player.canPromoteKnight}
                                title="🐑⛰️"
                                onClick={() => {
                                    dispatch.promoteKnight(index, loc);
                                }}
                            >
                                Promote
                            </ActionButton>}
                            {state.barbarianAttacks > 0 && <ActionButton
                                disabled={!knight.actionTaken && knight.activated && getTilesOfVertex(state.board, index, loc)
                                    .find(t => t.x === state.board.robber.x && t.y === state.board.robber.y) === undefined}
                                title="Robber in an adjacent hex can be chased away."
                                onClick={() => {
                                    dispatch.chaseAwayRobber(index, loc);
                                }}
                            >
                                Expel Robber
                            </ActionButton>}
                        </div>

                    </div>
                </FloatingFocusManager>
            }
        </FloatingPortal>
    </div>;
}