import {Circle} from "./Circle";
import {Road} from "./Road";
import React from "react";
import {EdgeLoc} from "../../../../server/src/rooms/schema/EdgeSchema";
import {TileSchema} from "../../../../server/src/rooms/schema/TileSchema";
import {SettlersSchema} from "../../../../server/src/rooms/schema/SettlersSchema";
import {CommandsDispatch} from "../../command/CommandsDispatch";
import {Harbor} from "./Harbor";
import {getEdgesOfVertex, getVerticesOfEdge} from "../../utils/tileUtils";
import {BoardSchema} from "../../../../server/src/rooms/schema/BoardSchema";

interface Props {
    state: SettlersSchema;
    dispatch: CommandsDispatch;
    showSpots?: string;
    playersTurn: boolean;
    setShowSpots: (v?: string) => void;
    tile: TileSchema;
    loc: EdgeLoc;
}

export function Edge(props: Props) {
    let edge = props.tile[props.loc];
    let ownerPlayerId = props.tile[props.loc].ownerPlayerId;

    let canPlaceRoad = (props.state.phase === "placing-road" || props.showSpots === "roads"
        || props.state.phase === "road-building-1" || props.state.phase === "road-building-2") && props.playersTurn
        && ownerPlayerId === "" && edge.canPlace;

    function buildRoad(index: number, loc: EdgeLoc) {
        if (props.state.phase === "placing-road") {
            props.dispatch.placeRoad(index, loc);
        } else {
            props.dispatch.buildRoad(index, loc);
            props.setShowSpots(undefined);
        }
    }


    let removableRoad = props.showSpots === "removable-roads" &&
        ownerPlayerId !== "" &&
        checkIfRoadCanBeRemoved(props.state.board, props.tile.x, props.tile.y, props.loc, ownerPlayerId);

    let index = props.tile.y * 7 + props.tile.x;
    return <div className={`edge ${props.loc}`} title={`${props.tile.x}-${props.tile.y}-${props.loc}`}>
        {canPlaceRoad &&
            <Circle onClick={() => buildRoad(index, props.loc)}/>}
        {removableRoad &&
            <Circle
                onClick={() => {
                    props.setShowSpots(undefined);
                    props.dispatch.playProgressCardLoc("diplomat", index, props.loc);
                }}
            />}
        {ownerPlayerId !== "" &&
        <Road tile={props.tile} loc={props.loc} state={props.state}/>}
        {edge.harbor &&
        <Harbor tile={props.tile} loc={props.loc} state={props.state}/>}
    </div>
}

function checkIfRoadCanBeRemoved(board: BoardSchema, x: number, y: number, loc: EdgeLoc, ownerPlayerId: string) {
    for (let vertexSchema of getVerticesOfEdge(board, x, y, loc)) {
        let noKnight = vertexSchema.knight === undefined || vertexSchema.knight.ownerPlayerId !== ownerPlayerId;
        let noSettlement = vertexSchema.ownerPlayerId !== ownerPlayerId;
        let noRoad = !getEdgesOfVertex(board, vertexSchema.x, vertexSchema.y, vertexSchema.loc)
            .filter(e => !(e.x === x && e.y === y && e.loc == loc))
            .some(e => e.ownerPlayerId === ownerPlayerId);
        if (noKnight && noSettlement && noRoad) {
            return true;
        }
    }
    return false;
}