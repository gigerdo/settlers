import {Circle} from "./Circle";
import {Settlement} from "./Settlement";
import {City} from "./City";
import React from "react";
import {TileSchema} from "../../../../server/src/rooms/schema/TileSchema";
import {SettlersSchema} from "../../../../server/src/rooms/schema/SettlersSchema";
import {CommandsDispatch} from "../../command/CommandsDispatch";
import {VertexLoc} from "../../../../server/src/rooms/schema/VertexSchema";
import {Knight} from "./Knight";
import {getEdgesOfVertex} from "../../utils/tileUtils";

interface Props {
    state: SettlersSchema;
    dispatch: CommandsDispatch;
    showSpots?: string;
    playerId: string;
    setShowSpots: (v?: string) => void;
    tile: TileSchema;
    loc: VertexLoc;
    showKnightSelection: boolean;
    setKnightToMove: (k?: [number, VertexLoc]) => void;
    knightBeingMoved: [number, VertexLoc] | undefined;
}

export function Vertex(props: Props) {
    let playersIndex = props.state.playerOrder.indexOf(props.playerId);
    let playersTurn = props.state.currentPlayerIndex === playersIndex;
    let currentPlayerId = props.state.playerOrder[props.state.currentPlayerIndex];
    let index = props.tile.y * 7 + props.tile.x;
    let vertex = props.tile[props.loc];
    let ownerPlayerId = vertex.ownerPlayerId;
    let city = vertex.city;

    let canPlaceSettlement = (props.state.phase === "placing-settlement" || props.showSpots === "settlements")
        && playersTurn && ownerPlayerId === "" && vertex.canPlace;

    function buildSettlement(index: number, loc: VertexLoc) {
        if (props.state.phase === "placing-settlement") {
            props.dispatch.placeSettlement(index, loc);
        } else {
            props.dispatch.buildSettlement(index, loc);
            props.setShowSpots(undefined);
        }
    }

    let canPlaceCity = (props.showSpots === "cities" || props.showSpots === "cities-medicine") && playersTurn && ownerPlayerId === props.playerId && !city;

    function buildCity(index: number, loc: VertexLoc, useMedicineCard: Boolean) {
        props.dispatch.buildCity(index, loc, useMedicineCard);
        props.setShowSpots(undefined);
    }

    let canPlaceWall = (props.showSpots === "walls" || props.showSpots === "walls-engineer") && playersTurn && ownerPlayerId === props.playerId && city && !vertex.wall;

    function buildWall(index: number, loc: VertexLoc, useEngineerCard: Boolean) {
        props.dispatch.buildWall(index, loc, useEngineerCard);
        props.setShowSpots(undefined);
    }

    let canPlaceMetropolis = props.state.phase === "placing-metropolis" && playersTurn && ownerPlayerId === props.playerId
        && city && vertex.metropolis === undefined;

    let canPlaceKnight = (props.showSpots === "knights" || (props.state.phase === "placing-knight" && playersTurn)) && vertex.canPlaceKnight;

    let canDestroyCity = props.state.phase === "destroying-city" && vertex.city && vertex.metropolis === undefined
        && props.playerId === vertex.ownerPlayerId;

    let player = props.state.players.get(ownerPlayerId);

    let edges = getEdgesOfVertex(props.state.board, props.tile.x, props.tile.y, props.loc);

    return <div className={`vertex ${props.loc}`} title={`${props.tile.x}-${props.tile.y}-${props.loc}`}>
        {canPlaceSettlement &&
            <Circle onClick={() => buildSettlement(index, props.loc)}/>}
        {canPlaceCity &&
            <Circle onClick={() => buildCity(index, props.loc, props.showSpots === "cities-medicine")}/>}
        {canPlaceWall &&
            <div style={{position: "absolute", left: "20px", top: "25px"}}>
                <Circle onClick={() => buildWall(index, props.loc, props.showSpots === "walls-engineer")}/>
            </div>}
        {canPlaceMetropolis &&
            <div style={{position: "absolute", left: "20px", top: "25px"}}>
                <Circle onClick={() => props.dispatch.buildMetropolis(index, props.loc)}/>
            </div>}
        {canDestroyCity &&
            <div style={{position: "absolute", left: "20px", top: "25px"}}>
                <Circle onClick={() => props.dispatch.destroyCity(index, props.loc)}/>
            </div>}
        {props.showKnightSelection &&
            <div style={{position: "absolute", left: "0px", top: "0px"}}>
                <Circle
                    onClick={() => {
                        if (props.knightBeingMoved !== undefined) {
                            props.dispatch.moveKnight(props.knightBeingMoved![0], props.knightBeingMoved![1], index, props.loc);
                        }
                        if (props.state.knightBeingDisplaced !== undefined) {
                            props.dispatch.moveKnight(props.state.knightBeingDisplacedIndex!, props.state.knightBeingDisplacedLoc!, index, props.loc);
                        }
                        props.setKnightToMove(undefined);
                    }}
                />
            </div>}
        {player &&
            <Settlement color={player.color} visible={player && !city}/>}
        {player && city &&
            <City color={player.color} wall={vertex.wall} metropolis={vertex.metropolis}/>}
        {canPlaceKnight &&
            <Circle
                onClick={() => {
                    props.setShowSpots(undefined);
                    props.dispatch.buyKnight(index, props.loc);
                }}
            />}
        {// Intrigue card
            vertex.knight &&
            props.showSpots === "opponent-knights" &&
            vertex.knight.ownerPlayerId !== currentPlayerId &&
            edges.some(e => e.ownerPlayerId === currentPlayerId) &&
            <Circle
                onClick={() => {
                    props.setShowSpots(undefined);
                    props.dispatch.playProgressCardLoc("intrigue", index, props.loc);
                }}
            />}
        {// Deserter card
            vertex.knight
            && props.state.phase === "removing-knight"
            && props.state.deserterPlayerId === props.playerId
            && vertex.knight.ownerPlayerId === props.playerId &&
            <Circle
                onClick={() => {
                    props.dispatch.removeKnight(index, props.loc);
                }}
            />}
        {vertex.knight &&
            <Knight
                disabled={!playersTurn || props.state.phase !== "playing" || vertex.knight.ownerPlayerId !== currentPlayerId}
                state={props.state}
                player={props.state.players.get(vertex.knight.ownerPlayerId)!}
                knight={vertex.knight}
                dispatch={props.dispatch}
                index={index}
                loc={props.loc}
                setKnightToMove={k => {
                    props.setShowSpots(undefined);
                    props.setKnightToMove(k);
                }}
            />}
    </div>;
}