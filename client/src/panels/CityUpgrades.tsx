import React, {useContext} from "react";
import {RoomStateContext} from "../hooks/RoomStateContext";
import {CityUpgradeColor} from "../../../server/src/rooms/schema/CityUpgradeSchema";
import {Resource} from "../../../server/src/rooms/schema/ResourcesSchema";
import {Tooltip} from "react-tooltip";

interface Props {
    color: CityUpgradeColor;
}

export function CityUpgrades(props: Props) {
    const {playerId, roomState, dispatch} = useContext(RoomStateContext);
    const player = roomState.players.get(playerId)!;
    const currentCityLevel = player.cityUpgrades[props.color];

    const levels = Array.from({length: 5}, (value, key) => key + 1).reverse()
    const maxLevel = 5;
    const requiredResources = currentCityLevel + 1;

    const cityProperties = CITY_PROPERTY_MAP[props.color];
    const resource = cityProperties.resource;

    return <div style={{display: "grid", height: "100%", width: "100%"}}>
        {levels.map(level =>
            <div
                key={level}
                style={{
                    border: "1px solid black",
                    height: "23px",
                    backgroundColor: cityProperties.backgroundColor,
                    opacity: level <= currentCityLevel + 1 || (currentCityLevel === level && level === maxLevel) ? undefined : "25%",
                }}
            >
                <div style={{textAlign: "center"}}>
                    {level === currentCityLevel + 1 && level <= maxLevel &&
                    <button
                        onClick={() => dispatch.buyCityUpgrade(props.color, false)}
                        disabled={player.resources[resource] < requiredResources || roomState.phase !== "playing"}
                    >
                        {level} {cityProperties.resourceIcon}
                    </button>}
                    {(level <= currentCityLevel || (currentCityLevel === level && level === maxLevel)) &&
                    <>{level === 1 ? "1-2" : level + 1}</>}
                    {level === 3 &&
                        <span data-tooltip-id="main">{cityProperties.buildingIcon}</span>}
                </div>
            </div>)}
        <Tooltip
            id="main"
            className="tooltip"
        >
            {cityProperties.tooltip}
        </Tooltip>
    </div>
}

interface CityProperties {
    resource: Resource;
    resourceIcon: string;
    backgroundColor: string;
    buildingIcon: string;
    tooltip: string;
}

export const CITY_PROPERTY_MAP: Record<CityUpgradeColor, CityProperties> = {
    yellow: {
        resource: "cloth",
        resourceIcon: "🥻",
        backgroundColor: "yellow",
        buildingIcon: "⚖️",
        tooltip: "Commodities can always be traded 2:1 for any commodity or resource.",
    },
    blue: {
        resource: "coin",
        resourceIcon: "🪙",
        backgroundColor: "lightblue",
        buildingIcon: "🏰",
        tooltip: "Knights can be promoted from strong to mighty.",
    },
    green: {
        resource: "paper",
        resourceIcon: "📜",
        backgroundColor: "lightgreen",
        buildingIcon: "🎉",
        tooltip: "If you get no resources after a dice roll, choose one resource from the bank.",
    }
}