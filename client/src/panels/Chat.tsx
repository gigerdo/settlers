import React, {useEffect, useRef, useState} from "react";
import {ChatMessage} from "../../../server/src/rooms/ChatMessage";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {Room} from "colyseus.js";

interface Props {
    room: Room<SettlersSchema>;
}

export function Chat(props: Props) {
    let ref = useRef<HTMLInputElement>(null);
    let bottomOfChatRef = useRef<HTMLDivElement>(null);

    let [messages, setMessages] = useState<ChatMessage[]>([]);

    const scrollToBottom = () => {
        let height = Number(bottomOfChatRef.current?.scrollHeight);
        bottomOfChatRef.current?.scrollTo(0, height);
    }

    useEffect(() => {
        props.room.onMessage("chat", (message: ChatMessage) => {
            setMessages(prev => [...prev, message]);
            scrollToBottom();
        });
        scrollToBottom();
    }, [props.room]);

    const sendMessage = () => {
        let msg = ref.current!.value;
        if (msg && msg !== "") {
            props.room.send("chat", msg)
            ref.current!.value = "";
        }
    }

    return <div style={{display: "grid", gridTemplateRows: "min-content min-content"}}>
        <div style={{height: "200px", overflowY: "scroll"}} ref={bottomOfChatRef}>
            {messages.map((m, i) => <div key={i}>
                <span style={{
                    borderLeft: `8px solid ${m.color}`,
                    paddingLeft: "5px"
                }}>
                    {m.player}:
                </span> {m.message}
            </div>)}
        </div>
        <div>
            <input
                ref={ref}
                style={{width: "100px"}}
                onKeyPress={event => {
                    if (event.key === "Enter") {
                        sendMessage();
                    }
                }}
            />
            <button onClick={sendMessage}>
                Send
            </button>
        </div>
    </div>
}