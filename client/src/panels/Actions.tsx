import React, {useState} from "react";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {CommandsDispatch} from "../command/CommandsDispatch";
import {OpenTradeModal} from "../trade/OpenTradeModal";
import {TradeReceivedModal} from "../trade/TradeReceivedModal";
import "./Actions.css";
import {VanillaOnly} from "../addons/VanillaOnly";
import {CitiesAndKnights} from "../addons/CitiesAndKnights";
import {ActionButton} from "./ActionButton";

interface Props {
    playerId: string;
    state: SettlersSchema;
    dispatch: CommandsDispatch;
    setShowSpots: (show?: string) => void;
}

export function Actions(props: Props) {
    let [showTrade, setShowTrade] = useState(false);

    let playersIndex = props.state.playerOrder.indexOf(props.playerId);
    let playersTurn = props.state.currentPlayerIndex === playersIndex;
    let player = props.state.players.get(props.playerId)!;

    return <div>
        <ActionButton
            onClick={() => props.dispatch.rollDice()}
            disabled={!playersTurn || props.state.phase !== "roll-dice"}
        >
            Roll Dice
        </ActionButton>

        <ActionButton
            onClick={() => {
                props.dispatch.finishTurn();
                props.setShowSpots(undefined);
            }}
            disabled={!playersTurn || props.state.phase !== "playing"}
        >
            Finish Turn
        </ActionButton>

        <ActionButton
            onClick={() => {
                setShowTrade(true);
            }}
            disabled={!playersTurn || props.state.phase !== "playing"}
        >
            Trade
        </ActionButton>

        <ActionButton
            disabled={!playersTurn || !player.canBuildRoad || props.state.phase !== "playing" || player.roads === 0}
            onClick={() => props.setShowSpots("roads")}
            title="🌳🧱"
        >
            Build Road
        </ActionButton>
        <ActionButton
            disabled={!playersTurn || !player.canBuildSettlement || props.state.phase !== "playing" || player.settlements === 0}
            onClick={() => props.setShowSpots("settlements")}
            title="🌳🧱🐑🌾"
        >
            Build Settlement
        </ActionButton>
        <ActionButton
            disabled={!playersTurn || !player.canBuildCity || props.state.phase !== "playing" || player.cities === 0}
            onClick={() => props.setShowSpots("cities")}
            title="🌾🌾⛰️⛰️⛰️"
        >
            Build City
        </ActionButton>
        <VanillaOnly>
            <ActionButton
                disabled={!playersTurn || !player.canBuyDevelopmentCard || props.state.phase !== "playing" || props.state.bank.developmentCardsTotal === 0}
                title="🐑🌾⛰️"
                onClick={() => props.dispatch.buyDevelopmentCard()}
            >
                Buy Development Card
            </ActionButton>
        </VanillaOnly>
        <CitiesAndKnights>
            <ActionButton
                disabled={!playersTurn || !player.canBuildWall || props.state.phase !== "playing"}
                title="🧱🧱"
                onClick={() => props.setShowSpots("walls")}
            >
                Build Wall
            </ActionButton>
            <ActionButton
                disabled={!playersTurn || !player.canBuyKnight || props.state.phase !== "playing"}
                title="🐑⛰️"
                onClick={() => props.setShowSpots("knights")}
            >
                Recruit Knight
            </ActionButton>
        </CitiesAndKnights>

        {showTrade &&
        <OpenTradeModal
            state={props.state}
            player={player}
            close={() => setShowTrade(false)}
            dispatch={props.dispatch}
        />}
        {props.state.openTrades.size > 0 && !playersTurn &&
        <TradeReceivedModal
            playerId={props.playerId}
            state={props.state}
            dispatch={props.dispatch}
        />}
    </div>;
}

