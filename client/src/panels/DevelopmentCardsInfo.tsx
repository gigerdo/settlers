import {Tooltip} from "react-tooltip";
import React, {ReactElement, useContext, useState} from "react";
import {SelectYearOfPlentyModal} from "../modals/SelectYearOfPlentyModal";
import {SelectMonopolyModal} from "../modals/SelectMonopolyModal";
import {DevelopmentCardSchema} from "../../../server/src/rooms/schema/DevelopmentCardSchema";
import {RoomStateContext} from "../hooks/RoomStateContext";
import {SimpleProgressCardButton} from "./cards/SimpleProgressCardButton";
import {AlchemistProgressCard} from "./cards/AlchemistProgressCard";
import {ResourceMonopolyCard} from "./cards/ResourceMonopolyCard";
import {TradeMonopolyCard} from "./cards/TradeMonopolyCard";
import {DeserterCard} from "./cards/DeserterCard";
import {CraneCard} from "./cards/CraneCard";

interface Props {
    setShowSpots: (show?: string) => void;
}

export function DevelopmentCardsInfo(props: Props) {
    const {player} = useContext(RoomStateContext);

    return <>
        <fieldset style={{minWidth: "80px"}}>
            <legend><b>Cards</b></legend>
            {player.developmentCards.map((card, index) => {
                return <MapCardToComponent key={index} card={card} setShowSpots={props.setShowSpots}/>;
            })}
        </fieldset>
    </>;
}

function MapCardToComponent({card, setShowSpots}: {
    card: DevelopmentCardSchema,
    setShowSpots: (show?: string) => void;
}): ReactElement {
    const {dispatch, roomState} = useContext(RoomStateContext);

    switch (card.type) {
        case "knight":
            return <DevelopmentCard
                card={card}
                description={"By playing the knight card, the robber can be moved.<br> First player to play three knights gets [Largest Army]"}
            />;
        case "victory-point":
            return <DevelopmentCard
                card={card}
                description={
                    roomState.settings.addonCitiesAndKnights ?
                        "Defender of Hexagonia! Grants one victory point." : "Grants one hidden victory point."
                }
            />;
        case "road-building":
            if (roomState.settings.addonCitiesAndKnights) {
                return <SimpleProgressCardButton
                    card={card.type}
                    onClick={() => dispatch.playProgressCard("road-building")}
                    tooltip="After playing this card, two roads can be placed immediately."
                />;
            } else {
                return <DevelopmentCard
                    card={card}
                    description={"After playing this card, two roads can be placed immediately."}
                />;
            }
        case "year-of-plenty":
            return <DevelopmentCard
                card={card}
                description={"Take two resources of your choice from the bank."}
            />;
        case "monopoly":
            return <DevelopmentCard
                card={card}
                description={"Steal all resources of one type from all players."}
            />;

        // Cities&Knights

        case "crane":
            return <CraneCard/>;

        case "engineer":
            return <SimpleProgressCardButton
                card={card.type}
                onClick={() => setShowSpots("walls-engineer")}
                tooltip="Build one city wall for free."
            />;

        case "medicine":
            return <SimpleProgressCardButton
                card={card.type}
                onClick={() => setShowSpots("cities-medicine")}
                tooltip="Upgrade a settlement to a city for ⛰️⛰️🌾 (one less of each)."
            />;

        case "mining":
            return <SimpleProgressCardButton
                card={card.type}
                onClick={() => dispatch.playProgressCard("mining")}
                tooltip="Collect 2 ore for every hex that you have a settlement or city on."
            />;

        case "constitution":
        case "printer":
            return <SimpleProgressCardButton
                disabled={true}
                card={card.type}
                onClick={() => {
                }}
                tooltip="Gives one victory point. (Can't be played and doesn't count against card limit)"
            />;

        case "irrigation":
            return <SimpleProgressCardButton
                card={card.type}
                onClick={() => dispatch.playProgressCard("irrigation")}
                tooltip="Collect 2 grain for each hex adjacent one of your settlements or cities."
            />;

        case "alchemist":
            return <AlchemistProgressCard/>;

        case "resource-monopoly":
            return <ResourceMonopolyCard/>;

        case "trade-monopoly":
            return <TradeMonopolyCard/>;

        case "bishop":
            return <SimpleProgressCardButton
                card={card.type}
                onClick={() => dispatch.playProgressCard("bishop")}
                tooltip="Move the robber. Draw 1 random resource or commodity from each player with a settlement or city on the hex."
            />;

        case "deserter":
            return <DeserterCard/>;

        case "saboteur":
            return <SimpleProgressCardButton
                card={card.type}
                onClick={() => dispatch.playProgressCard("saboteur")}
                tooltip="Each player with the same or more victory points as you must discard half their resources/commodities."
            />;

        case "intrigue":
            return <SimpleProgressCardButton
                card={card.type}
                onClick={() => setShowSpots("opponent-knights")}
                tooltip="You may displace an opponent's knight. The knight must be on an intersection connected to one of your roads."
            />;

        case "diplomat":
            return <SimpleProgressCardButton
                card={card.type}
                onClick={() => setShowSpots("removable-roads")}
                tooltip="You may remove an open road (Without another road or other piece at one end). If you remove your own road, you may place it somewhere else."
            />;

        case "merchant":
            return <SimpleProgressCardButton
                card={card.type}
                onClick={() => setShowSpots("merchant")}
                tooltip="Place the merchant on any hex next to one of your settlements or cities. You can now trade the resources produced by this hex 2:1."
            />;

        default:
            return <div>{card.type}</div>;
    }
}

function DevelopmentCard({card, description}: { card: DevelopmentCardSchema, description: string }) {
    const {roomState, dispatch, player, isPlayersTurn} = useContext(RoomStateContext);

    let [showYearOfPlentyModal, setShowYearOfPlentyModal] = useState(false);
    let [showMonopolyModal, setShowMonopolyModal] = useState(false);

    const disabled = !isPlayersTurn || card.type === "victory-point" || card.sameTurn
        || player.hasPlayedDevelopmentCard
        || (roomState.phase !== "playing" && roomState.phase !== "roll-dice");

    return (
        <div>
            <span data-tooltip-id={"card" + card.type}>
                <button
                    disabled={disabled}
                    onClick={() => {
                        switch (card.type) {
                            case "knight":
                            case "road-building":
                                dispatch.playDevelopmentCard(card.type);
                                break;
                            case "year-of-plenty":
                                setShowYearOfPlentyModal(true);
                                break;
                            case "monopoly":
                                setShowMonopolyModal(true);
                                break;
                        }
                    }}
                >
                    {card.type}
                </button>
            </span>
            <Tooltip
                id={"card" + card.type}
                html={description}
            />

            {showYearOfPlentyModal &&
                <SelectYearOfPlentyModal
                    close={() => setShowYearOfPlentyModal(false)}
                    dispatch={dispatch}
                />}
            {showMonopolyModal &&
                <SelectMonopolyModal
                    close={() => setShowMonopolyModal(false)}
                    dispatch={dispatch}
                />}
        </div>
    );
}