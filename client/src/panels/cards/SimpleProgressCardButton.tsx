import React, {useContext} from "react";
import {RoomStateContext} from "../../hooks/RoomStateContext";
import {Tooltip} from "react-tooltip";
import {ProgressCard} from "../../../../server/src/rooms/schema/DevelopmentCardSchema";

interface Props {
    card: ProgressCard;
    onClick: () => void;
    tooltip: string;
    disabled?: boolean;
}

export function SimpleProgressCardButton(props: Props) {
    const {roomState, isPlayersTurn} = useContext(RoomStateContext);

    let disabled = !isPlayersTurn || roomState.phase !== "playing";
    if (props.disabled !== undefined) {
        disabled = props.disabled;
    }

    return <div data-tooltip-id={"card-" + props.card}>
        <button
            disabled={disabled}
            onClick={props.onClick}

        >
            {props.card}
        </button>
        <Tooltip
            id={"card-" + props.card}
            className={"tooltip"}
            style={{maxWidth: "350px"}}
        >
            {props.tooltip}
        </Tooltip>
    </div>;
}