import {SimpleProgressCardButton} from "./SimpleProgressCardButton";
import React, {useContext, useState} from "react";
import {RoomStateContext} from "../../hooks/RoomStateContext";
import {Modal} from "../../modals/Modal";
import {CITY_PROPERTY_MAP} from "../CityUpgrades";
import {CityUpgradeColor} from "../../../../server/src/rooms/schema/CityUpgradeSchema";

export function CraneCard() {
    const {playerId, roomState, dispatch} = useContext(RoomStateContext);
    const player = roomState.players.get(playerId)!;

    const [modal, setModal] = useState(false);
    let [commodity, setCommodity] = useState("coin");

    return <>
        <SimpleProgressCardButton
            card={"crane"}
            onClick={() => setModal(true)}
            tooltip="You can build one city improvement for 1 commodity less than normal."
        />
        {modal && <Modal close={() => setModal(false)}>
            <h2>Crane</h2>
            <p>You can build one city improvement for 1 commodity less than normal (The first level is free):</p>

            <div>
                <NewComponent color="yellow"/>
                <NewComponent color="blue"/>
                <NewComponent color="green"/>
            </div>
            <p>
                <button onClick={() => setModal(false)}>Cancel</button>
            </p>
        </Modal>}
    </>;
}

function NewComponent({color}: { color: CityUpgradeColor }) {
    const {playerId, roomState, dispatch} = useContext(RoomStateContext);
    const player = roomState.players.get(playerId)!;

    let cityProperties = CITY_PROPERTY_MAP[color];
    const resource = cityProperties.resource;
    const currentCityLevel = player.cityUpgrades[color];
    const requiredResources = currentCityLevel;

    return <span
        style={{
            border: "1px solid black",
            padding: "10px",
            backgroundColor: cityProperties.backgroundColor,
        }}
    >
        <button
            onClick={() => dispatch.buyCityUpgrade(color, true)}
            disabled={player.resources[resource] < requiredResources}
        >
            {player.cityUpgrades[color]} {cityProperties.resourceIcon}
        </button>
    </span>;
}
