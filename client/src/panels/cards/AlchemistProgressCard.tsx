import React, {useContext, useEffect, useState} from "react";
import {RoomStateContext} from "../../hooks/RoomStateContext";
import {SimpleProgressCardButton} from "./SimpleProgressCardButton";
import {Modal} from "../../modals/Modal";

export function AlchemistProgressCard() {
    const {dispatch, roomState, isPlayersTurn} = useContext(RoomStateContext);
    const [modal, setModal] = useState(false);
    const [redDie, setRedDie] = useState(1);
    const [yellowDie, setYellowDie] = useState(1);

    useEffect(() => {
        setRedDie(1);
        setYellowDie(1);
    }, [modal]);

    let totalRoll = redDie + yellowDie;

    return <>
        <SimpleProgressCardButton
            card={"alchemist"}
            onClick={() => setModal(true)}
            disabled={roomState.phase !== "roll-dice" || !isPlayersTurn}
            tooltip="Instead of rolling the dice, set them to any values you want. (Can only be played before dice roll)"
        />
        {modal && <Modal close={() => setModal(false)}>
            <h2>Alchemist</h2>
            <p>Instead of rolling the dice, choose the numbers you want:</p>
            <div style={{display: "grid", gap: "10px"}}>
                <div>Red die: (This number is used to distribute city progress cards)</div>
                <div>
                    <select id="red" name="redDie" onChange={event => setRedDie(Number(event.target.value))}>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>

                <div>Yellow die:</div>
                <div>
                    <select id="yellow" name="yellowDie" onChange={event => setYellowDie(Number(event.target.value))}>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>
                <div>Total roll: {totalRoll}</div>
                <button
                    onClick={() => {
                        setModal(false);
                        dispatch.rollDice(redDie, yellowDie);
                    }}
                >Roll Dice
                </button>
                <button onClick={() => setModal(false)}>Cancel</button>
            </div>
        </Modal>}
    </>;
}