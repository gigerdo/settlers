import {SimpleProgressCardButton} from "./SimpleProgressCardButton";
import React, {useContext, useState} from "react";
import {RoomStateContext} from "../../hooks/RoomStateContext";
import {Modal} from "../../modals/Modal";

export function DeserterCard() {
    const {dispatch, playerId, roomState} = useContext(RoomStateContext);
    const [modal, setModal] = useState(false);
    let players = Array.from(roomState.players.values())
        .map(p => p.id)
        .filter(id => id !== playerId);
    let [player, setPlayer] = useState(players[0]);

    return <>
        <SimpleProgressCardButton
            card={"deserter"}
            onClick={() => setModal(true)}
            tooltip="Choose an opponent. They must remove one of their knights. You may then place one of your own knights on the board."
        />
        {modal && <Modal close={() => setModal(false)}>
            <h2>Choose an opponent</h2>
            <p>That opponent will have to remove one of their knights. You may then place a knight of the same
                strength.</p>

            <div>
                <select
                    id="playerToStealFrom" name="players" onChange={event => setPlayer(event.target.value)}
                >
                    {players.map(playerId => <option key={playerId} value={playerId}>
                        {roomState.players.get(playerId)!.name} ({roomState.players.get(playerId)!.color})
                    </option>)}
                </select>
                <span>
                {" "}
                    <button
                        onClick={() => {
                            setModal(false);
                            dispatch.playProgressCard("deserter", undefined, player);
                        }}
                    >
                    Done
                </button>
            </span>
            </div>
            <p>
                <button onClick={() => setModal(false)}>Cancel</button>
            </p>
        </Modal>}
    </>;
}