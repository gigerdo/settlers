import {SimpleProgressCardButton} from "./SimpleProgressCardButton";
import React, {useContext, useState} from "react";
import {RoomStateContext} from "../../hooks/RoomStateContext";
import {Modal} from "../../modals/Modal";
import {Commodity} from "../../../../server/src/rooms/schema/ResourcesSchema";

export function TradeMonopolyCard() {
    const {dispatch} = useContext(RoomStateContext);
    const [modal, setModal] = useState(false);
    let [commodity, setCommodity] = useState("coin");

    return <>
        <SimpleProgressCardButton
            card={"trade-monopoly"}
            onClick={() => setModal(true)}
            tooltip="Each player must give you 1 of your chosen commodity."
        />
        {modal && <Modal close={() => setModal(false)}>
            <h2>Resource Monopoly</h2>
            <p>Name a commodity. Each player must give you 1 of that type of commodity if they have it.</p>

            <div>
                <select id="resource" name="resource" onChange={event => setCommodity(event.target.value)}>
                    <option value="coin">Coin</option>
                    <option value="paper">Paper</option>
                    <option value="cloth">Cloth</option>
                </select>
                <span>
                {" "}
                    <button
                        onClick={() => {
                            setModal(false);
                            dispatch.playProgressCard("trade-monopoly", commodity as Commodity);
                        }}
                    >
                    Done
                </button>
            </span>
            </div>
            <p>
                <button onClick={() => setModal(false)}>Cancel</button>
            </p>
        </Modal>}
    </>;
}