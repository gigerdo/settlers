import {SimpleProgressCardButton} from "./SimpleProgressCardButton";
import React, {useContext, useState} from "react";
import {RoomStateContext} from "../../hooks/RoomStateContext";
import {Modal} from "../../modals/Modal";
import {Resource} from "../../../../server/src/rooms/schema/ResourcesSchema";

export function ResourceMonopolyCard() {
    const {dispatch} = useContext(RoomStateContext);
    const [modal, setModal] = useState(false);
    let [resource, setResource] = useState("lumber");

    return <>
        <SimpleProgressCardButton
            card={"resource-monopoly"}
            onClick={() => setModal(true)}
            tooltip="Each player must give you 2 of your chosen resource."
        />
        {modal && <Modal close={() => setModal(false)}>
            <h2>Resource Monopoly</h2>
            <p>Name a resource. Each player must give you 2 of that type of resource if they have them.</p>

            <div>
                <select id="resource" name="resource" onChange={event => setResource(event.target.value)}>
                    <option value="lumber">Lumber</option>
                    <option value="brick">Brick</option>
                    <option value="wool">Wool</option>
                    <option value="grain">Grain</option>
                    <option value="ore">Ore</option>
                </select>
                <span>
                {" "}
                    <button
                        onClick={() => {
                            setModal(false);
                            dispatch.playProgressCard("resource-monopoly", resource as Resource);
                        }}
                    >
                    Done
                </button>
            </span>
            </div>
            <p>
                <button onClick={() => setModal(false)}>Cancel</button>
            </p>
        </Modal>}
    </>;
}