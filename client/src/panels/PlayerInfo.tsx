import React from "react";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {Tooltip} from "react-tooltip";
import {VanillaOnly} from "../addons/VanillaOnly";

interface Props {
    playerId: string;
    state: SettlersSchema;
}

export function PlayerInfo(props: Props) {
    return <div style={{display: "grid", gap: "5px"}}>
        {props.state.playerOrder.map((playerId, index) => {
            let currentPlayerIndex = props.state.currentPlayerIndex;
            let player = props.state.players.get(playerId)!;
            return <div
                key={playerId}
                style={{
                    fontWeight: index === currentPlayerIndex ? "bold" : "normal",
                    border: `1px solid ${player.color}`,
                    borderLeft: `15px solid ${player.color}`,
                    padding: "4px",
                }}
            >
                <div>
                    <div title={player.id}>
                        {!player.connected ? <><b>DISCONNECTED</b><br/></> : null}
                        {index + 1}. {player.name}
                    </div>
                    <div>
                        <span
                            data-tooltip-id="main"
                            data-tooltip-content={`Victory Points (${props.state.settings.addonCitiesAndKnights ? 13 : 10} needed to win)`}
                        >
                            <>✌️ {player.victoryPointsVisible}</>
                            <VanillaOnly>
                                {props.playerId === player.id && player.victoryPointsHidden > 0 && <>({player.victoryPointsHidden})</>}
                            </VanillaOnly>
                        </span>
                        {(player.largestArmy || player.longestRoad) &&
                        <> ({<>
                            <VanillaOnly>
                                {player.largestArmy &&
                                    <span data-tooltip-id="main" data-tooltip-content="Largest army">⚔</span>}
                            </VanillaOnly>
                            {player.longestRoad &&
                                <span data-tooltip-id="main" data-tooltip-content="Longest road">🛣️</span>}
                        </>})
                        </>}
                        <span data-tooltip-id="main" data-tooltip-content="Resource cards">
                            , 🃏 {player.resourcesTotal}
                        </span>
                        <VanillaOnly>
                            <span data-tooltip-id="main" data-tooltip-content="Development cards">
                                 {" "}/ {player.totalDevelopmentCards}
                            </span>
                        </VanillaOnly>
                        <VanillaOnly>
                            <span data-tooltip-id="main" data-tooltip-content="Number of knights played">
                                , ⚔️ {player.knights}
                            </span>
                        </VanillaOnly>
                    </div>
                </div>
                <Tooltip
                    id="main"
                    className="tooltip"
                />
            </div>;
        })}
    </div>;
}