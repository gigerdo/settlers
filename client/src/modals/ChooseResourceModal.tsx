import React from "react";
import {Modal} from "./Modal";
import {CommandsDispatch} from "../command/CommandsDispatch";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {Resource} from "../../../server/src/rooms/schema/ResourcesSchema";
import {TradeResourceButton} from "../trade/TradeResourceButton";

const resources: Resource[] = ["lumber", "brick", "wool", "grain", "ore"];

interface Props {
    state: SettlersSchema;
    dispatch: CommandsDispatch;
    playerId: string;
}

export function ChooseResourceModal(props: Props) {
    let player = props.state.players.get(props.playerId)!;

    let canChooseResource = player.receivedResourcesThisTurn === 0
        && player.cityUpgrades.green >= 3
        && !player.hasChosenResourceThisTurn;

    return <Modal
        close={() => {
        }}
    >
        <h2>Aqueduct: Choose a resource to receive</h2>
        {!canChooseResource &&
            <div>Please wait for the other players to choose their free resources.</div>}
        {canChooseResource &&
            <div style={{display: "grid", gap: "10px"}}>
                <div>You did not receive any resources this turn. Due to your green city upgrade, you are allowed to
                    choose one free resource from the bank:
                </div>
                <div style={{display: "flex", gap: "5px", minHeight: "25px"}}>
                    {resources.map(resource =>
                        <TradeResourceButton
                            key={resource}
                            resource={resource}
                            resourceAmount={1}
                            onClick={() => props.dispatch.chooseResource(resource)}
                        />)}
                </div>

                <div style={{marginTop: "20px"}}>
                    <button onClick={() => props.dispatch.chooseResource(undefined)}>I don't want any resource.</button>
                </div>
            </div>}
    </Modal>;
}