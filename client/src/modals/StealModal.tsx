import React, {useEffect, useState} from "react";
import {Modal} from "./Modal";
import {CommandsDispatch} from "../command/CommandsDispatch";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";

interface Props {
    tileIndex: number;
    playerId: string;
    close: () => void;
    state: SettlersSchema;
    dispatch: CommandsDispatch;
}

export function StealModal(props: Props) {
    let tile = props.state.board.tiles[props.tileIndex];
    let playersToStealFrom = Array.from(tile.players.values())
        .filter(playerId => playerId !== props.playerId);
    let [stealFromPlayer, setStealFromPlayer] = useState(playersToStealFrom[0]);

    useEffect(() => {
        if (playersToStealFrom.length === 1) {
            props.close();
            props.dispatch.placeRobber(tile.x, tile.y, playersToStealFrom[0]);
        }
        if (playersToStealFrom.length === 0) {
            props.close();
            props.dispatch.placeRobber(tile.x, tile.y, "");
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return <Modal
        close={props.close}
    >
        <h2>Select Resource To Steal From All Players <button onClick={props.close}>X</button></h2>
        <select id="playerToStealFrom" name="players" onChange={event => setStealFromPlayer(event.target.value)}>
            {playersToStealFrom.map(playerId => <option key={playerId} value={playerId}>
                {props.state.players.get(playerId)!.name} ({props.state.players.get(playerId)!.color})
            </option>)}
        </select>
        <button onClick={() => {
            props.close();
            props.dispatch.placeRobber(tile.x, tile.y, stealFromPlayer);
        }}>
            Done
        </button>
    </Modal>;
}