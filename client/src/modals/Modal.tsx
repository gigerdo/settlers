import React, {ReactNode} from "react";

interface Props {
    close: () => void;
    children: ReactNode;
}

export function Modal(props: Props) {
    return <div
        style={{
            position: "fixed",
            top: 0,
            left: 0,
            background: "rgba(0, 0, 0, 0.5)",
            height: "100%",
            width: "100%",
            zIndex: 99,
        }}
        onClick={() => props.close()}
    >
        <div
            style={{
                background: "white",
                maxWidth: "100%",
                position: "fixed",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                padding: "40px"
            }}
            onClick={event => {
                event.stopPropagation();
            }}
        >
            {props.children}
        </div>
    </div>
}