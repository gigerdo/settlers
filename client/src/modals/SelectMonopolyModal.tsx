import React, {useState} from "react";
import {Modal} from "./Modal";
import {CommandsDispatch} from "../command/CommandsDispatch";

interface Props {
    close: () => void;
    dispatch: CommandsDispatch;
}

export function SelectMonopolyModal(props: Props) {
    let [resource, setResource] = useState("lumber");

    return <Modal
        close={props.close}
    >
        <h2>Select Resource To Steal From All Players <button onClick={props.close}>X</button></h2>
        <select id="first" name="resource1" onChange={event => setResource(event.target.value)}>
            <option value="lumber">Lumber</option>
            <option value="brick">Brick</option>
            <option value="wool">Wool</option>
            <option value="grain">Grain</option>
            <option value="ore">Ore</option>
        </select>
        <button onClick={() => {
            props.close();
            props.dispatch.playMonopoly(resource);
        }}>
            Done
        </button>
    </Modal>;
}