import React from "react";
import {Modal} from "./Modal";
import {CommandsDispatch} from "../command/CommandsDispatch";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {Resource} from "../../../server/src/rooms/schema/ResourcesSchema";

const resources: Resource[] = ["lumber", "brick", "wool", "grain", "ore"];

interface Props {
    state: SettlersSchema;
    dispatch: CommandsDispatch;
    playerId: string;
}

export function ChooseProgressCardModal(props: Props) {
    let player = props.state.players.get(props.playerId)!;
    let canChooseProgressCard = props.state.playersWinningBarbarianAttack.find(id => id === player.id) !== undefined;

    return <Modal
        close={() => {
        }}
    >
        <h2>Barbarian attack repelled! Choosing progress cards.</h2>
        {!canChooseProgressCard &&
            <div>Please wait for the other players to choose their progress card.</div>}
        {canChooseProgressCard &&
            <div style={{display: "grid", gap: "10px"}}>
                <div>Multiple people had the same knight strength when defending against the barbarians.</div>
                <div>So everyone can choose one free progress card:</div>
                <div style={{display: "flex", gap: "5px", minHeight: "25px"}}>
                    <button
                        disabled={props.state.bank.scienceCardsTotal <= 0}
                        onClick={() => props.dispatch.chooseProgressCard("green")}
                    >
                        Trade (Yellow)
                    </button>
                    <button
                        disabled={props.state.bank.politicsCardsTotal <= 0}
                        onClick={() => props.dispatch.chooseProgressCard("blue")}
                    >
                        Politics (Blue)
                    </button>
                    <button
                        disabled={props.state.bank.tradeCardsTotal <= 0}
                        onClick={() => props.dispatch.chooseProgressCard("yellow")}
                    >
                        Science (Green)
                    </button>
                </div>
            </div>}
    </Modal>;
}