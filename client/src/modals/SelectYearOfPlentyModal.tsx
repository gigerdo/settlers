import React, {useState} from "react";
import {Modal} from "./Modal";
import {CommandsDispatch} from "../command/CommandsDispatch";

interface Props {
    close: () => void;
    dispatch: CommandsDispatch;
}

export function SelectYearOfPlentyModal(props: Props) {
    let [first, setFirst] = useState("lumber");
    let [second, setSecond] = useState("lumber");

    return <Modal
        close={props.close}
    >
        <h2>Select Two Resources <button onClick={props.close}>X</button></h2>
        <select id="first" name="resource1" onChange={event => setFirst(event.target.value)}>
            <option value="lumber">Lumber</option>
            <option value="brick">Brick</option>
            <option value="wool">Wool</option>
            <option value="grain">Grain</option>
            <option value="ore">Ore</option>
        </select>
        <select id="second" name="resource2" onChange={event => setSecond(event.target.value)}>
            <option value="lumber">Lumber</option>
            <option value="brick">Brick</option>
            <option value="wool">Wool</option>
            <option value="grain">Grain</option>
            <option value="ore">Ore</option>
        </select>
        <button onClick={() => {
            props.close();
            props.dispatch.playYearOfPlenty(first, second);
        }}>
            Done
        </button>
    </Modal>;
}