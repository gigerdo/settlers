import React, {useContext, useState} from "react";
import {Modal} from "./Modal";
import {CommandsDispatch} from "../command/CommandsDispatch";
import {SettlersSchema} from "../../../server/src/rooms/schema/SettlersSchema";
import {TradeResources} from "../../../server/src/rooms/schema/TradeSchema";
import {Resource} from "../../../server/src/rooms/schema/ResourcesSchema";
import {TradeResourceButton} from "../trade/TradeResourceButton";
import {CitiesAndKnights} from "../addons/CitiesAndKnights";
import {RoomStateContext} from "../hooks/RoomStateContext";

const resources: Resource[] = ["lumber", "brick", "wool", "grain", "ore", "paper", "cloth", "coin"]

interface Props {
    state: SettlersSchema;
    dispatch: CommandsDispatch;
    playerId: string;
}

export function DiscardCardsModal(props: Props) {
    const {isPlayersTurn} = useContext(RoomStateContext);
    let [resourcesToDiscard, setResourcesToDiscard] = useState<TradeResources>({
        lumber: 0, brick: 0, wool: 0, grain: 0, ore: 0, paper: 0, cloth: 0, coin: 0
    });
    let cardsDiscarded = Object.values(resourcesToDiscard).reduce(((previousValue, currentValue) => previousValue + currentValue))

    let player = props.state.players.get(props.playerId)!;
    let totalCards = player.resources.lumber + player.resources.brick + player.resources.wool +
        player.resources.grain + player.resources.ore + player.resources.paper + player.resources.cloth +
        player.resources.coin;
    let maximumCards = 7 + (3 - player.walls) * 2;
    let cardsToDiscard = totalCards > maximumCards ? Math.floor(totalCards / 2) : 0;
    if (props.state.saboteurPlayed) {
        if (isPlayersTurn) {
            cardsToDiscard = 0;
        } else {
            cardsToDiscard = Math.floor(totalCards / 2);
        }
    }

    const update = (resource: Resource, amount: number) => {
        setResourcesToDiscard(prev => {
            return {
                ...prev,
                [resource]: prev[resource] + amount
            }
        });
    }

    return <Modal
        close={() => {
        }}
    >
        {props.state.saboteurPlayed ? <h2>Saboteur: Discarding Cards</h2> : <h2>Robber: Discarding Cards</h2>}

        {(cardsToDiscard === 0 || player.hasDiscardedThisTurn) &&
        <div>Waiting for the other players to discard their cards.</div>}
        {cardsToDiscard > 0 && !player.hasDiscardedThisTurn &&
        <div style={{display: "grid", gap: "10px"}}>
            <div>Choose {cardsToDiscard} cards to discard:</div>
            <CitiesAndKnights>
                <div>({3 - player.walls} walls)</div>
            </CitiesAndKnights>
            <div style={{display: "flex", gap: "5px", minHeight: "25px"}}>
                {resources.map(resource =>
                    <TradeResourceButton
                        key={resource}
                        resource={resource}
                        resourceAmount={player.resources[resource] - resourcesToDiscard[resource]}
                        onClick={() => update(resource, 1)}
                        disabled={cardsDiscarded === cardsToDiscard}
                    />)}
            </div>

            <div>Discarded: {cardsDiscarded} / {cardsToDiscard}</div>
            <div style={{display: "flex", gap: "5px", minHeight: "25px"}}>
                {resources.map(resource =>
                    <TradeResourceButton
                        key={resource}
                        resource={resource}
                        resourceAmount={resourcesToDiscard[resource]}
                        onClick={() => update(resource, -1)}
                    />)}
            </div>
            <div></div>
            <div>
                <button
                    disabled={cardsDiscarded !== cardsToDiscard}
                    onClick={() => props.dispatch.discardCards(resourcesToDiscard)}
                >
                    Discard
                </button>
            </div>
        </div>}
    </Modal>;
}