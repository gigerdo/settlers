import {VertexLoc, VertexSchema} from "../../../server/src/rooms/schema/VertexSchema";
import {EdgeLoc, EdgeSchema} from "../../../server/src/rooms/schema/EdgeSchema";
import {BoardSchema} from "../../../server/src/rooms/schema/BoardSchema";
import {TileSchema} from "../../../server/src/rooms/schema/TileSchema";

export function getEdgesOfVertex(board: BoardSchema, x: number, y: number, loc: VertexLoc): EdgeSchema[] {
    let ownerTile = getTile(board, x, y)!;

    let edges = [];
    if (loc === "top") {
        edges.push(ownerTile.north);
        let topLeft = getTopLeftOf(board, x, y);
        if (topLeft) {
            edges.push(topLeft.east);
            edges.push(topLeft.south);
        }
    } else {
        edges.push(ownerTile.south);
        let bottomLeft = getBottomLeftOf(board, x, y);
        if (bottomLeft) {
            edges.push(bottomLeft.north);
            edges.push(bottomLeft.east);
        }
    }
    return edges;
}

export function getVerticesOfEdge(board: BoardSchema, x: number, y: number, loc: EdgeLoc): VertexSchema[] {
    let ownerTile = getTile(board, x, y)!;

    switch (loc) {
        case "north": {
            let vertices = [ownerTile.top];
            let topRightOf = getTopRightOf(board, x, y);
            if (topRightOf) {
                vertices.push(topRightOf.bottom);
            }
            return vertices;
        }
        case "east": {
            let vertices = [];
            let topRight = getTopRightOf(board, x, y);
            if (topRight) {
                vertices.push(topRight.bottom);
            }
            let bottomRight = getBottomRightOf(board, x, y);
            if (bottomRight) {
                vertices.push(bottomRight.top);
            }
            return vertices;
        }
        case "south": {
            let vertices = [ownerTile.bottom];
            let bottomRight = getBottomRightOf(board, x, y);
            if (bottomRight) {
                vertices.push(bottomRight.top);
            }
            return vertices;
        }
    }
}


export function getTile(board: BoardSchema, x: number, y: number): TileSchema | undefined {
    if (x >= 0 && x < 7 && y >= 0 && y < 7) {
        return board.tiles[y * 7 + x];
    }
    return undefined;
}

export function getTopRightOf(board: BoardSchema, x: number, y: number): TileSchema | undefined {
    let topRightX;
    let topRightY;
    if (y % 2 === 0) {
        topRightX = x;
        topRightY = y - 1;
    } else {
        topRightX = x + 1;
        topRightY = y - 1;
    }
    return getTile(board, topRightX, topRightY);
}

export function getBottomRightOf(board: BoardSchema, x: number, y: number): TileSchema | undefined {
    let bottomRightX;
    let bottomRightY;
    if (y % 2 === 0) {
        bottomRightX = x;
        bottomRightY = y + 1;
    } else {
        bottomRightX = x + 1;
        bottomRightY = y + 1;
    }
    return getTile(board, bottomRightX, bottomRightY);
}

export function getTopLeftOf(board: BoardSchema, x: number, y: number): TileSchema | undefined {
    let topLeftX;
    let topLeftY;
    if (y % 2 === 0) {
        topLeftX = x - 1;
        topLeftY = y - 1;
    } else {
        topLeftX = x;
        topLeftY = y - 1;
    }
    return getTile(board, topLeftX, topLeftY);
}

export function getBottomLeftOf(board: BoardSchema, x: number, y: number): TileSchema | undefined {
    let bottomLeftX;
    let bottomLeftY;
    if (y % 2 === 0) {
        bottomLeftX = x - 1;
        bottomLeftY = y + 1;
    } else {
        bottomLeftX = x;
        bottomLeftY = y + 1;
    }
    return getTile(board, bottomLeftX, bottomLeftY);
}

export function getTilesOfVertex(board: BoardSchema, index: number, loc: VertexLoc) {
    let ownerTile = board.tiles[index];
    let {x, y} = ownerTile;

    let tiles = [ownerTile];
    if (loc === "top") {
        let topLeft = getTopLeftOf(board, x, y);
        if (topLeft) {
            tiles.push(topLeft);
        }

        let topRight = getTopRightOf(board, x, y);
        if (topRight) {
            tiles.push(topRight);
        }
    } else {
        let bottomLeft = getBottomLeftOf(board, x, y);
        if (bottomLeft) {
            tiles.push(bottomLeft);
        }

        let bottomRight = getBottomRightOf(board, x, y);
        if (bottomRight) {
            tiles.push(bottomRight);
        }
    }
    return tiles;
}

export function getAllVerticesOfTile(board: BoardSchema, x: number, y: number): VertexSchema[] {
    let tile = getTile(board, x, y)!;
    let vertices = [tile.top, tile.bottom];
    let topLeft = getTopLeftOf(board, x, y);
    if (topLeft) {
        vertices.push(topLeft.bottom);
    }
    let bottomLeft = getBottomLeftOf(board, x, y);
    if (bottomLeft) {
        vertices.push(bottomLeft.top);
    }
    let bottomRight = getBottomRightOf(board, x, y);
    if (bottomRight) {
        vertices.push(bottomRight.top);
    }
    let topRight = getTopRightOf(board, x, y);
    if (topRight) {
        vertices.push(topRight.bottom);
    }
    return vertices;
}
