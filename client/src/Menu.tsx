import {Room, RoomAvailable} from "colyseus.js";
import React, {useEffect, useRef, useState} from "react";
import {Link, useNavigate} from "react-router";
import {client} from "./command/client";
import {UserNameInput} from "./UserNameInput";
import {useLocalStorage} from "./hooks/useLocalStorage";

export function Menu() {
    let navigate = useNavigate();
    let [rooms, setRooms] = useState<RoomAvailable[]>([]);
    const {userName, userId, setUserId, setUserName} = useLocalStorage();
    const lobbyRef = useRef<Room<unknown>>();

    useEffect(() => {
        client.joinOrCreate("lobby")
            .then(lobby => {
                setUserId(lobby.sessionId); // Use the lobby session-id as persistent user-id
                lobbyRef.current?.leave();
                lobbyRef.current = lobby;

                lobby.onMessage("rooms", (rooms) => {
                    setRooms(rooms);
                });

                lobby.onMessage("+", ([roomId, room]) => {
                    setRooms(prev => {
                        const roomIndex = prev.findIndex((room) => room.roomId === roomId);
                        if (roomIndex !== -1) {
                            prev[roomIndex] = room;

                        } else {
                            prev.push(room);
                        }
                        return [...prev];
                    });
                });

                lobby.onMessage("-", (roomId) => {
                    setRooms(prev => {
                        return prev.filter((room) => room.roomId !== roomId);
                    });
                });

                lobby.onError((code, message) => {
                    console.error(`Error joining lobby. message=[${message}], code=[${code}]`);
                });

                return lobby;
            });
        return () => {
            lobbyRef.current?.leave();
            lobbyRef.current = undefined;
        }
    }, [setUserId]);

    return (
        <>
            <h1>Settlers of Hexagonia</h1>
            <UserNameInput
                userName={userName}
                setUserName={setUserName}
            />
            {userName && <>
                <h2>Rooms</h2>
                <button
                    onClick={() => client.create("settlers", {
                        userId: userId,
                        userName: userName
                    })
                        .then(room => {
                            navigate(`/room/${room.id}`);
                        })
                        .catch(err => {
                            console.error("Failed to create room.", err);
                        })}>
                    New Room
                </button>
                <ul>
                    {rooms.map(r => <li key={r.roomId}>
                        <Link to={`/room/${r.roomId}`}>{r.roomId} ({r.metadata?.players ?? "?"}/{r.maxClients})</Link>
                    </li>)}
                </ul>
            </>}
        </>
    )
}