# settlers

Settlers of Catan clone. (https://settlers.giger.dev)

* server: server based on colyseus.io
  * start postgres in docker
    * `docker run -d --name settlers-db -e POSTGRES_PASSWORD=password -p 5432:5432 postgres`
  * Start with `DATABASE_URL=postgres://postgres:password@localhost:5432/postgres?sslmode=disable npm start`
* client: react spa
  * Start with `npm start`

## Deployment
See `.gitlab-ci.yml` for implementation.

* Build image with `docker build -t registry.gitlab.com/giger/settlers .`
* The image can run without a database, though this will mean all game data is lost on restart
  * A database can be set via `DATABASE_URL` parameter. The expected format
    is `postgres://user:password@host:port/dbname`
  * If a database is set, it will be automatically migrated on startup
  * An admin password can be set with the env var `ADMIN_PASSWORD`
 
## TODO

- Progress cards: trader(yellow), politics(blue), science(green)
  - Max 4 cards, discard if more (player can choose)
  - Implement individual card actions
- Show received cards, resources etc. after roll
- Improve number assignment
- Add pirate icon for barbarians

## DB Queries

Shows all finished games with their players and the winner.

```postgresql
select room_id,
       game_save.created_at,
       state::json ->> 'winner'                                                           as winner,
       key                                                                                as player,
       value ->> 'name'                                                                           as name,
       value ->> 'color'                                                                          as color,
       array_length(ARRAY(select json_object_keys((state::json ->> 'players')::json)), 1)         as player_count,
       ARRAY(select value ->> 'name' from json_each((game_save.state::json ->> 'players')::json)) as players
from game_save, json_each((game_save.state::json ->> 'players')::json)
where state::json ->> 'winner' = key
```
